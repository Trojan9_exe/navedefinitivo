{
    "id": "26f5e463-5db4-4ddd-8911-d74651b7dd2f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_aux",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a65d94d8-32f3-4c6b-a5df-773a70cc8611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 59,
                "y": 92
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0f74c96a-168e-45d5-b8e6-997b831983dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 112,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "52abe580-76ff-4812-b357-36c4bd887ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 44,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ea3e8ff8-fbe4-4003-b49d-59687822837d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5a0f9207-3e58-4d11-8f2b-230c6e213580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 32
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "afba6dce-478b-45b6-8699-999d2462c41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "00fc214f-aac3-4de5-9dc4-ab364e31667e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "86875e6b-98c3-4f88-b0e8-bcd95dd23f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 132,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dcbd6ca3-4402-4f4a-aef1-a77920db4bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "320a538c-a712-47b5-a4b2-9c91b7940cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "272af2c5-0d60-4d0a-897e-c53a8c586fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 120,
                "y": 32
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "13895450-ae6d-46b6-8dbd-cac049698d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "105e0a28-5147-4ea1-8e9a-8d9ad51cf1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 106,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1ce6abc0-caa6-4df1-8b82-7898c9a9272c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 80,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "db2e424b-5206-403e-a844-ef8f15e200e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 148,
                "y": 92
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5ad9d3a5-3b0f-4ec9-8a2c-cbe2cb2bda9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4606bf83-9eb7-4218-802a-3ad3d3d76d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "59d0f1bd-f612-4180-b556-4076420699d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 94,
                "y": 92
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "16e99cf7-06f4-4111-b347-daa3a0c1851e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 190,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "87a25949-8791-4137-bec5-8507fdb8b453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "006aaa12-df18-4069-9fb2-b478f8e25982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0494ea36-6d62-40cd-8c22-1fa1aa69049d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d4fd0133-1681-4014-94d0-b8b82641f5cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "abc1d5b6-f152-44cd-8f75-544e4fc20585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 32
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3b383d28-c3c9-467d-ac64-8d12c5c1210d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 200,
                "y": 32
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cbd261a5-e6f0-4ad0-8d3e-141870ee6dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 32
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ca09cb7c-8b03-4f15-9609-799e17a2e8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 144,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d99ebd1d-cb2d-4d11-9963-0a1245ca1a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 122,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "39835da6-a634-43b3-9904-7a0fea80d246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "aa77fc7c-f921-4f05-bee8-f4e718e24b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "81f76ec3-3557-412f-9894-869b526337c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 233,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "abd089f0-96a4-4e1c-b6ae-339cd73dcf42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "66b15d00-f590-4172-8156-db61a4470d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8d527843-ea50-4388-8303-f1c388ad3644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7c461656-f691-402e-8790-1ae978aaa14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "09aec7cb-48ce-45a3-882c-6271ef2297b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3c5a3939-a582-4994-817d-01c3dc9d0f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "585c7bbc-70d9-402d-9687-3f2a253b9cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 224,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cda1db2a-e681-4a4e-9ffc-a125579b90fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 215,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "364755c2-4810-4d2a-ab6f-9aa5a65dd63a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 32
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "46304805-1935-42fe-af9b-dec31fcc0c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "850ee5a8-590a-404e-98f7-438b54c9d1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 117,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3e1b6903-b63f-4e0e-98d9-93a5694e0e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 206,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d1b91b7c-c437-4287-b033-299bad506bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "66ab6329-980b-444e-9315-bc89ef078eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f9871e82-94c7-4ca9-844d-950620939083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "27e70183-123d-446e-be6b-c89554cb212f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ee4c83e1-94ae-446a-b095-513d417c3cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2f786975-53e4-4d20-9dff-5fa8f6353a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 160,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0cae616c-b2a1-4c86-a19e-ac45794cb5a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eefe889d-23c5-42bf-8edf-969a176f35ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cb7f79e9-37f5-44c4-9a54-4623a7cf23bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 140,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6359d5e7-ebd6-4649-b486-0be6a3ce1b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "56cd2009-6a6a-40ab-8b63-c234b5cea105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67f1f4db-37ad-4bb8-a5fd-14be1bd71e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "eddee1b2-acc8-4660-8c20-f1ef66d92fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bbf185ed-cb00-4362-9ec1-f01f038e50a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bac360b3-8285-4b8e-aa8c-bedd9d3ed0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3e1a38d2-adef-4d46-98d1-f67849d7418e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6ddf7fd1-e1c8-4acf-96c6-a9dbdef6d7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 52,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "60b15055-b3b5-4e67-8ae4-ef6c0d2e7145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8337a98e-8472-4fce-9ac5-3ad4a06f678d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0f8dd235-9b77-4667-b5a7-8c9240887612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "512e2f2c-b25d-486f-93e5-2417bfcf03a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 32
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f76d961d-0632-4256-b6fd-0f283f8294f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "49cb8df9-22d1-4e76-b7d7-2e3e814d6050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0d39548b-f5af-42fd-8e36-69744e9e7b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "274e105d-5322-447c-b86c-4a6e390cae80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a9ffca13-10f9-404b-a3ae-e7aae662c67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 152,
                "y": 62
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "943a0691-ecc5-4377-8ccf-29d417c10e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4b5982de-9f39-441c-9779-3e145d6719e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 36,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a557cfc2-ea0b-4a36-9cfa-aabbc9aaf5a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 62
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3f63c5ad-0994-44b5-ac90-fa12cacde5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "30b315b3-224c-445a-bb64-f701843d3959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 140,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1233849e-a78c-46f2-b6da-709f483ee9f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 92
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "df525f10-303c-4fd6-bca8-6d1fe264415c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 179,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b9159c78-84c3-4f3c-8186-406c400c1aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 136,
                "y": 92
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5f6ab01d-657b-48d3-8369-500b567f4ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d433103d-6a0b-46d6-97ba-6f7d83f6ffe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f2066bcb-c5a9-4028-8e99-b2e49ec00cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6d3dda2e-3f3a-4d3b-a042-5b7803c7c91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bf482bd9-52ac-4d6e-9597-ceb5fc491abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 125,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7222d5ca-d6b3-4afb-b2c6-2df672af5f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 92
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "025bca9f-0846-49fd-86f6-5141a1b17b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 170,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "83986a85-f18b-42e9-b378-f07df0c0d72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ff1d15d7-842e-4102-8214-eff49fe2f418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "73ebfbf3-c62a-4abc-997a-182821a8d3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5d518d62-76e0-4250-beb7-f883975ceb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9f2a8ff3-c172-43f8-9f9e-302bf6861eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 180,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b4123cef-f46a-42c9-9fad-b19813f1c588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 210,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dc35488e-8f10-4ca4-946f-88647829b247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "30d80e2f-5d96-4d0f-8d33-7ac715a4df1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "08081941-e343-4e46-9d48-726d3ff48c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 92
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "93f4eed7-661e-496b-af4c-cc2850ba2e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 188,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "68d7ac4a-40c8-44c6-b51d-05318b88fd6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 46,
                "y": 32
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}