{
    "id": "87e83c60-cfbb-435c-a342-c4c341477185",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_interface",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\font_interface\\spyagencyv3.ttf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "102ac2b0-df27-457b-a172-08b7d61e2f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 192,
                "y": 182
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9ee5de85-4986-46b5-b5fb-e1c7f9b18490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 38,
                "y": 212
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bc64742b-2eff-43df-acb9-21e446961d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 182
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7ba63555-7e2b-4912-958d-001015113072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "724b27ce-75a8-476c-adf4-18a1e8f933dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 96,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "094c9a19-f576-4dee-b601-301b1623f007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "58e2740e-265f-42a5-bb5a-5593eafc1782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 233,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d2f8b4f3-0ded-48b1-bab1-bcf5f3d29dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 63,
                "y": 212
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a297e902-dfaf-4173-8857-38344ed6cd6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 206,
                "y": 182
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7e9300c1-574a-4211-8953-3e0454c48abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 182
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b4d2a40d-bbda-41ba-9e28-017ec28f7194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3e7b9a1a-1dfd-4f9b-8aef-42149c7ccbf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 32
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "30c3bd49-ac4b-460c-9266-12617711fba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 212
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ba53eda7-aad5-4060-9b5e-06ce9ea2e828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 213,
                "y": 182
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f511d78d-1513-4281-ba39-5c0c73ed2816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 83,
                "y": 212
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4f8ce025-3bd6-4a4e-884f-d0e0c793f087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 32
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "47ff15be-a2b7-4bbe-974c-5c005683667a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 92
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cbd19eca-4c10-4db9-96be-79071a74062b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 212
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "be8cd1cb-0c69-4c45-b958-c63e8f147697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 176,
                "y": 92
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b157a433-0532-4ded-ae62-dd0fc6230447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bbe954ac-5dd6-40f7-9123-81cd8c08c49c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 92
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e0518dfe-0416-4950-b727-34eeb0ddc92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8c6a0d4c-5ab6-4595-9957-7c094e923349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "789426aa-72e3-4465-a8a1-470c3612a3af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b38087a3-7477-4ea3-9c27-76a9ae8d9c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "da406d0b-94b8-45c1-a5a3-c254f1243c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a14829d1-32f6-4825-a8ac-e77ed6b32e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 67,
                "y": 212
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fe3c9d3a-e473-4e3b-846b-61584798b599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 212
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "74900d45-281c-4522-a054-8b9a5969939c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 152
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "06f3cb5a-f016-4f4e-97b7-0b8ba180eadf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7f70abc5-8944-4bc5-bba6-91315d0ef5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 152
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "dd68db59-034f-4560-b9df-cb82d2db352a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2b5050a2-df3c-4ca9-8a42-987c38c51521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d1e791a1-9848-4bc7-8f1d-8cc8b42c4cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "afda7351-ca82-4d8c-a8c4-cd50c4f96824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "295f37e4-0756-45a7-bc1d-dc4bed472f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fff4fe53-a01d-4f33-97eb-5541a0b91c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "68f0fd6f-2a60-475c-96c3-110e70d6a33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 92,
                "y": 152
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b7b1363f-4ffa-415b-a804-39055527f13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 152
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "90696e3e-0618-44cb-95bd-0e3ae9e15afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 236,
                "y": 92
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "58baee94-4830-4cca-992f-8678b83eae33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "867cd1f0-3cd2-4ea1-a066-047b549a51c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 48,
                "y": 212
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9ad0967b-b46a-4003-8e70-99cd867d78df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 152
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "79354e66-d2b6-4a8d-9669-b50bb5bf8adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5f4102cf-426c-44aa-b909-0d657c49f866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 152
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f6035141-edd9-43da-a409-c9431f845f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 32
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b3784549-6290-4fd2-a017-c307db019646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "effb72c1-eb46-4805-9b78-289e8d916826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 108,
                "y": 32
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c17672b0-84de-4003-93aa-b94a2d383349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7000422d-e372-4fcd-a5ea-cfa297df5660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a42f9dc-9337-4ef1-9393-73fbf69c665d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 141,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "13545a7b-34b3-4dda-be4b-e75a7a6377e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 206,
                "y": 92
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9adafa2c-0638-4fd2-8198-45fb48586a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 216,
                "y": 92
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dcd4b78f-7c63-42cc-af2b-fb817fa732e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 174,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "89f4a595-3bd6-4095-9eeb-b017100ec034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 32
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bbde55f1-c2b9-4047-9bdf-1a33daa5e3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1a1842a0-9689-408a-9de5-1cc5daacd614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 32
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b4127873-46d7-4904-8269-ccda23195fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 32
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c5dd2a6e-6545-443a-9976-6405be6734ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 122
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dde10600-8a99-413e-9060-8d5dbe219905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 220,
                "y": 182
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "698b3b1c-8f20-4b10-ade2-920e2825711a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 32
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "719bcba6-5344-44cb-8153-5f24b65f9fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 241,
                "y": 182
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b2604139-bef0-44a5-8a06-ca4bf1e21884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 122
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "71851816-1342-48cd-8a8e-69dae4ce2c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7b6b0ddf-94af-4616-b184-189120fe7ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 234,
                "y": 182
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fa2f2910-f225-4416-a73a-fd20aa84acf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 152
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b9332972-118d-4c25-9e03-83d75ba0d0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 152
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "12a8f904-b28c-4d0b-8ae8-bb9885dad6a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "98ad1dc4-1d06-49cf-bded-cc59f3f51cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 173,
                "y": 152
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ffee2add-ccdc-48d7-bf6c-7ec23f189953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 244,
                "y": 122
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ab8422ba-2dab-4896-ae4b-bff0ae9ab75d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 162,
                "y": 182
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "06c826e3-35aa-4a00-9916-ea267b171881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 182,
                "y": 152
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "80d0f4ff-4790-44ec-8b1f-2a5c64d19301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 218,
                "y": 152
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "64b751c0-3308-4c7c-94c4-2bd8531cdfd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 75,
                "y": 212
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a6b25583-4310-4db7-ad51-3f27b5eb82c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 9,
                "y": 212
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1b96ba60-17aa-4b18-9ec8-528a34241528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 182
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "98a64185-d661-4f11-93cc-8feb96755733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 79,
                "y": 212
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "686a0d96-3073-4862-a128-8e943eb7f90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "af41e35c-6f34-4505-a36e-00486064230d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 182
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "921a9b4b-747f-44a0-acda-8ebe09aaad3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 182
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "853a8da7-9a48-482f-8df0-f88407f60656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 182
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "74c709ca-ae96-40bb-aed5-d2d592d1f0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 235,
                "y": 122
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ce2d220e-21e5-4cc4-8735-8714ddfbc385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 245,
                "y": 152
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1f534a39-2c41-403e-9c22-3cd173de9e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 236,
                "y": 152
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "07e91196-bcb0-4473-9745-eed08e724cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 122,
                "y": 182
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "59d9c156-2a49-48a2-b91d-835c0f5ce9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 200,
                "y": 152
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d2a001f6-865f-430a-a9f6-868f64c5dceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ff6b46ea-271a-473d-985b-24f79193d295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d58543dd-1473-437c-9c9c-40398673ac6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 92
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f3f53fbc-f051-46c1-89e0-242d5c9bd377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 86,
                "y": 92
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "45831325-eb36-46dd-8d2f-8edb1a049784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 209,
                "y": 152
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3a77da95-fd5b-4cb0-8c3d-1a8013fac962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bbbdc993-9e57-4e31-9ec5-c92e93e9ac29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 33,
                "y": 212
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "94cbf0bd-b01a-47ce-83d8-a7c01ce43f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1d38e0fc-80bd-47c8-b8ad-c80663a9400d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "a78ccb8c-ea48-4b9f-86bb-b739f08ebf9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 28,
                "offset": 0,
                "shift": 2,
                "w": 0,
                "x": 87,
                "y": 212
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "1e381239-0650-42ee-9aa5-f30d1cc7f299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 58,
                "y": 212
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "c7f22410-0f12-4085-a865-51930cca7c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 227,
                "y": 152
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "fb24faf5-a3bb-4007-af18-0b8677ac1a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "fa508dc9-b41a-4111-915c-6fc0b8320f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "eefc672e-8f75-43e4-8475-7824632e83d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 62
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "231ca4c8-a065-482e-babc-56efa45d462d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 43,
                "y": 212
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "1f8b300b-65d7-4595-a684-148ddf3c1722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 92
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "a43b1056-3238-429a-a39a-9ac06e533c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 28,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 82,
                "y": 182
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "14171aae-cd86-4301-873c-e7c930aaaf46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "eb24115e-a785-43be-85e8-0293f37633f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 182
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "2c99fc33-794e-4530-b92b-95ff09916b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "66eba119-d1a2-458c-8498-bd9040501e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 62
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "5e0934f8-0f69-4c19-8c7f-9f4fcf6a3d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "2c9af4da-218a-4af9-91e4-0f33cc44f83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "2f4f881d-0e33-4e99-9023-af0b8c36df04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 28,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 170,
                "y": 182
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "e1ecb5c1-ef5d-4c7a-9bff-cc3f69d21060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 154,
                "y": 182
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "c3b5c653-0c89-4320-bd9d-fc0723e55bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 32
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "286550dc-58b7-416b-8359-147a17f14865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 122
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "65f14960-ee70-4f35-9bde-1b9dd8db3863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 122
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "8617ea7a-0581-4093-b679-d54126a7328c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 28,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 248,
                "y": 182
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "debbc308-798d-4574-8107-924f80a7722a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 127,
                "y": 122
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "5e678404-bcf8-45c1-81cf-2f77edba6e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 62
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "69a5924a-dd83-4932-b098-9766d733a3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 71,
                "y": 212
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "e211aa33-5c3f-4b20-98dd-d1416c2919ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 28,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 2,
                "y": 212
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "4166097d-13d7-465b-8845-6aac8561870a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 15,
                "y": 212
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "2bbc601d-1f07-4186-80f7-1576046c5dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 182
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "efff3afb-c2b0-4993-8c45-4371b9561bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 32
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "10f44f74-94d5-47cc-8c5b-2e6aa49fe148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "10ee03df-e4f8-4ff1-98e3-5d867552a901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "891e3c41-44aa-44a5-822b-0d8e8c545586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "667cec79-6f2d-436f-9ee6-faea55bd7bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 92
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "102634cc-fc3b-48f9-8b5e-2d290bc65ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 32
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "37c8739e-3b05-425e-b5a9-1645588525ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 196,
                "y": 32
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "0a047d53-9ee9-45c1-801b-6c30b92c8843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 163,
                "y": 32
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "561b985d-e9e7-4a61-8e84-834fbf80e822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 62
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "b5b16ddb-ef45-454e-a58a-be7d017adc26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 32
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "c3a9d627-ea61-4f47-abbf-bf0bfc4a1c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "b68bd3f2-1815-4879-a6d3-d70b49f0c5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "c41e40d8-8ea5-4092-b94e-096d99d75c78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 156,
                "y": 92
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "81840694-af63-4e0d-a523-f40e1edf56fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 208,
                "y": 122
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "5d8ea017-e42f-43ea-8904-e5c96c43bb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 199,
                "y": 122
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "b7ee36f8-4f9c-4a62-87c3-e574f07144a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 190,
                "y": 122
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "4bbc354d-5943-4fb8-9115-e7b1833d0ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 181,
                "y": 122
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "06b2163f-6ec2-4f84-859e-816a721cd953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 28,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 199,
                "y": 182
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "d2d3bc52-9d6d-40b5-9b38-b27054cb3f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 227,
                "y": 182
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "c81f9d5e-294b-4777-90a3-02d2166011d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 138,
                "y": 182
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "99ad55a8-7d0c-438f-b12b-433285fb0611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 130,
                "y": 182
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "705349d1-02bc-4744-bc1d-c5ded8abba7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "7ddecb5c-1910-4acb-9a64-f16f9d70c725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 62
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "99112f66-79f7-4110-8e29-c06fbdc82a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "ef4e602e-e09a-4ae5-80d0-2e00ea4f633a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "88b764e3-431c-4557-baa5-0d611b14add1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 62
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "72bee20e-dc64-4119-a9af-e4d9eba6844e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "62b294dc-c0e2-406e-88ab-b50b8d13b07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 92
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "4ebfe6f6-9939-43a9-bcc9-2928aeb4fdef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 28,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 217,
                "y": 122
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "b6254618-95f3-4487-94f5-b571eaf4296e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "bcd30705-9d33-4cb4-a050-33b0ec7d08f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "8c5692d7-3800-44bd-aa37-a788bfafd827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "e8b938e5-8d13-48d8-995a-1b540603ec55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 62
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "70cb3dea-bb7f-4456-974c-8dc5ed44b01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "fbfe870a-1aa1-44c8-9d22-d18d0665faeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "56c63284-74e7-4624-a006-67b5bf955543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 28,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 186,
                "y": 92
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "72bbd7fa-dbb4-48ff-ac10-0a5a9b97dba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 172,
                "y": 122
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "15b6dbae-4ba2-4cbe-8cfb-16e07c1beea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 163,
                "y": 122
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "082ac7f9-3133-41cf-8538-6c50cefeb245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 122
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "5d4de301-37d5-4e4a-b9c3-623bb7e319bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 91,
                "y": 122
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "d5d3f25a-67c2-4f9c-a987-8c0a10282566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "32bcfaa2-5bac-4680-807d-339a417aab1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 191,
                "y": 152
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "5af2da96-93a3-4800-8344-b022e8d55e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 122
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "30d4ac2b-ac67-481a-b530-d3da98f78cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "22851034-6885-4a11-93cf-822f977ed0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 145,
                "y": 122
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "ac2804fd-308b-450e-91b5-dea6fb28bb56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "0629f9f1-b777-4d97-8de1-512fad060cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 136,
                "y": 122
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "6649af32-9517-4146-841d-3404ea94e022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 122
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "acd49e54-9922-4142-9f78-d20fb83d86ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 182
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "1c627ba8-dc62-430c-9e1a-f4454d71cb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 28,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 178,
                "y": 182
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "757a862f-8003-443f-99a4-10ea245ea5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 28,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 185,
                "y": 182
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "051db38a-374b-4746-9043-e88a8da570d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 28,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 146,
                "y": 182
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "00b9ccfb-ed98-4571-9d0e-e4b56a7335f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 28,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 106,
                "y": 182
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "7d68c466-3c3d-4c5b-80bb-aa9d972b7b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 62
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "57ff5b75-99fd-4a48-8aa2-6aeeabc68fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 152
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "5d7788f1-8a2c-4a9a-ab3c-6311a5160245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 152
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "17399104-2976-4dca-90d4-359d8801422f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 152
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "7128263d-24b8-498c-a9e0-74237ae62b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 152
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "f1d441b4-fa13-4028-a39b-070fbf013cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 152
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "3c1188cd-b771-46a9-987f-adc407f3e889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 128,
                "y": 152
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "8a29b855-88df-4da5-a7b2-b38e65529d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 130,
                "y": 32
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "3af54032-af32-43bb-9c07-aa6ce0eb90d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 92
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "3d8d8197-5066-4a79-aee9-fcf270c3f5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 137,
                "y": 152
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "9821345d-4a3c-4ec3-8bcd-dd929d3dbaf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 152
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "0dcced93-2229-4937-83f3-db373a11ff2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 155,
                "y": 152
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "d2288617-6163-4487-bd39-4b86fe5eaf2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 164,
                "y": 152
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "73ff1d23-ebfb-4993-bb7d-9965a62ca575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 92
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "399ef0c4-d1d0-4f72-a068-1c175f9d9d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 182
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "ea44c848-2022-4ed0-9ad7-7ff6b9fc13d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 146,
                "y": 92
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}