{
    "id": "766d1fd5-39a5-4de5-96a8-b4a52316d6f2",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_final",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3f25f935-ef09-4dec-869c-7fefce2ab7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 203,
                "y": 113
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2da3e8d7-319b-4c90-b6a0-fed9c0b251c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 12,
                "y": 150
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a0fd9e38-57f1-44fc-b02b-f51d55669c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 158,
                "y": 113
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "36ba8781-98ed-47ea-9194-2bada3bd7428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7d1b7634-0c46-4efb-b8ec-5426c75fc877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 201,
                "y": 39
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "849b6261-5ecd-4d59-a0da-23434c69871c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9ff18cb1-4e88-4c61-9433-9a4b5cffaff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a97104d7-bef4-4e8e-a339-b67dd5a930a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 7,
                "y": 150
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "614e8347-2c96-46e2-a06e-2b43cc9ed6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 219,
                "y": 113
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "076feb2f-3573-4c74-acfd-e758404f9203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 176,
                "y": 113
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a6d1bf38-30f2-4030-b4e1-0864aec6c59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 165,
                "y": 39
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "139f1046-72ec-463a-a4a2-035d3920b2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 39
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cbf83238-4001-43f7-a04a-207afc1dacef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 17,
                "y": 150
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0fafe4dc-c1fe-4e0b-835d-16d3582baa43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 149,
                "y": 113
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2e3689fa-7cd3-48b1-9a36-8f3ea45947f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 22,
                "y": 150
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6b6ac731-b2c5-42e7-ab39-863135aa7ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4a4ae05f-4d54-4c82-bf80-b2c2bbabb437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "69f178a8-8a23-4b6e-830f-60a2a80f6116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 37,
                "y": 150
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "06feb87c-5f56-41a0-bf3e-1846f9e7f956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 76
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d9f5ab33-f296-4b80-bc01-0f8c2540b8a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 76
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f4c27d01-a6f9-40c4-94da-253f573af78d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 39
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "15266c5e-5c6a-4cce-a2f6-703c05e78830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 39
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "417adb40-13d0-4efc-bde1-8cc878cd7fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 93,
                "y": 39
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "51ca1c35-ed83-4143-af88-e504869b48f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 113
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "045270b7-76f0-4b9b-a48f-a0c7fe928693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 110,
                "y": 76
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "496753d3-1fa2-4a8f-94e5-6bb7eb0e4539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 76
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a90ed985-382c-4013-adc5-e277bbe7e4a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f051478f-cc28-4fb7-a228-fdeeabebad05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 241,
                "y": 113
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b9dbff08-8fee-4761-8de9-b18663c03469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "85d1101c-febd-4dd0-b9ae-09f6c81a134e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 41,
                "y": 39
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "87f4b64d-a04b-4887-b952-2d492257f9e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 109,
                "y": 113
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "42b2e865-1b91-48e9-9a05-59cd7caa1a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 113
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2244327d-75b4-430d-b03f-e3fe8b62dd78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9faed9f0-df04-4e1d-89a1-2d79d4952dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "be01dcc0-f7c4-487a-906d-3000c6690d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 76
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0bfcee47-3b0f-47cb-948a-5faa2ac03ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 76
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "50eb06fb-3194-446f-8de9-ac5a2b4dd80e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 74,
                "y": 76
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bcf6aef6-191d-40f8-8bcd-66ea06f78dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 113
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4f4e97a4-e212-4afd-b883-193a3bd2b4bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 119,
                "y": 113
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "071f900a-bcca-4980-922a-46e67f5c96bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 76
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "292486e4-bc87-4b76-867f-cd2ffacbf7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "eb7e2f25-c098-4cfc-adb9-24f5fe9d50bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 27,
                "y": 150
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3e32f9f0-1056-47a5-9165-4a5d31d32454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 113
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ad205dc2-c5db-4cd3-a03d-4f49674cd1f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "483379c3-3aae-4cc0-858a-93d9de6ad9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 113
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fa53aa89-550b-4576-b1c5-198464f582f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "14bce374-7599-42d2-a993-905f8df48631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 105,
                "y": 39
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6e352cf0-3c3e-4689-bacc-4758b94f7080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c23ec4ca-cf99-4235-bcfa-d351ae3fcd34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 117,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c7c82c9f-6377-4f6c-bb4f-f5b9684f6737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "dae4d04a-2608-4807-8de4-5f7fa64cbad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1b799c0c-a6bd-4b37-ae4c-8e26c95193f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 129,
                "y": 39
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bea9f6fb-c474-434f-8869-7b3a08f7c8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 141,
                "y": 39
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d157f27d-d5d2-40cd-b72a-e7812ed1eb63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 153,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f6784b96-8325-4c53-9281-99cd9a4f3a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c1666def-dcec-4e81-9fbe-479dc2073234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "43f24ea8-fd19-49a0-819c-6a727dbbda50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "31f1f07c-afa1-479a-b4f8-a9bbc65cba1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "07276297-602d-4a70-99a9-f7848f42775c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 76
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1cbefd41-c917-44da-8e8b-4d6b40df3e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 113
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "348da7e8-48b2-439f-a8fe-dcd7a6aa3c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ab206606-e47b-4b70-a36f-03d7d99dbb26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 211,
                "y": 113
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "de02d5d1-fccf-4939-871e-2b244799a1ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 189,
                "y": 39
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "289a45f9-b631-433d-9d12-2b7f060c6a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 15,
                "y": 39
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ba126fd4-2e2f-47b1-92b5-d0d5ac1f3108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 4,
                "shift": 12,
                "w": 5,
                "x": 234,
                "y": 113
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "42f8d034-1e68-424a-abdd-f8a66dc31a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 133,
                "y": 76
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3c5e9a53-7c89-4640-8300-f7ac19ace39a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 144,
                "y": 76
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a7c5d729-0017-44fb-8aa1-fbc8cdb3f5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 243,
                "y": 76
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3b5e643c-b15f-42a1-a550-0465b10c1dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 232,
                "y": 76
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fa7f6783-8604-4e6e-b80c-f69fa2afdc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 155,
                "y": 76
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "696c58f7-a26f-4af7-bc8c-36f5e4e006a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 185,
                "y": 113
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5ca8d2d1-1946-4bd5-becb-00245d7ec36f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 166,
                "y": 76
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "30b60c05-583c-4043-9771-343c970f8375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 177,
                "y": 76
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "19acb116-bf87-4a53-8064-a16b14f03a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 247,
                "y": 113
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7c87c044-c310-4503-9a48-3d5a5fda2985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 227,
                "y": 113
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "376b158b-6e4e-4a83-aa57-3c61ba0b8f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 210,
                "y": 76
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a449d33a-4570-4ccb-b77c-54691a88d0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 150
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "39e3c033-51b9-4f7e-b1d9-ffed894055d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9af1e956-ae9a-4470-a332-02382694dfea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "457b3e14-d455-4825-b286-e128a2206f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 221,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4a12e9b0-d4df-4bc2-ac24-992074936c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 76
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ad360950-f63e-44e2-82c0-faef7f7cea23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 188,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6b11f30f-fb8b-466b-b483-822e5353aefa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 99,
                "y": 113
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9c578535-79a1-45de-ba0d-51262ee62359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 89,
                "y": 113
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "67870085-ba93-44aa-aa1a-e0a00fe7869d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 167,
                "y": 113
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f23e0b33-244c-477c-88b6-e09f95e20a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 122,
                "y": 76
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c4e196cf-17e4-4d15-98e1-91c0dfdc6459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 213,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "34817cc2-e874-4351-bfe3-f3bcfefb6e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "09a07084-9d56-4280-a504-3d33a8ed00a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 225,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d48061f6-1a86-4a50-8174-f2765f0c6159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ea1a74f8-0cb8-4c52-bdcb-40229aa012bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 113
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "de9ad580-d4e1-4a75-a8c4-417b36aa1081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 113
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6325201-0c5d-48d0-bc55-a6eb8074150c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 42,
                "y": 150
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bbecaecb-4aad-44cc-9dc7-9401f21c0150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 113
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "be16a754-1df9-4a36-b2f0-0ab78846c9a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 54,
                "y": 39
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}