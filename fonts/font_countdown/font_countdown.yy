{
    "id": "1bc286ae-8f16-43ff-96df-b0dcfd625e3a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_countdown",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fe2a8c5b-13f4-49c3-af62-4d3699236ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 32,
                "y": 102
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "27b9750f-6cd4-4ff5-bfdb-b1cc49c7a56a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 18,
                "y": 102
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3def3c74-3611-4aee-b61d-ce90450baccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 167,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "42d566ad-beb1-4c9f-80de-25e01f41112e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 204,
                "y": 27
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "62b2cd2e-8f24-4540-a9f2-b55f3cd47e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 217,
                "y": 27
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3f40c506-687d-4686-a0c8-f78f3f7e1640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fb4d3b0f-5e07-46ea-8212-877d626f1a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2186b432-1ec3-4cb7-ae16-184617715e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 107,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b01514f8-75dd-4295-9ee6-4dc8e48b53f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 102
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f76e8121-5f07-45b1-bc86-3d8a3862e1ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 240,
                "y": 77
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7a217811-d5fb-45a9-8bf8-89507877f101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 195,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b302fb92-207d-4ae1-a01c-d3327722b8fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 77
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c13805b8-3e04-4c0d-a9ed-95be8bd1a0fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 25,
                "y": 102
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6ed2fcb2-4ffc-4111-b4fc-a770083a721b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 102
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d0f9a326-15fd-4bf4-bcf4-8c4220d0a99d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 39,
                "y": 102
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c2885dda-25f2-43e1-a0b8-db9fefbdb144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 46,
                "y": 102
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "14845687-b387-4584-9f0c-d2255d606b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "95456d98-4a4d-4a56-a311-6513e1e0bbff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 177,
                "y": 77
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "13e8fad0-8cae-4c74-bff2-555da472f04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 77
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "19dc97b0-0901-4cc9-a587-cd2a5e677833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e06f66d2-e689-4806-818c-a6d45ffa6828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f6b2a0d8-a2cf-4ef6-85b9-baf3d37b2e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "370707a2-01e1-4306-8568-a3f4105f6d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 93,
                "y": 52
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ac3afe5c-92e1-4174-b54e-8c16849a1f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e1099657-3180-431f-a6f3-e66b12746924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 178,
                "y": 27
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "db62e0da-24de-4930-bdac-af4c5897fba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ab193e82-87b2-41c4-9302-501c14ec70c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 53,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4233621a-8686-453a-a53e-173ea3feba65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 60,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1d01a3d3-7f3a-4c69-ab5c-396ebef15e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "77e0a2d1-3a8a-4dbc-9140-6d9f0c4a2360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "63d59e95-586f-474b-99b3-187b62683e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cd80e0c9-5c4a-4ea4-bb9c-83134eeabf63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "849ca63d-bd76-4fec-84d7-b635dc6a504b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "37b10fcd-6cc1-4933-a4a5-3828186573c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8f17e5e5-fab1-418e-ae22-6e4d5168e506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 113,
                "y": 27
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8c9dd184-78ba-4fcf-b255-5ced8ba6eb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9fd037f3-4263-4428-9214-a5fd2f12980f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 100,
                "y": 27
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "03f391ec-58ef-4a56-9f4e-c86e19a04654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1474777a-62f8-4be8-8377-18b16cb10f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "32180802-5ce6-4a9e-9496-dba0923144e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "981be41f-4678-4a8d-8e99-58a1bf9bb18b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 27
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "75fbb2d2-f694-4862-89aa-26659bb0abe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 113,
                "y": 102
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9866db02-19f5-4378-ba06-715fadeedb2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a6290e50-736a-45f4-8103-72d3f7fcbf3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f16e5147-3ec8-436e-b948-08d90ae1a5a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5e262cc8-daa9-4e89-b463-9ec76dac3d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "911868d6-84bc-4018-9f98-87c8b95f9ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 27
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fbfc88fa-730c-4576-aceb-7b4779b4841e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "32792cba-25ad-4d9c-941b-6c956bc6e176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 77
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "665da0ca-d92e-4167-8eef-fe79a301aa09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c936db90-0db3-4790-b915-a7c149ef5238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 27
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f01a67ed-f415-4b31-a251-1086ec22249d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 52
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "249a07db-1d77-47de-b49d-fa09173890ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 72,
                "y": 27
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9085d417-05c3-4ae0-af96-24b807460279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 27
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c13731b5-ad84-469a-ba63-520f5deceeb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5b4a460c-3b47-4bf1-8d14-19a1981790f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "564a9dee-866c-4d14-af64-127e5998f1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "36457d79-6326-414f-bb5d-17d7a60106f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "084015fb-f92a-4463-8964-cb9008bc2a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 27
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6ab78ef1-ccc9-4c65-b308-4c40170624e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 102
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fed37b3d-f22d-4a2b-beee-4f97d4c69d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 74,
                "y": 102
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ac21d0a8-79f2-4421-9146-76b81f5397c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 67,
                "y": 102
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4c4a87b5-1b88-43ba-831d-123f00c933a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 166,
                "y": 52
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "58fac17d-9983-46fc-89e4-2fe236db3b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 178,
                "y": 52
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c12a1212-37db-406f-8f4f-56ab1a48eefe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 102
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "239651ed-8736-4303-bff8-3229ef21187f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 126,
                "y": 27
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "13c9abcd-7ec3-4aa7-9ac2-04800c2e39b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 139,
                "y": 27
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9de535af-5eae-4151-a3a1-442685731ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 152,
                "y": 27
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d11fe7a3-9b13-40c9-96a1-45ced6dd7ceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 106,
                "y": 52
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c0216dbd-4ddf-42af-8aa9-aaf5e05aeb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 165,
                "y": 27
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c3236c6c-b468-4d50-9caf-fbd156b08fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 231,
                "y": 77
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d171c165-e693-4c5e-b267-656c4886947d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 190,
                "y": 52
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f3e5f0e7-41b4-40db-9198-c2bffd439aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 214,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3edaa589-77e1-4809-b90f-a90c190527b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 119,
                "y": 102
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0bd0469b-7452-4fbc-a34f-99f2683e0e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "14d50223-7ffa-49cb-81bb-8c71940c876b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5061274b-be77-46b9-8e81-f9f094d31868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 95,
                "y": 102
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "39073b52-c581-4f2e-b660-f2fea317ac7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d2e55e24-ac45-45da-bd40-d36c6140417c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 226,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5af07eda-4b26-45e7-b17e-65fb18e7d0c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "54ef3c88-21a6-4597-8b45-469db97ecdc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 230,
                "y": 27
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a33a23bd-1049-4ec0-a472-c399feec39a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 118,
                "y": 52
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e635da06-55e5-4349-945a-0261f12f9387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 186,
                "y": 77
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4b956c78-35d5-405c-8662-5ab98be93b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 130,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e76448b9-e8c4-485b-98c1-66d35bcddea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 77
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "190bf90c-978f-4b11-b18d-d6b536428dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 142,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "420a94f3-251e-4d5b-8fd2-c45e739d4c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 52
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3900af76-0d43-44bd-b96a-4a6f9fbf0288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "37ab2fc2-7a6e-4abf-a636-629f59d1888e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 191,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fca4fe61-2731-44f8-8c57-545e75d50d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 52
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "61aff7bb-bb22-43d6-8741-e111ef5447f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 77
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e21cd905-eb6f-428f-ad5d-4a0517fefd8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 222,
                "y": 77
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3559e044-533c-4f92-b1e8-ef267ee70f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 125,
                "y": 102
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "16425c80-8f30-40d1-b449-98907e616432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 204,
                "y": 77
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "10e79cde-f33b-4bda-b9e4-63d05a4357ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 238,
                "y": 52
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "619502d8-57de-4e52-b6fd-96c2bce9a199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "22a38fe7-1ca9-4d7d-b4ec-294d2c572bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "0fcd9798-70e6-47ad-b3c0-451f1194ac96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "b883575e-0e09-4deb-b0d8-7e1f874159c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "fb6a79c9-945e-4cdb-a6ba-e28d8d410fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "e1ff7ef4-e445-4c49-a661-e90d149e8908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "8807160d-e467-4b0e-903c-dc7f78f0e948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "d4aefd7b-3eae-49d5-aa85-f4a68943348e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "9fb34288-335f-41c5-9f37-4b1d04de39f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "9951f04d-a398-4d7a-9ef7-705b3e2dfd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "6ed01081-c712-4ecf-a063-d5bf9a6b3dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "885de845-70a5-475d-89bf-e00116ba6571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "a1617040-18ee-4f15-aacb-f4a2545950ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c01cebb5-4a1f-4807-bbca-b225d80f416e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2fcf2d9d-27df-446a-8ddc-d0bd2db32064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "f7b390bc-a71d-41f5-afb2-13aea69e42f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "6d93f8ab-9bc3-4017-aaa1-3d51bf2bc7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "d2551cda-5a94-4b8e-a6fd-d14d73f7577c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "2b5045ab-4dbe-4d73-86fc-734c73056904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "ae9360c1-e9d2-4e0e-acd4-5357b710c15a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "888a63fe-d71e-49f5-b998-1d02e75a19cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "5987b2e8-8510-448c-86ce-a853e2b1ca2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "ecb24ae1-d8b6-4239-bbe8-460688ec1fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "9182688f-fada-4741-a291-e76c2ff342ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "b5a98ac7-0890-4aec-b7d7-76b7155aeb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "8b445a66-574a-470f-9a43-4345b18aa308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "aa7a3ebf-e3e4-4c46-963c-8fbdb78bc3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "8cab4ae6-ee3e-4230-9ae1-60049d159dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f72dfc05-2629-4bb0-b4b3-afdeb4063120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "b5fb60a5-b43d-4431-b42a-6d826bb1050b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "577b501b-0e21-407b-a308-61ed08cf6bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "33b3f5ad-728c-48a1-b82d-ba19263f3a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "8562f606-e6a2-40b0-8e67-bbd6c854d31a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "11b43963-cd38-444b-92be-57bfa3be8a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "f96e8935-7ae8-4a9a-84a8-15a172a489e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "61a0a293-3c77-48e3-a820-4accc2bc5362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "04aaed3c-6349-42fa-a661-dc6b85cf65ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "44e4ea71-c81d-4d95-bcae-c3b4de4a14f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "11c36cb0-c601-47d3-ae18-01fbe8e99ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "3c2ddad4-3674-4586-a60b-7a110fba5c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "0c726521-95ad-4b25-8d49-a209196b19c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "ccd29d55-43b7-4418-b15a-a2110794d8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "3124146d-e7b2-4a00-a448-2f1aa7f57b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "577c9a00-86da-447a-9b09-7a84765a5ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "c70c9485-e977-487d-a712-336ce7aae52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "c2a307a2-fb8e-4eb5-a926-f624a47d2e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "5a010d6a-5f6a-4c58-8560-1d9e4666f59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "4e01ff68-c8db-4bc3-9b38-fc82a747d100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "bd233441-0494-434f-93ee-c871e02388bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "dc7111e1-0d06-458a-ab5f-43f5208c0fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "3db3b34c-d309-442c-a05f-fdd543e6a6be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "1afd5fcc-77fc-4519-adcc-7c53a3ead8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "810eb2d8-5642-48a0-9dad-c8024f410604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "09aebd41-c96d-4bc5-bd56-bf08826874fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "230753bd-eeda-4adf-abbc-559dd6732f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "85792273-9f76-48a8-800f-b152e100b2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b96224f7-9e32-4c3e-a6ce-7d493f89e2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "ad144e54-2a3d-4937-9f5b-48d7570a38e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "4ed4f91a-62f8-4506-b923-8b8d9b2e0f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "9398e649-a546-450e-b3f8-91cdff5e95e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "c8e02c64-7feb-4492-832c-3f913cf22169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "899ee76e-f480-433c-a6b9-98eff8855405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "3822c2d1-bd33-40c8-9edd-c4406b37d48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "8847fd6e-af95-4b2f-b70b-79401c18b02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "eb1392d1-f337-4420-8027-ae6d75dbc301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "04e75a37-b666-46a0-9a17-a1dc37b342bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "75af7b47-a458-4675-b74c-28d30eaa6429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 32
        },
        {
            "id": "efec176f-42ec-4f7b-8e66-1a184dde755c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 33
        },
        {
            "id": "bf7544bf-c122-49fe-95f7-44c9e0dbe25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "d35fb159-9495-48f7-b60b-8c66c04a39ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "c6f6fa62-b507-474d-a4c8-0f593682e33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "784b4801-2aff-41eb-883f-7ade2984a306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "d5a8b448-888a-44b6-a8d0-a90c656edf22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "ab53b6e4-7589-43c7-b860-e928d58d6f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "a747a701-9f83-445c-b6fb-e788cade248c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "5d9ee997-cb98-4b26-980f-167203857290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 109
        },
        {
            "id": "ffbd49d1-3105-4e46-b3d7-313c7cb5ee74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 110
        },
        {
            "id": "e922cf88-a568-4d05-82cd-b769b0b679e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "454b79ed-5657-4840-b39f-65078df4f55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "2858bd5f-0b85-4944-a921-314f7a10b5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "3c0bda06-600f-4ee1-be64-dcf42e619ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "cba687bf-797e-426b-a362-0c501c90bfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "4146882a-3588-4ff5-9e6e-9c7b3b7570ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "0ea28d1f-2423-44b4-ac4f-349f2524aca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "37419c6d-ca65-49f5-927c-d7edf3374dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "97e0b096-435d-401e-9528-1ea5785d8ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Black",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}