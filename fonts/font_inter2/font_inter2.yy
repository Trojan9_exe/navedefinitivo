{
    "id": "76b24481-8bfd-4c49-9c9a-7afd6b9c87f3",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_inter2",
    "AntiAlias": 1,
    "TTFName": "fonts\\font_inter2\\spyagencyv3.ttf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "607ed2f2-4b7f-448d-8fd7-57a55fa102dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 204,
                "y": 166
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7c3f6e7e-c706-4109-9527-2ff5b4570669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 274,
                "y": 166
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "856da06a-fe89-4411-9202-ae8ae20b5e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 56,
                "y": 166
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a733d1b9-0700-4d7d-ab76-1bf550a374b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "04ace601-b1cb-4c06-894c-5f22951a1953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 478,
                "y": 43
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2a50be14-fb00-42dc-84b7-d20cee3f9af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "82c73abe-9a75-4d5c-95aa-45f27f19c6ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6149cfe1-2dbb-4df3-8804-880e230d69ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 279,
                "y": 166
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "92fc69d2-ad7a-4bc5-a8f8-59638df7dc84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 169,
                "y": 166
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4f5e7097-b24d-4dcb-b16d-8452d526b751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 220,
                "y": 166
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e7a282a5-671e-422d-9a9f-240b645d2131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 491,
                "y": 43
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "91a9f99a-28a8-4037-8a2d-5d83af0fabcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 200,
                "y": 43
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "970c6201-3599-496c-b980-c1dda6f95a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 250,
                "y": 166
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a8cec75f-4f40-45ae-944e-77bff26a8056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 46,
                "y": 166
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "956a8162-3684-4039-b023-25592c60bbe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 284,
                "y": 166
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "78f23fd6-63f0-4d54-b1e3-aa5d752fbe11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5bf69dd7-d75e-47d8-944e-753e45bcc557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8ed4e4a5-3913-454e-ac31-e10d2fc6c5bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 268,
                "y": 166
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "391e55dd-7d03-49b9-8fba-53996a61e256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 84
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "641cdb04-374d-4520-9d48-051bf5ad0d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 28,
                "y": 84
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ade78ec5-5266-42ff-aad9-0f2370319da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 144,
                "y": 43
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "023ebd39-c7c4-4814-ae3c-d5c5fc0d60f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 41,
                "y": 84
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d2e15e90-fee3-4c01-b2d9-77821367d62e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 206,
                "y": 125
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c20cdceb-890b-4538-bf73-467bc47ab7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ed512c2d-9c0f-4178-b2a2-969eadfc4cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 158,
                "y": 43
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "646caecc-1978-41f7-86f3-30a7e00a0e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 67,
                "y": 84
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "215b2400-3a6a-4d4e-a8ba-c6d8bc8c7700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 319,
                "y": 166
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b375ed34-61bd-4695-87cf-6b16e1a457b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 262,
                "y": 166
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0b4def70-f885-4df0-900b-e079bcf08edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 406,
                "y": 125
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7953c765-4f2f-427b-8f0f-89f0c5d4bdf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 256,
                "y": 43
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0a922680-7686-4a2e-b6d7-9d4f90343a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 182,
                "y": 125
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "08a34f6b-9712-48b7-9094-81afd5c140d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 84
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b65bb1b1-7ea1-4b8b-98fc-760184fe7060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c2426e57-af7f-4892-b0d9-25c8e0b311ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 228,
                "y": 43
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ee35cb77-a67e-4b9f-9cd9-69131e93e98c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 106,
                "y": 84
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "51bae092-3de0-498a-909e-f27efb4d86e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 119,
                "y": 84
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "319c61e2-5101-4421-8571-d20d04b665b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 132,
                "y": 84
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c0f02a98-6307-468c-b0ab-9433dd44a71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 384,
                "y": 125
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "85734eae-4e6e-4b1c-9862-b8f62497b7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 373,
                "y": 125
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "09626859-f24c-4d46-862e-1d046af0477e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 158,
                "y": 84
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "60186e9c-a8ff-40fc-afe8-838af5f6ae2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 171,
                "y": 84
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2b27ffe5-af22-4a82-9912-194125cd8470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 289,
                "y": 166
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "94ca543d-982c-4410-8e9f-006edee2a31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 14,
                "y": 125
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8d10384a-fd81-4cf9-ada1-814f3d43cbd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 84
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "90714c02-57d4-495a-9e9d-18684cf14801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 362,
                "y": 125
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "69eb9bdd-b5f9-4d37-a0c2-fac2a078610d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "58c74d01-a5e3-4519-94cd-a3c1fa47b31a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 262,
                "y": 84
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "43b49d9f-4911-46f9-93e3-2e86ffb4f9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 275,
                "y": 84
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "22957dc8-e01e-45dc-9232-c3c1b7229ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 194,
                "y": 125
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "36e0d30a-68d9-49aa-92fe-806e065fe74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d65b9ef2-210c-4953-b90d-14c97a2d962b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 301,
                "y": 84
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "81b5477f-1d05-40fd-9e82-28ba48b67f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 439,
                "y": 43
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "864c9213-b260-4306-be1b-e648523df855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 314,
                "y": 84
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f8cbc8d2-1c79-498f-b7a9-564bfd8bd0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 236,
                "y": 84
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b1e3419e-324e-4f82-a3f2-4286bf92f579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 197,
                "y": 84
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dd4499f4-fc5f-448c-a626-91714bda99bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "084858dd-15dc-432a-b880-dd0e201901c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 46,
                "y": 43
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b7d4a991-0700-4b7a-815c-b7c41f71f00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2cd8b239-c541-4cf6-9237-84e9b42ff014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 452,
                "y": 43
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d921430f-f47f-4c9a-bd6d-c08d9e337034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 178,
                "y": 166
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fd695bfb-6466-43b7-9463-19c5c032a4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 289,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9d4e34e7-d28c-4ea7-b4d5-f7f9b80ab3d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 106,
                "y": 166
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ea25b94e-db47-4572-ad06-7677f001d20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 413,
                "y": 43
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "de1441fc-9c7f-44d8-8307-81e1f393d14f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 32,
                "y": 43
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6323bcb6-3b6c-48b4-9e1f-407fca904ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 115,
                "y": 166
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d20dbf75-64e3-483c-bee7-ff946e31f9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 125
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bae1a0d5-ac35-4163-a185-a4a1b4dd118c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 158,
                "y": 125
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d3edcd39-de07-4509-b49d-30ceb117b6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 125
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a4ee88f0-6ec4-4275-ac38-6a964caca359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 125
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fb623ea0-a1c5-46d0-80e4-ca1e38f6b3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 125
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "97f7c43a-e94f-4e00-9371-70a069619fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 166
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ed167555-0acc-4665-a229-07af6c1a5180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 125
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "883776fb-0f16-4801-9e7b-41c5a1bb886a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 125
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c427581a-dfc4-4dd6-b276-2907b2740169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 256,
                "y": 166
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8ab6f544-9555-4a93-b5cb-08255ead4984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 228,
                "y": 166
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0a59bc6b-ec36-4e48-a108-100ef6738e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 125
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "552a3c75-0804-4018-80c2-54b6b009d9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 294,
                "y": 166
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b4d9c602-407d-447d-af4e-7ba0dd247cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6b97ef9d-35ba-480b-b873-fa9f96842219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e5367845-53ac-41a6-8ecb-e7eaf4499d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 495,
                "y": 84
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dc55e3f1-cc26-412e-91cc-a5173f10e545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 125
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a6d8b8ec-1764-43bc-8cc6-f637c0afd9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 125
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9412e40c-3517-4bcc-9f84-f4db6b5ea112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 428,
                "y": 125
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9760f710-07b1-422b-8055-2661c7068e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 125
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a1b94f04-7cd7-40ce-907f-7a47bc093058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 166
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "89bec215-8c56-47ce-9a08-269a877d4640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 290,
                "y": 125
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a62c4b02-0d86-4d2c-9537-90dc51bc22f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 374,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "80ebe501-f4cf-4b74-b9aa-9c6e1667c769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "74e4cc07-2f0d-4f3a-8014-824990bb5c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 387,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0f0aafc6-ec9e-4c0f-982d-ad34d1aefb99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 400,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "608b105d-cdb2-41cc-84e3-1c3566419a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 166
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "edebaef2-39d2-46bb-b868-ee2ae78dc9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 417,
                "y": 125
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ccdf3dda-e44f-4cf0-9334-e967f4981eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 299,
                "y": 166
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8484bfcb-97e5-4c5a-b550-a26ad3c723db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 326,
                "y": 125
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "25951205-4e8a-4204-b836-7549ee617acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 186,
                "y": 43
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "b920244c-f232-440c-bcff-fac37387ff92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 39,
                "offset": 0,
                "shift": 3,
                "w": 0,
                "x": 324,
                "y": 166
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "1856f597-1a24-441b-9756-c557fd6bc197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 304,
                "y": 166
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "3eafa830-3f71-46a5-b92d-af3d51bb4e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 302,
                "y": 125
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "0d3d78a8-8776-4bec-8a05-28185d249aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 17,
                "y": 43
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "713acba5-ca25-484a-8783-8fda3e2c8f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "b88dbfb4-c5e3-4c97-86f0-d39fec5c9d98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "57970554-f377-4919-a2cb-da53f5ca1e7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 39,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 309,
                "y": 166
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "83d7c8ec-4d14-424c-b0bb-d2a8049d10a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 145,
                "y": 84
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "b0bc65b9-bde0-422a-9d02-991f516dbe3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 39,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 142,
                "y": 166
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "ce1590e3-3ee0-4b22-91c6-df09da4b202b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "7f5746a6-6d1d-442b-bd79-e744035b094f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 96,
                "y": 166
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "05273575-e90b-4cc6-9b7e-578f42745930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 351,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "818b457d-1e51-46fb-8cc0-5c7d353eacc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "d90ed51f-fc0b-4e31-a150-aec686a4bdbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "92e927e5-15b5-458e-996a-deb3e34afb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "098416e2-ad5a-4636-8cab-c5dd44290464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 39,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 86,
                "y": 166
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "d224bbe8-9177-4100-b399-39d86d7a68fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 395,
                "y": 125
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "6dab0e68-3190-4e54-8225-10708717a801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 102,
                "y": 43
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "a2043aec-498f-4161-b9ae-ed45788141a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 439,
                "y": 125
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "ff25018a-969b-4352-8cfc-b9ef60400f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 472,
                "y": 125
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "e2f2c346-2880-44d7-a468-483552c15534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 39,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 212,
                "y": 166
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "e5fc584d-cfe0-4d9c-81ad-ac3426710c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 363,
                "y": 84
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "1a4a4ad6-8d34-404c-8c7c-318f4f32c6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 441,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "28f8dbf7-06c6-4b73-9da0-30b67160a50e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 39,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 314,
                "y": 166
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "6160cb61-bb46-436d-96c5-e3c976a4747e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 39,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 160,
                "y": 166
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "03e1aaca-220f-45a8-b229-e949872afe6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 244,
                "y": 166
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "7f4cc3e8-ba40-4bb4-8413-44c6349ba4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 166
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "f8967b7d-64e9-4cd7-bb1d-ad02cd581ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "a1f22e85-34a2-41cb-a49a-2ce4d0aab885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "22737a76-c644-4d0a-95b1-b7b696da30df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 39,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "d6fabd57-9974-4028-ae59-5bb660cd0431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "296cc07f-332b-4960-95db-bacbef7ebe82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 322,
                "y": 43
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "69de4f02-ee8b-4fb7-b32c-aa9194511a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 242,
                "y": 43
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "52390156-5a1d-418d-be24-aee197ccfe72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 130,
                "y": 43
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "f26403a1-b2d7-4081-95c6-42df4f18543a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 116,
                "y": 43
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "c92e17ee-1f89-4a0e-9b16-4a7aa158f097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 88,
                "y": 43
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "4fbd8ae9-8e35-4521-82e0-d2116225ecc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 74,
                "y": 43
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "73bc7ba0-704f-4217-8a3c-da4bc2e9c142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 60,
                "y": 43
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "a47fcf69-e7dc-44ee-bc05-6067158170ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "04ee74c5-8914-4aa0-9bd5-edc190d6bf06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 270,
                "y": 43
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "126f9b1f-ad81-4e8a-a536-06c987863aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 483,
                "y": 125
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "b1a82e7a-4467-45c8-a018-857f8f5cd0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 35,
                "y": 166
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "4c17a64f-220e-4887-a4ca-17ef6c83cdfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 461,
                "y": 125
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "a70acbfd-faf3-4335-b789-a3389c64e958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 450,
                "y": 125
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "2aeee4f4-6ba3-4c9f-a84a-ec913a153d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 39,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 133,
                "y": 166
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "72c6bad8-877d-4666-b729-e1d93dea1f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 39,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 236,
                "y": 166
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "a16f20f3-d79e-4c12-8c2c-8c20e3e6b13f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 39,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 24,
                "y": 166
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "9a1e5c58-83f6-4578-9109-2438559d627d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 39,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 124,
                "y": 166
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "927281d1-f583-40fa-8535-85d681949c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "c2ac8e9f-92b1-455e-a32a-c2c15f6187e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 283,
                "y": 43
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "0ff6a368-ea38-4e09-afc4-b12ea923522f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 296,
                "y": 43
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "125d8b2f-991e-4bab-a9e9-7b3d766a57fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 335,
                "y": 43
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "77d449bf-455e-4220-8e3d-f884af823a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 348,
                "y": 43
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "0a052efb-786a-4d11-b004-c7a0aa10dff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 361,
                "y": 43
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "6454f0f0-d495-4244-90ca-ca01b984f5ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 309,
                "y": 43
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6855b79d-0ed8-46fa-9c7a-aa6a93a53798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 80,
                "y": 84
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "071357b1-44f5-4f09-9f86-9a46c883c7e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "31df0116-ccc9-4d7c-8873-bc8693701667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 249,
                "y": 84
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "8221d87e-4e1c-4fec-928d-2e4e259290c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 210,
                "y": 84
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "4ad6ac57-432d-488f-bf75-c7e50f1dad8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 184,
                "y": 84
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "41677869-3f69-4ab9-a5c3-49a4ed5c74eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 288,
                "y": 84
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "92470fe6-6783-4d8e-8d3c-50ebad002af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "caf86df1-383a-448c-90d3-18775882af58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 435,
                "y": 84
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "1dee4608-2312-49d6-bf7d-a3eaf4aca7e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 459,
                "y": 84
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "7943c91f-be01-4f1d-8678-d3bf2a966048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 423,
                "y": 84
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "08fbf141-1b0b-4baa-a656-3ab3c30d89a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 399,
                "y": 84
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "c5070bb1-d14d-4430-9912-da1a14fe10a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 387,
                "y": 84
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "b4dea4a7-3ec9-4211-844e-49d29f5360d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 375,
                "y": 84
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "8417f7ed-cb6c-4157-941c-d8b380df28f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 447,
                "y": 84
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "5d2e23c0-b115-4bcc-8b08-1da385ce945f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 351,
                "y": 84
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "f227bc2b-eea7-4684-ab52-8b90a52d4972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "cf722844-0146-4f86-b140-b9c8441c18e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 339,
                "y": 84
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "ec7e4f4f-dc27-44c7-b272-e70b6a3233c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 327,
                "y": 84
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "dd01e347-6521-460e-b666-62f0ca11c0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 254,
                "y": 125
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "803a267f-1b7c-4d76-8f78-94e0f730e07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 471,
                "y": 84
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "68785e7f-3817-4d67-930b-04139cda3112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 411,
                "y": 84
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "492d7cf9-8c82-44d0-badc-40aea6215256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 39,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 151,
                "y": 166
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "7da6b374-7788-4fb8-a46f-ac5c2f1b679c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 166
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "a3b2cfd4-2dfe-4d7d-aed0-a203a2e01018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 39,
                "offset": -2,
                "shift": 6,
                "w": 9,
                "x": 494,
                "y": 125
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "8fff6713-d6ab-42ea-b54b-3ff136d4099b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 39,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 187,
                "y": 166
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "2bbe4135-fbc2-4d99-bc9e-ca9e5972e71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 43
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "3db1d139-e778-4506-9ffe-dbcc6d3aa6c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 338,
                "y": 125
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "999f633e-1b11-4cec-8d1e-328ff7c5c63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 483,
                "y": 84
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "903f915a-dc90-459e-8815-6791df2dab15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 38,
                "y": 125
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "34691329-6b62-4e91-82a4-b6057fc427d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 50,
                "y": 125
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "166a4d9c-0539-4852-b075-55e769bfc495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 230,
                "y": 125
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "30821308-17c7-4d15-b9a3-318e2904cba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 110,
                "y": 125
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "cd7fc1e4-cb30-4a82-985d-180b79a8daea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 214,
                "y": 43
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "a21efbfd-d322-4ee8-9ba4-13494fb80e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "71e754e2-af00-442b-af61-ccaa507abd33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 125
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "1fb92dcb-a9f2-4459-9593-5a8d98b4364a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 266,
                "y": 125
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "547569cf-e5c3-4f02-aa83-9c3962ddcfc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 278,
                "y": 125
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "ce9c3706-1723-4f36-8991-1adb1e92b10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 350,
                "y": 125
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "406c9419-c519-48c5-9538-c9f52e9a7c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 465,
                "y": 43
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "a60f86c0-1aaa-42f2-a840-c3be3a6949a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 314,
                "y": 125
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "adb929ed-d5ac-4dc2-b010-9d8182769bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 426,
                "y": 43
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}