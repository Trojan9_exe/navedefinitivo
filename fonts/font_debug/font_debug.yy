{
    "id": "9740dc7f-c1e8-4de0-adbe-cfcd3ebea597",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_debug",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1e132120-ba90-4c84-bf5e-632071d95ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ad2fd864-49bb-488a-b359-8bd07882bf66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 171,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5b067345-a60a-4319-84a2-25bed0c00869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 215,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4c5f9765-6b27-4d37-8e81-a146f95b5b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6e7d6079-3d76-4c14-bcdc-f4975dbcd4cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "515d8e02-098b-4034-acca-2e8d879fe421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 23
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b577f686-b013-4604-b84d-b28f107b971d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "70473655-82d3-4081-822b-685ebb73494c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 176,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6b7588ce-ffaf-4f46-a224-eddd3249545e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 116,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fc902c24-5e7c-42dc-9b40-3c22a6fab7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "22457a83-1481-4057-9544-87fd2289536f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c8bb1212-03aa-4eff-ac19-9196e577889d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f6193ba0-fd79-45a1-803c-41bf4d6b2bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 132,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5086eb34-4195-43a5-a247-811ce7f8a667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 108,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "18d7b73b-b52c-4cce-9aa6-9a33c6ba7a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 160,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5e882ac9-a2ed-4040-b843-411eb126e1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "22877654-4ffe-4092-b9da-26c3e853396f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b1f13303-f844-41ef-896b-bddc34a5f146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 156,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "30f20e26-2e96-42be-a5ed-5ae0e4022d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b307fcb4-5b48-445b-8e6e-068092fef986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 179,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2444a46c-07ca-496b-ae6c-fe5d50813449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "92c1103b-015b-41b2-b9b9-8ce933f75278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "167f7e61-53db-451b-81c0-0d71314b31aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 23
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f63e5df2-a17a-4893-8672-458cf8655b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "04358ae1-f294-4f2b-976d-ac26c6a76104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb51d31d-a246-4d20-a872-2d5d942b694d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fd026508-e277-4b4c-98b5-2102235e70d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 166,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "475b25e8-c8cc-444c-8899-46408369c8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "738a442a-5105-4400-bb76-532dbd2bec36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "35725ace-b1bd-46a7-b7b8-01969d36d54e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 206,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "46f1b020-033b-43a5-b873-6a41ad30941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 197,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5af26de0-3c68-449b-afa2-d539f6b6e942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1c2eced5-de76-41b4-8243-ae2b4b7dd2cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 23
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "10222ed7-3959-4e3f-87ee-43801fe8facf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fbc0d5af-eeab-4494-943d-654334e56005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e83f1f54-5170-4fe7-b80d-535c99c4df77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 112,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9217a19a-a7b7-4517-8167-654c872550c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "386cd655-f664-433a-a05b-d2d95c1f41cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 152,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b2b1e4fb-7e61-4e2d-aa7b-14d8cde34653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "95ab7a83-dddf-42d5-b692-bc974ed29ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cfd1da13-e074-4c76-9a6d-6dc64fd4c021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6360f3ed-cdc3-4ba5-bf5b-7043ac479585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4008fb4b-9088-4fd7-be5d-b29093e37771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cc2024f7-2833-418f-b88d-afb62e6cacc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8952ab00-cf5a-45e7-87de-fd4195c90820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4991a48a-f971-4b2a-afb5-b0c61f37ef0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e3f9e2f8-1011-46cf-8670-c48bcb47f072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f876e151-9e0e-429b-abff-c79f34abfe88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8a65556b-8c45-4c27-bb85-eda54aa99b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7307834a-4ce8-4340-8231-f0e2f3233f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "07004c69-92f6-43b8-b206-0463a3faa60b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 132,
                "y": 44
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d7c5a0e1-18a5-48af-8258-c4e6c1f98efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 44
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b9460647-76ff-4f60-9867-022c6d78eece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "63f32bc3-b2dc-4ea1-9ae9-eb766a1bd5e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c3eccfad-c7dd-4ea3-b4b9-5c01dc302708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "21f47fff-5ca1-4171-a3a4-038624d16be3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "dc4ceb95-5ecd-4056-a57f-85e2aa6170c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0a59c62f-6997-4430-aa8f-d5b1a9a16c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7cf10515-5954-4873-aed4-05b29ca49fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 176,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1412653d-e5cd-4a23-bd64-931605d570bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "89031f29-31a9-4e40-98be-5fceebd3a4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "13ebc7ad-5f5d-4262-a91a-62cbb4736233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 153,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "31a2f711-12cb-48a5-82ee-3e77f4f4c6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8a27786b-4851-43f7-b2c4-c3929931d524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7e1264c7-bb49-45f1-86d1-f29e86eb4693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 100,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "58507603-afd8-4c7c-8247-016db49f1abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 186,
                "y": 23
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1140e482-1013-42c9-af51-2f47901d1bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 196,
                "y": 23
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5c1e4fcb-e3ab-4c0a-8c85-ba1d8896cd97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1d0b4485-a6e3-43ea-832e-9d9bf508a4ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 206,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a996a318-5f02-46f0-8f50-bb5e2da102d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 216,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "70ca08c8-0b42-4ef7-a960-f8158a64c22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b402864b-99d5-4528-a3c1-edce8a8c1832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "56adcf40-4950-426c-9975-4e6a511e6245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7c4cad2d-78c5-4b51-ab35-f31521331c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "56d5c0fb-9a3a-45cf-a33a-9c2717132d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 226,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "05f36ebe-9c32-49c9-99cb-e6231e46eac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3a4747b5-e7bf-4fb0-9072-c75d50963cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 161,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "768cd917-14a8-44dd-963f-9d589698a098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "31f3488e-3365-4f21-9582-e7a25846a038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 170,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e2c9d17b-6337-453d-8816-bcf60556afdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5a50a17e-9eeb-4e59-ae8b-f6569f1302f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "78ce5af7-a6c7-4578-a677-cce3a8116978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8585e80b-3f74-48e6-a376-b7fc1d4393b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c40ff123-33b0-436c-840e-2c5eea356043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 242,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3c2d4e56-9a0c-45f6-93bc-2d0178cbe78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e55af00b-0daa-423d-8932-e6fbe0ce3de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 188,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "922e0fd6-5e7f-4327-ab3b-e96c01883072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "86402328-fc05-4212-a515-598c9d3b1c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1f00dc6b-26f7-4851-a86d-1487fe1ab7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8c3df989-5bba-4b3a-96fd-52f5b362c3af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cc8ceaee-6e6b-4421-8f40-bb3c6ff7e093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 224,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "2f0790ff-6d4b-4b69-9c32-440cbe92b0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 233,
                "y": 44
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b784d2a4-da18-4a13-87cc-231fd62d5d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 181,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5a542347-39d8-4c82-86df-da9cb8cd14c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9ff844f8-118b-4ef0-9a45-7cd414f0ab30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 23
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}