{
    "id": "626ff852-66b1-4a94-a1d3-b7a803dd872b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_intro",
    "eventList": [
        {
            "id": "9a71610b-210c-44b7-baa4-a3c3ec86df79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "626ff852-66b1-4a94-a1d3-b7a803dd872b"
        },
        {
            "id": "32abb9b4-882d-409e-9c28-fd7335767ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "626ff852-66b1-4a94-a1d3-b7a803dd872b"
        },
        {
            "id": "f3ea7468-022c-4438-a427-87d6f8524862",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "626ff852-66b1-4a94-a1d3-b7a803dd872b"
        },
        {
            "id": "cd551358-7850-41b4-a030-496f11c54898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 78,
            "eventtype": 9,
            "m_owner": "626ff852-66b1-4a94-a1d3-b7a803dd872b"
        },
        {
            "id": "e0b39bb3-f8d8-4c53-b63f-df22687d6b8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "626ff852-66b1-4a94-a1d3-b7a803dd872b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3af9c09-2566-4717-ac7d-986188955819",
    "visible": true
}