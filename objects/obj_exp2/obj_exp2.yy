{
    "id": "db0d9f48-fb05-4223-9aeb-9f93783958b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exp2",
    "eventList": [
        {
            "id": "ae7a8c36-67b4-4fae-913d-a61860f1f3a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "db0d9f48-fb05-4223-9aeb-9f93783958b2"
        },
        {
            "id": "24e4a2e0-40b7-4fc6-a2f9-d8992734e37c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "db0d9f48-fb05-4223-9aeb-9f93783958b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
    "visible": true
}