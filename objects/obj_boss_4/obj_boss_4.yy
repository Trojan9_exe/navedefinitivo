{
    "id": "050c822c-9475-4134-8297-e41c7d2110cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_4",
    "eventList": [
        {
            "id": "21ed21ce-1e73-4418-b999-e7eb7967af46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "050c822c-9475-4134-8297-e41c7d2110cb"
        },
        {
            "id": "fbb969d7-1a45-4aa9-b98e-4727141e583f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "050c822c-9475-4134-8297-e41c7d2110cb"
        },
        {
            "id": "61d7632e-6f20-4d4c-8163-18a072356646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "050c822c-9475-4134-8297-e41c7d2110cb"
        },
        {
            "id": "4f605c1e-e5b1-41ca-a96b-198355d78865",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "050c822c-9475-4134-8297-e41c7d2110cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
    "visible": true
}