//17, 29, 33, 43, 50, 67
var textAux = font_inter2; //fonte auxiliar;
//este vai ficar aqui pois as barras devem aparecer por cima;
if(lincoln) draw_sprite(spr_USSlincoln, 0, 640, 360);
if(US_Base) draw_sprite(spr_BaseaAmericana, 0, 640, 360);
if(ultima) draw_sprite(spr_ultima, 0, 640, 360);

//desenhando as barras;
draw_sprite_ext(spr_barras, 0, 0, 0, 1280, barras[0], 0, c_black, 1);
draw_sprite_ext(spr_barras, 0, 0, 720, 1280, barras[1], 0, c_black, 1);

if(SPGeneral) draw_sprite(spr_major, 0, 10, general);
if(SPGeneral2) draw_sprite(spr_generalClint, 0, 30, general2);	
if(Powell) draw_sprite(spr_genPowell, 0, 30, general3);
if(SPPiloto)  draw_sprite(spr_soldado, 0, 1090, piloto);

//texto na tela;
if(next == 17 || next == 29 || next == 33 || next == 43 || next == 50 || next == 67 || next == 69 || next == 70 || next == 71 || next == 72) {
	//trocando a cor do background durante a cutscene, no momento em que os diálogos 
	//ainda não começaram;
	var lay_id = layer_get_id("Background");
	var back_id = layer_background_get_id(lay_id);
	layer_background_change(back_id, spr_Aux);
	
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	//utilizando outra fonte para o texto ficar maior e não perder qualidade;
	draw_set_font(textAux); 
	draw_text(640, 360, print);
}
else {
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(font_interface);
	draw_text(640, 660, print);
}
//verificando novamente para escrever por cima da barra
if(lincoln) {
	draw_set_font(textAux); 
	draw_text(room_width / 2, 30, "USS Abraham Lincoln");
}
if(US_Base) {
	draw_set_font(textAux); 
	draw_text(room_width / 2, 30, "Base na Arábia Saudita");
}
//desenhando a tela de fadein...;
draw_sprite_ext(spr_barras, 0, 0, 0, 1280, 720, 0, c_black, a);
if(next > 4 && next < 8) draw_sprite_ext(spr_IntroDef, 0, 640, 360, 1, 1, 0, -1, intro);