xpos = 640;
intro = 0;
a = 1;
fadeout = 0;
str = "";
print = "";
barras[2] = [100, 100];
general3 = 800;
general2 = 800;
general = 800;
piloto = 800;
//========================
podeDigitar = true;
ultima = false;
SPGeneral = false;
SPPiloto = false;
SPGeneral2 = false;
finalizado = false;
lincoln = false;
HeliLincoln = false;
US_Base = false;
Powell = false;
//========================
l = 0;
next = 0;
holdspace = 0;

//antes do tutorial
strings[0] = "Greg? Greg? CAPITÃO GREG SMITH ME ESCUTE...";
strings[1] = "Major? MAJOR TAYLOR, SIM, ESTOU TE OUVINDO, desculpe...";
strings[2] = "Escute, pare de lanchar no F15 e vá para o seu treinamento";
strings[3] = "Entendido, major Taylor!";

//oq devia aparecer aqui, vou colocar antes de tudo, com imagens e música;
strings[4] = ""; // Nave começa a andar
strings[5] = ""; //acelerar
strings[6] = ""; //acelerar
strings[7] = "";
strings[8] = ""; // Nave desacelerada

//inicio, meio e fim do tutorial
strings[9] = "Neste tutorial vamos \n aprender à atirar e movimentar o F15...";
strings[10] = "Para controlar o F15 use as teclas WSAD, \n para atirar aperte espaço e para soltar bombas aperte a letra B";
strings[11] = "Agora capitão, destrua os 5 inimigos e o encouraçado que se aproxima!";
strings[12] = "Muito bem capitão!";
strings[13] = "No canto superior direito, preste atenção na sua vida e na quantidade \n de bombas que você tem!";
strings[14] = "Às vezes, quando destruir um inimigo um mini coração aparecerá, \n pegue-o caso esteja com pouca vida";
strings[15] = "Para acessar mais configurações como a dificuldade ou tela cheia, \n aperte a tecla ESC à qualquer momento.";
strings[16] = "Vejo você em breve, capitão!";
//=======================================================================================================;
//Sobre a primeira missão:
/*Ideia: Da start da missão, ele é detectado, tem q destruir os misseis que lançam nele, depois 
aparecem dois caças MIG atirando e tem que matar, aparece o primeiro couraçado, destrói aparecem mais MIGs, 
assim por diante até matar os 4.*/
//"cutscene" antes da primeira fase ====================================================================;
strings[17] = "Major Taylor chama em sua sala no porta aviões USS Abraham Lincoln o capitão Greg Smith, \n para dar as instruções da missão secreta de reconhecimento nas ilhas próximas a península onde os \n inimigos escondem depósitos de armamentos pesados, com suspeitas de armas nucleares.";

strings[18] = "-Capitão, estou chamando o senhor aqui, pois tenho uma missão estritamente sigilosa e secreta em mãos, \n como o é meu melhor homem, preciso de você nessa, topa? ";
strings[19] = "-Major, estou pronto para a missão.";
strings[20] = "-Perfeito, temos que minar nosso inimigo bigodudo, \n o general Powell deflagrou a operação Tempestade no Deserto, ";
strings[21] = " mas antes do ataque terrestre precisamos abrir espaço no \n golfo para que nosso desembarque seja minimamente seguro. ";
strings[22] = "-Temos notícias receptadas que os iraquianos escondem em sua \n frota marítima 4 encouraçados armados com mísseis Scud, prontos para nos atacar, ";
strings[23] = " precisamos que sejam aniquilados para que possamos desembarcar \n nossas tropas e tanques no Kuwait, seu tempo para concluir a missão é de 25min.";
strings[24] = "-Sem seu sucesso a operação tempestade no deserto não terá sucesso. Está pronto capitão?";
strings[25] = "-Sim senhor!";
strings[26] = "-Já tem um caça equipado com nossos misseis Tomahawk te esperando na pista. \n Não esqueça que o inimigo enviará os MIG’s atrás de você. Contamos contigo.";
strings[27] = "-Senhor, voltarei com a missão concluída.";
strings[28] = "Ótimo, com o sucesso dessa missão teremos mais duas \n pela frente e você é peça chave, aguardo seu retorno com segurança.";
//na timeline deve ser chamado next == ;
//=======================================================================================================;
//retorno da primeira missão: 
strings[29] = "Ao pousar o caça, capitão Smith é recebido pelo major Taylor, \n onde é levado para a sala de comando e para sua surpresa ele vê o general Clint a sua espera.";

strings[30] = "-Capitão, cheguei a pouco no USS Lincoln e fiz questão de acompanhar sua missão. \n O Major não comentou, mas sua missão foi vital para nosso sucesso, agora, ";
strings[31] = "major e capitão preciso de vocês para outras duas missões \n muito estratégicas. Posso contar com vocês? E mais uma vez, meus parabéns pela missão.";
strings[32] = "-Sim, senhor general! Muito obrigado!";
//=======================================================================================================;
/*Ainda sobre a segunda missão: 
Ideia: Nessa fase, ficaria livre o inicio entre caças e tanques até chegar no prédio no meio do deserto 
e onde os outros dois caças se juntariam para auxiliar na conclusão na missão.*/
strings[33] = "Do Porta Aviões USS Lincoln, capitão Smith, major Taylor e general Clint embarcam em um \n helicóptero até a base americana na Arábia Saudita, cerca de 60 minutos do porta aviões.\n Já na base Smith, se reúne para ouvir a segunda missão.";

//início da segunda missão;
strings[34] = "-Major, qual será a segunda missão?";
strings[35] = "-Capitão, devido ao sucesso de sua primeira missão, conseguimos entrar no Kuwait e já estamos dando \n um show terrestre, os bigodudos estão saindo de lá e estamos em franca expansão ao Iraque. ";
strings[36] = "Temos a localização da célula militar de comando Iraquiana,  precisamos \n destruí-la, mas enviando os misseis de nossa frota terrestre, podemos ter muitas perdas civis, ";
strings[37] = ", é preciso que uma equipe de caças liderada por você lance misseis próximo do alvo. \n Já adianto que a edificação é muito complexa e feita como um bunker no deserto.";
strings[38] = " Precisará de ajuda para destruir, quando chegar nas proximidades desse \n complexo, outros 2 caças se alinharam ao seu para dizimarem o inimigo.";
strings[39] = "-Essa missão é muito perigosa Capitão, por isso foi o escolhido, \n muito cuidado com as baterias antiaéreas, tanques e também os caças inimigos.";
strings[40] = "-Está pronto Capitão?";
strings[41] = "-Estaremos na torcida por você, sua missão deve ser \n feita em no máximo 50 min, senão corre sério risco de insucesso.";
strings[42] = "-Senhor concluirei com êxito a missão."; //aqui começa a segunda missão;
//=======================================================================================================;
strings[43] = "Ao pousar, capitão Smith é recebido como herói, toda a base em festa. É parabenizado por todos, \n em especial pelo seu amigo major Taylor. General Clint o cumprimenta e o chama para a sala de comando maior da base, \n onde estava a sua espera o chefe do estado maior general Powell que o recebe com salva de palmas.";
//retorno da segunda missão;
strings[44] = "-Capitão Smith, parabéns por suas duas missões de êxito nessa operação, \n o senhor é uma inspiração para toda nossa força aérea. ";
strings[45] = " Como já foi comentado pelo major Taylor e general Clint, \n temos mais uma missão para o senhor, será a última operação. ";
strings[46] = "Está liberado hoje, comemore, descanse que em 2 dias eu pessoalmente te passarei a missão.";
strings[47] = "-Sim Senhor, obrigado pelas palavras general. ";
strings[48] = "-Você foi merecedor delas e pode me chamar de Powell.";
strings[49] = "-Obrigado senhor, ficarei alerta para às novas ordens.";
//=======================================================================================================;
/*Ainda sobre a terceira missão:
Ideia: Nessa fase, sairia já com os caças aliados, ficaria livre o inicio entre caças e tanques até 
chegar no prédio no vilarejo, onde ele solta a bomba e com a explosão e conclusão na missão.*/
strings[50] = "Ainda na base na Arábia Saudita, general Powell chama o capitão Smith para sua sala, \n onde reservadamente irá reportar a missão final.";
//início da terceira missão;
strings[51] = "-Capitão, estamos somente nós dois nessa sala, pois o que vou te \n passar talvez soe como uma missão suicida. Pode, se quiser, declinar da missão,";
strings[52] = "mas de antemão digo que não teremos outro piloto tão hábil e experiente para a mesma. ";
strings[53] = "Nessa missão infelizmente perderemos vidas civis, não haverá a possibilidade de não o fazê-lo. ";
strings[54] = "-O senhor quer declinar?";
strings[55] = "-De maneira nenhuma, senhor general, irei cumprir a missão. ";
strings[56] = "-Muito bem Smith, vou te dizer com cuidado tudo que temos sobre o inimigo.";
strings[57] = "-Nossos homens em terra já estão chagando a Bagdá, a guerra está praticamente \n ganha, mas queremos que haja o menor número de baixas possível. ";
strings[58] = "Para isso capitão, mais uma vez você será fundamental. \n Temos a localização do local onde Saddam comanda sua tropa.";
strings[59] = "Precisamos que destrua o local, mas dessa vez \n, você usará uma bomba nuclear. ";
strings[60] = "Queremos dizimar a área, mesmo que ele se esconda como um rato em túneis, ele \n não deve sobreviver. Como o local é próximo a um vilarejo, o mesmo será extinto. ";
strings[61] = "Ponderemos e achamos que as perdas são insignificantes \n diante do feito. Capitão esta é a sua última missão nessa operação, ";
strings[62] = "mesmo com o que foi dito aqui, está dentro da missão?";
strings[63] = "-General Powell, sou um capitão que nunca deixou de cumprir uma missão na vida, \n não será essa que deixarei. Pode autorizar a equipar o jato que irei cumprir.";
strings[64] = "-Até o local, terá contigo mais 5 jatos para tua escolta e tuas ordens.";
strings[65] = "-Desejo sorte em seu retorno à base com segurança, pode ir \n a pista que já embarcará e decolará em 10 min.";
//retorno da última fase;
strings[67] = "Ao pousar, capitão Smith é recebido como herói nacional, toda a base em festa. \n É recebido com música e fogos em comemoração. General Powell está ao telefone, \ncorre para Smith e passa o aparelho, na linha está o presidente dando os parabéns. \n No dia seguinte, capitão Smith é promovido a major e receberá a medalha de honra da Casa Branca.";
strings[68] = ""; //string vazia para mostrar a última imagem;

//coloquei aqui para não ter que ajustar tudo novamente;
strings[69] = "Diogo Baiocco";
strings[70] = "Wallan Rocha";
strings[71] = "Gabriel Souza";
strings[72] = "Apresentamos...";
strings[73] = "";
//=======================================================================================================;