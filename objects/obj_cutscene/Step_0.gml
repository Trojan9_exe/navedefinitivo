//fade
if(!fadeout) 
//função que retorna o máximo dos valores de entrada com até 16 números;	
a = max(a - 0.005, 0.25);
else
a = min(a + 0.005, 1); //retorna o menor valor de entrada

if(next > 4) {
	intro += 0.2;
	if(intro == 1) intro = 1;
}
if(next == 8) {
	intro--;
	if(intro <= 0) intro = 0;
}

if(podeDigitar) {
	l += 0.5;

	//função que copia uma string para outra, no número 1 e na variavel l
	print = string_copy(str, 1, l); //determinamos de onde começa e termina a cópia;

	if(l > string_length(str) + 100) {
		if(next < array_length_1d(strings) - 1) {
			l = 0;	
			next++;
			if (next == array_length_1d(strings) - 1)
				holdspace++;
	}
	else if (next >= array_length_1d(strings) - 1) 
				finalizado = true;
	}
	str = strings[next];
		
		if(Powell) { //major taylor
			general3 -= 8;
			if(general3 <= 470)
				general3 = 470;
		} 
		
		if(SPGeneral2) { //general clint
			general2 -= 8;
			if(general2 <= 470)
				general2 = 470;
		} 
		if(SPGeneral) { //major taylor
			general -= 8;
			if(general <= 470)
				general = 470;
		} 
		if(SPPiloto) { //capitão smith
			piloto -= 8;
			if(piloto <= 470)
				piloto = 470;
		}
}
	
barras[0] ++; //barra de cima;
if(barras[0] >= 100) 
	barras[0] = 100;
	
barras[1] --; //barra do rodapé;
if(barras[1] <= -100) 
	barras[1] = -100;