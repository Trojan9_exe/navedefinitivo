{
    "id": "dc900df6-516d-4bc6-9cd8-3eda5f333d0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cutscene",
    "eventList": [
        {
            "id": "2b619458-d928-4ef1-bfc7-87bbfd15dc41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc900df6-516d-4bc6-9cd8-3eda5f333d0d"
        },
        {
            "id": "b4d4d29b-1fb7-48b6-82ef-79f0f8bdf8b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 80,
            "eventtype": 9,
            "m_owner": "dc900df6-516d-4bc6-9cd8-3eda5f333d0d"
        },
        {
            "id": "47a966f2-1320-4801-83d3-56d7304688c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc900df6-516d-4bc6-9cd8-3eda5f333d0d"
        },
        {
            "id": "cf7d7557-95a6-4026-bad0-095b6a755988",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dc900df6-516d-4bc6-9cd8-3eda5f333d0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}