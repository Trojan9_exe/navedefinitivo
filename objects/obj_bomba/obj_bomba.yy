{
    "id": "6489a88d-6742-4bfc-b92a-7aa660c7c851",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bomba",
    "eventList": [
        {
            "id": "62ca2290-f618-4b0a-b888-d457dca787d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6489a88d-6742-4bfc-b92a-7aa660c7c851"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfd69fec-7e07-4f44-8d0b-05d66285045a",
    "visible": true
}