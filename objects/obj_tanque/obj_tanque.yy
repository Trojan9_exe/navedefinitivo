{
    "id": "560a58ab-6118-45e2-9478-45f5345ad9ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tanque",
    "eventList": [
        {
            "id": "8d4d055f-799d-44a4-a7cd-03cec44c5ce6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "560a58ab-6118-45e2-9478-45f5345ad9ea"
        },
        {
            "id": "ac3ca280-2c52-483b-bceb-960d93da6221",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "560a58ab-6118-45e2-9478-45f5345ad9ea"
        },
        {
            "id": "ea7e406f-5447-48b9-ac1c-79ceae410e16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "560a58ab-6118-45e2-9478-45f5345ad9ea"
        },
        {
            "id": "8a54d2f3-e50d-4d4b-8e23-a3a1a6dd8c58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "560a58ab-6118-45e2-9478-45f5345ad9ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1ba96737-f2b3-4ae9-b72c-19ab8d84392b",
    "visible": true
}