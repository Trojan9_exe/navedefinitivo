{
    "id": "463fcfee-19aa-4f18-8090-dc89bce721f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_f15_Aux1",
    "eventList": [
        {
            "id": "fb7bfa14-80ac-4246-a77f-a0e0905de6ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "90d12e21-6976-489e-a9d5-5ee955e261a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "4ac9205c-bc7c-4537-b228-6327e0d61ccb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "fb8abf64-0865-4045-9a29-8b1e681c54af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "dcfeaa3d-4e91-48cc-ac6f-254d51e67c3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "dfcea2aa-b5bb-49fd-a014-cb0ddaa3033e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "6417cb05-cf15-4e54-b703-ac4d4cc5c4c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        },
        {
            "id": "087262e0-6590-4a72-b69d-861033a87608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "463fcfee-19aa-4f18-8090-dc89bce721f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
    "visible": true
}