/// @description Step Event
//obj_f_15_evt_Step();
if(hp <= 0) {
	instance_destroy();	
	show_message("Você falhou capitão!");
	game_restart();
}
isKeyBombing = checkKeyIsBeingPressed(isKeyBombing, ord("B"));
if(isKeyShooting && isTimeToShoot>=shootingDelay) {
	switch(global.direcao){
		case 0: //cima 
		case 2: //baixo
			instance_create_layer(x-6+hspeed, y,"player_tiros", obj_tiro);
			instance_create_layer(x+6+hspeed, y,"player_tiros", obj_tiro);
			break;
		case 1: //direita 
		case 3: //esquerda 
			instance_create_layer(x, y-6+vspeed,"player_tiros", obj_tiro);
			instance_create_layer(x, y+6+vspeed,"player_tiros", obj_tiro);
			break;
		default: break;
	}
		
	audio_play_sound(sound_tiro_03, 1, 0);
		
	if(isTimeToShootCycle>=isTimeToShootCycleDelay) {
		isTimeToShoot = -5;
		isTimeToShootCycle = 0;
	}
	else {
		isTimeToShootCycle++;
		isTimeToShoot = 0;
	}
}

if(global.HaveBombsAux > 0 && isKeyBombing && isTimeToBOMBShoot>=shootingBOMBDelay){
	instance_create_layer(x, y, "player_tiros", obj_bomba);
	audio_play_sound(sound_missile_01, 1, 0);
	global.HaveBombsAux --;
	isTimeToBOMBShoot = 0;
}
isTimeToBOMBShoot++;
isTimeToShoot++;
var obj_player = instance_find(obj_F_15, 0);
if(instance_exists(obj_player)) {
	switch(global.direcao) {
	
	case 1: //direção da fase 2;
			if(obj_player.x > x) x += 5;
			else x = obj_player.x;
			if(obj_player.y > y) y += 5;
			else y = obj_player.y + 90;
			break;
	
	case 2: //fase 3
			if(obj_player.x > x) x += 5;
			else x = obj_player.x + 90;
			if(obj_player.y > y) y += 5;
			else y = obj_player.y;
			break;
	}
}
