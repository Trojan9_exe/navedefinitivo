{
    "id": "2f04074f-8c3b-4838-b4f0-91b7a1398bcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_3",
    "eventList": [
        {
            "id": "d0b08483-19de-42b6-ad5a-b4aba9b6959d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f04074f-8c3b-4838-b4f0-91b7a1398bcb"
        },
        {
            "id": "75f8af44-e363-48f8-8d43-e332537b51c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f04074f-8c3b-4838-b4f0-91b7a1398bcb"
        },
        {
            "id": "905f2ef2-0a38-4362-82cd-91cfcf8d0350",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f04074f-8c3b-4838-b4f0-91b7a1398bcb"
        },
        {
            "id": "f67837a1-dc1a-43f8-b0d4-c0ae919d1d08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f04074f-8c3b-4838-b4f0-91b7a1398bcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
    "visible": true
}