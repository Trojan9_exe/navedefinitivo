{
    "id": "9118dbae-904f-4351-97a4-1e6f371d5725",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exp",
    "eventList": [
        {
            "id": "16ff3214-a74e-4552-8928-62f31c98caa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9118dbae-904f-4351-97a4-1e6f371d5725"
        },
        {
            "id": "615a42bb-58b7-4b81-b47c-e5b9507b3d85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9118dbae-904f-4351-97a4-1e6f371d5725"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
    "visible": true
}