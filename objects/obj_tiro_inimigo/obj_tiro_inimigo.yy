{
    "id": "93c40132-4c62-4bc6-879b-7e574af90239",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tiro_inimigo",
    "eventList": [
        {
            "id": "eef5116d-922d-447f-bcfd-64cbbfe5abd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93c40132-4c62-4bc6-879b-7e574af90239"
        },
        {
            "id": "29097550-018b-4b4b-9523-438e8bc77ea0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "93c40132-4c62-4bc6-879b-7e574af90239"
        },
        {
            "id": "deb4882c-525c-4de5-a42d-7a2cf438d01d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93c40132-4c62-4bc6-879b-7e574af90239"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "481b3994-20d0-40ed-8fcd-dedd7e3f1cc9",
    "visible": true
}