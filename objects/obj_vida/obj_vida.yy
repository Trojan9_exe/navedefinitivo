{
    "id": "0c557464-d4f4-4e91-8f9d-eaba64679e03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_vida",
    "eventList": [
        {
            "id": "de57a691-99cb-4fcd-b775-28a4defbc430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c557464-d4f4-4e91-8f9d-eaba64679e03"
        },
        {
            "id": "7640efc4-98cb-412c-aacd-1b48ba4c5530",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0c557464-d4f4-4e91-8f9d-eaba64679e03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03856174-7a81-4b18-b956-656d5a86420a",
    "visible": true
}