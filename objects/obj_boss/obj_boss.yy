{
    "id": "4e16ce8c-db88-4fc5-a5f4-affd6e6219f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "22f60d5a-324c-4bf7-9bfa-fe2bfd62b93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e16ce8c-db88-4fc5-a5f4-affd6e6219f4"
        },
        {
            "id": "b59db532-199b-4080-8180-2c497dd9bcb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4e16ce8c-db88-4fc5-a5f4-affd6e6219f4"
        },
        {
            "id": "7b294082-8536-46a2-b3b0-c12e63fc50bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4e16ce8c-db88-4fc5-a5f4-affd6e6219f4"
        },
        {
            "id": "d0aa6948-f911-4c1a-aac6-7d355a4cd596",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e16ce8c-db88-4fc5-a5f4-affd6e6219f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
    "visible": true
}