if(y >= 200) vspeed = 0;
if(hp <= 0)	{
	instance_destroy();
	if(global.tut_needed_bosses>0) {
		global.tut_needed_bosses--;
	}
	global.pontos += points;
	if(!global.tutorial) {
		global.qtdBOSS -= 1;
			if(global.qtdBOSS <= 0) {
				obj_F_15.interacao = false;
				NaveSaindo(global.direcao);	
			}	
	}
	//obj_F_15.hspeed += 5; 
	instance_create_layer(x, y, "inimigos", obj_exp2);
}

if(isTimeToShoot>=shootingDelay && instance_exists(obj_F_15) && instance_find(obj_F_15, 0).interacao){
	var Bullet = instance_create_layer(x, y,"boss_tiros", obj_tiro_boss);
	isTimeToShoot = 0;
	Bullet.direction = point_direction(x, y, obj_F_15.x, obj_F_15.y);
	Bullet.image_angle = Bullet.direction+90;
}
if(!global.tut_needed_bosses) isTimeToShoot++;