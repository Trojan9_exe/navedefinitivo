{
    "id": "e4527075-c4b2-4101-bad0-c87d9caaae15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enterprise",
    "eventList": [
        {
            "id": "1c904db0-8139-47e7-a842-41410e9ae4c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e4527075-c4b2-4101-bad0-c87d9caaae15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8059a81c-b1f9-45b9-9b4f-2dc0df4bbe0c",
    "visible": true
}