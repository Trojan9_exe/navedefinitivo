{
    "id": "44fb30a3-b6a7-4371-a13e-052451e2f805",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_penta",
    "eventList": [
        {
            "id": "99f0dc4e-1db4-4fe1-be4c-5e5a1b5683b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44fb30a3-b6a7-4371-a13e-052451e2f805"
        },
        {
            "id": "38226965-b49f-4299-9196-930534f090c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44fb30a3-b6a7-4371-a13e-052451e2f805"
        },
        {
            "id": "0fbc8653-c10f-4d86-81df-2641a8bb9f02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6489a88d-6742-4bfc-b92a-7aa660c7c851",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "44fb30a3-b6a7-4371-a13e-052451e2f805"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a44edfab-e17a-407d-8c19-e6ac276571d3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5e6132e-2847-444d-a5ba-7c6a5525b0b3",
    "visible": true
}