{
    "id": "1c9676ad-30fc-4cd8-9a2e-3e3a433c1c86",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ilha",
    "eventList": [
        {
            "id": "0abbabb6-b25a-4ebb-a477-ed9947d97c7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c9676ad-30fc-4cd8-9a2e-3e3a433c1c86"
        },
        {
            "id": "a915d9a8-3dbb-4e85-a54f-5d65eebb3a3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c9676ad-30fc-4cd8-9a2e-3e3a433c1c86"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85faf633-3672-4467-a636-4f6cbf173dc7",
    "visible": true
}