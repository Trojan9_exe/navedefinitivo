{
    "id": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tiroTanque",
    "eventList": [
        {
            "id": "dd644413-0f7f-47d1-abc7-64b27c3c9995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        },
        {
            "id": "184bb9b8-97ae-4ee0-b954-06c4b4a133de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        },
        {
            "id": "e2751615-2fd5-4ed7-aa77-ec6a1ef87f4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        },
        {
            "id": "3a3af51d-0496-4017-8be6-cfce4992a521",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "463fcfee-19aa-4f18-8090-dc89bce721f5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        },
        {
            "id": "06798068-b8e3-4807-9a5d-9c3fd1fc7ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        },
        {
            "id": "d730f091-2f41-4935-aadd-7d432f21de9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78c369f0-0f53-4e3d-9b19-d5f4a1b855c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4b25e45-c751-4d2e-a3b2-bd4b6a7b0a3c",
    "visible": true
}