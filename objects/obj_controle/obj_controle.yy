{
    "id": "35ef1278-d491-4927-8268-6d32276ad13d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controle",
    "eventList": [
        {
            "id": "f5ef235f-4184-48cd-9a4a-f34a13cd86be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "35ef1278-d491-4927-8268-6d32276ad13d"
        },
        {
            "id": "de301dbc-eeb7-47d2-afc3-8fde5eda28c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "35ef1278-d491-4927-8268-6d32276ad13d"
        },
        {
            "id": "9e0c6d4c-2ae3-42a6-b970-58a50efcbccf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "35ef1278-d491-4927-8268-6d32276ad13d"
        },
        {
            "id": "1a63bd27-6be0-4317-8a56-bc71e217dd75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "35ef1278-d491-4927-8268-6d32276ad13d"
        },
        {
            "id": "9edf2801-6ef4-4009-8f11-70a9d637d2a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35ef1278-d491-4927-8268-6d32276ad13d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}