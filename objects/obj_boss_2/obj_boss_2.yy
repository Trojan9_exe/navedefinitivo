{
    "id": "c645616b-cbbe-433e-a7ac-47c913fae79f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_2",
    "eventList": [
        {
            "id": "c5fb7412-1a2d-42f6-922b-8e0356c05cc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c645616b-cbbe-433e-a7ac-47c913fae79f"
        },
        {
            "id": "a2eec251-bdc7-4ccc-b1b6-ece79539028f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c645616b-cbbe-433e-a7ac-47c913fae79f"
        },
        {
            "id": "b3da4bbd-a33e-4d90-bec7-fd67c1ddd14b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c645616b-cbbe-433e-a7ac-47c913fae79f"
        },
        {
            "id": "f2cb9ada-faa9-4178-89ab-75eb0268876c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c645616b-cbbe-433e-a7ac-47c913fae79f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
    "visible": true
}