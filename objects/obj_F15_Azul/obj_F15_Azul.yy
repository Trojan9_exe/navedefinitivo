{
    "id": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_F15_Azul",
    "eventList": [
        {
            "id": "530492f3-8ddd-404b-bfd5-dc8276b080f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "a7f6411d-3043-4617-a731-1ebb87db6917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "28963cef-a21c-4a05-a9d8-04361b8c0171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "709fdc8a-ff4a-4d18-b20d-666197fc783f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "50fde7fd-b3e9-4b13-8adb-07e619d5dc22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "700027d7-3c7b-4e6f-8741-fe5e39eaa7ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "6088138a-7843-4cfb-bfec-3824aa56777e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        },
        {
            "id": "9d35a42a-0613-4e6c-a8c1-2b6cee9c6465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f8b7c57a-5ac4-4d41-93e7-9a397919f3c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
    "visible": true
}