{
    "id": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tiro_boss",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "93c40132-4c62-4bc6-879b-7e574af90239",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4b25e45-c751-4d2e-a3b2-bd4b6a7b0a3c",
    "visible": true
}