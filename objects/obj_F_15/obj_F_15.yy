{
    "id": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_F_15",
    "eventList": [
        {
            "id": "f84b4714-06f9-480d-a14f-d7e26a228bad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "9fb737bc-a8ac-4f47-9b16-167b1b4863b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "c948da6d-8c01-4ca9-975f-c9b56ceb39b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "6001c771-8f94-4930-9eb4-abe412ac0ff2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "2e64cac0-c21a-4af7-af63-0c91a0d5f55f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0c557464-d4f4-4e91-8f9d-eaba64679e03",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "69949c37-7865-4135-b7f4-9c9e0c71b0e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "9d257ee7-29d6-4ba7-beb9-abf1859f342d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        },
        {
            "id": "da30cae7-cf68-4187-a51b-b1877e208550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a44edfab-e17a-407d-8c19-e6ac276571d3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
    "visible": true
}