/// @description DIRECAO
if(!variable_instance_exists(self, "points"))
	points = global.enemyPoints;
	
if(!variable_instance_exists(self, "hp"))
	hp = global.enemyHP;
	
if(!variable_instance_exists(self, "ispeed"))
	ispeed = global.inimigo_speed;
	
switch(global.direcao){
	case 0: //cima 
		vspeed = ispeed;
		hspeed = 0;
	break;
	case 1: //direita 
		vspeed = 0;
		hspeed = -ispeed;
	break;
	case 2: //baixo
		vspeed = - ispeed;
		hspeed = 0;
	break;
	case 3: //esquerda 
		vspeed = 0;
		hspeed = ispeed;
	break;
	default: break;
}
/// @description MUDAR DIRECAO
switch(global.direcao){
	case 0: //cima 
		image_angle = -180;
	break;
	case 1: //direita 
		image_angle = -270;
	break;
	case 2: //baixo
		image_angle = 0;
	break;
	case 3: //esquerda 
		image_angle = -90;
	break;
	default: break;
}
last_direcao = global.direcao;