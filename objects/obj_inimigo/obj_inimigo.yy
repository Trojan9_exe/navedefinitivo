{
    "id": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inimigo",
    "eventList": [
        {
            "id": "005cc3af-c639-46b2-bac8-f35e882583aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "f41408a5-3c0b-49aa-8bb0-cc2bb8a90362",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d86f95ae-24f6-4ea5-abe5-aa232a56d17e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "95195e86-f50e-4a62-b0ef-e6561569a708",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6489a88d-6742-4bfc-b92a-7aa660c7c851",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "8a2275b1-f03f-4ade-b8d9-4cc9dcc86a79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "7decdbbd-e4d3-475c-b1e7-143a5b35b677",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "621c9550-702b-4738-918c-f4191942e7e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "963812d0-7aa7-4463-91d5-19b8dd50b9e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15bdb8f6-4799-4fb9-99d0-0fcd743af6c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        },
        {
            "id": "47c6e535-3f35-448e-a4a1-7c9b86da8ffc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08a9aacd-f014-4158-8795-3bc48f3b4a1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98468062-ee6a-4398-8acc-06e1eae0e7b0",
    "visible": true
}