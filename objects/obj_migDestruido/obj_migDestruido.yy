{
    "id": "982b973c-3c73-404e-ab3b-a095c3994b0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_migDestruido",
    "eventList": [
        {
            "id": "c9e76341-71c3-42ba-b8d1-f03f4105372e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "982b973c-3c73-404e-ab3b-a095c3994b0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "44a43fb4-2892-4fe0-a689-451a433c929b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8078e41f-4437-48f1-a2e9-5d19ac6b963c",
    "visible": true
}