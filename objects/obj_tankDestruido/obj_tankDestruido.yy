{
    "id": "44a43fb4-2892-4fe0-a689-451a433c929b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tankDestruido",
    "eventList": [
        {
            "id": "5c5ef8d7-f9af-4063-bcf1-5493a893b04a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "44a43fb4-2892-4fe0-a689-451a433c929b"
        },
        {
            "id": "6054b226-6c4e-453a-b258-92c10cc960b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "44a43fb4-2892-4fe0-a689-451a433c929b"
        },
        {
            "id": "c3aa7c8d-6ead-4988-9495-bed2fefe9ff5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "44a43fb4-2892-4fe0-a689-451a433c929b"
        },
        {
            "id": "a334e491-6ff9-4b5b-a69d-89f945603fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44a43fb4-2892-4fe0-a689-451a433c929b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d87539eb-78b0-4424-9e2a-e26a15140a54",
    "visible": true
}