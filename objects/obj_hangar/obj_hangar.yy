{
    "id": "a44edfab-e17a-407d-8c19-e6ac276571d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hangar",
    "eventList": [
        {
            "id": "be45fab9-d9e4-4eb2-a3db-75958073d702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a44edfab-e17a-407d-8c19-e6ac276571d3"
        },
        {
            "id": "eecd11f4-35d5-486c-a09a-9477e777b3f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a44edfab-e17a-407d-8c19-e6ac276571d3"
        },
        {
            "id": "88b973f2-7d2e-4e43-930a-867b2e6ed77f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6489a88d-6742-4bfc-b92a-7aa660c7c851",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a44edfab-e17a-407d-8c19-e6ac276571d3"
        },
        {
            "id": "50ee7c79-2823-4756-a9c6-f9d8b6be9fbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a44edfab-e17a-407d-8c19-e6ac276571d3"
        },
        {
            "id": "5b08934d-893e-43cb-bd71-148292e99ed8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a44edfab-e17a-407d-8c19-e6ac276571d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "297f0172-7983-4f19-a846-3c61fb5d65ef",
    "visible": true
}