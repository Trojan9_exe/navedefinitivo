{
    "id": "40805885-8457-4baa-8a76-7490a77619b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mig",
    "eventList": [
        {
            "id": "05d3b715-437a-46f8-b661-8d324ea8c442",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40805885-8457-4baa-8a76-7490a77619b6"
        },
        {
            "id": "c78dbf8f-64f6-4ace-b178-aa0a8c39c02f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40805885-8457-4baa-8a76-7490a77619b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08a9aacd-f014-4158-8795-3bc48f3b4a1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98468062-ee6a-4398-8acc-06e1eae0e7b0",
    "visible": true
}