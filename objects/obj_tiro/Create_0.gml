/// @description direção
if(!variable_instance_exists(self, "tspeed"))
	tspeed = global.player_tiro_speed;

switch(global.direcao){
	case 0: //cima 
		vspeed = -tspeed;
		hspeed = 0;
		image_angle = 0;
	break;
	case 1: //direita 
		vspeed = 0;
		hspeed = tspeed;
		image_angle = -90;
	break;
	case 2: //baixo
		vspeed = tspeed;
		hspeed = 0;
		image_angle = -180;
	break;
	case 3: //esquerda 
		vspeed = 0;
		hspeed = - tspeed;
		image_angle = -270;
	break;
	default: break;
}
last_direcao = global.direcao;