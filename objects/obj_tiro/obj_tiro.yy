{
    "id": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tiro",
    "eventList": [
        {
            "id": "8726fa1d-173e-4836-898a-464a7f23e8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c"
        },
        {
            "id": "b938174a-4d0d-4cd8-af6e-206d69a6360d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c"
        },
        {
            "id": "11a81566-9ef8-4819-a6a7-30163e39bafd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7fe7621d-a2a1-4dbf-a8fb-0b3f72c5765c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "481b3994-20d0-40ed-8fcd-dedd7e3f1cc9",
    "visible": true
}