{
    "id": "c4b25e45-c751-4d2e-a3b2-bd4b6a7b0a3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiroBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec19262c-8ecf-490d-9f9d-dea543932850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4b25e45-c751-4d2e-a3b2-bd4b6a7b0a3c",
            "compositeImage": {
                "id": "c073f84e-1b2f-4c77-963c-95fdec11538c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec19262c-8ecf-490d-9f9d-dea543932850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909110d9-6120-4830-a448-381277d62b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec19262c-8ecf-490d-9f9d-dea543932850",
                    "LayerId": "50183174-57c1-402e-a2de-16ae6ab2717f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50183174-57c1-402e-a2de-16ae6ab2717f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4b25e45-c751-4d2e-a3b2-bd4b6a7b0a3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}