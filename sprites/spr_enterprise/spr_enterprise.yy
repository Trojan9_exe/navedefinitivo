{
    "id": "8059a81c-b1f9-45b9-9b4f-2dc0df4bbe0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enterprise",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "296a0dd2-52b9-4d88-aed9-24e7f3e49f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8059a81c-b1f9-45b9-9b4f-2dc0df4bbe0c",
            "compositeImage": {
                "id": "022ca18c-6b0c-4d11-8603-624db8a5e96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296a0dd2-52b9-4d88-aed9-24e7f3e49f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f7bc70-68d0-4c8a-9c0f-801b314eab2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296a0dd2-52b9-4d88-aed9-24e7f3e49f78",
                    "LayerId": "84aa395b-d9c8-4d76-9734-47eec9ae5029"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "84aa395b-d9c8-4d76-9734-47eec9ae5029",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8059a81c-b1f9-45b9-9b4f-2dc0df4bbe0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}