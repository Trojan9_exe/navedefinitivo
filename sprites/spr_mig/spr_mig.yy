{
    "id": "98468062-ee6a-4398-8acc-06e1eae0e7b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea53099c-1b14-4601-ac60-51a3ffcb78d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98468062-ee6a-4398-8acc-06e1eae0e7b0",
            "compositeImage": {
                "id": "ee95f949-53d2-46a7-9d58-67c2ea9ea3fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea53099c-1b14-4601-ac60-51a3ffcb78d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7735b00c-77d8-4983-9ec0-86c29a05db6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea53099c-1b14-4601-ac60-51a3ffcb78d8",
                    "LayerId": "00ca8b94-c184-4e03-ab23-f16a7297cebe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "00ca8b94-c184-4e03-ab23-f16a7297cebe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98468062-ee6a-4398-8acc-06e1eae0e7b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 50
}