{
    "id": "5a74feec-ce76-4320-9618-f8d921dce819",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_USSlincoln",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59304610-68c9-434e-a9a5-dd414e0d2419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a74feec-ce76-4320-9618-f8d921dce819",
            "compositeImage": {
                "id": "eb8fbcde-c77e-44b6-8e6d-8ebf14d5bd2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59304610-68c9-434e-a9a5-dd414e0d2419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26eeb4fb-e3a9-44e5-9a51-0334d95f8ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59304610-68c9-434e-a9a5-dd414e0d2419",
                    "LayerId": "db28de1a-e0fc-4d46-8354-243bffa600ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "db28de1a-e0fc-4d46-8354-243bffa600ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a74feec-ce76-4320-9618-f8d921dce819",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}