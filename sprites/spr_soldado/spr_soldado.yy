{
    "id": "6166db8e-96a8-4a21-a7b8-864329060263",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_soldado",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ca2eeb8-3a1d-4708-8f48-c17466100689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6166db8e-96a8-4a21-a7b8-864329060263",
            "compositeImage": {
                "id": "f3f81bf3-aadd-41e7-b213-87bca8340605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca2eeb8-3a1d-4708-8f48-c17466100689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444ba4ab-94c4-4460-bc4a-cae9fbec3f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca2eeb8-3a1d-4708-8f48-c17466100689",
                    "LayerId": "76a9ed69-72ae-4606-b115-4ea524dc40d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "76a9ed69-72ae-4606-b115-4ea524dc40d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6166db8e-96a8-4a21-a7b8-864329060263",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 0,
    "yorig": 0
}