{
    "id": "70c6937a-4b60-48ba-b630-72ff13123b1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_USSLincolnHeli",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b071eda-89d3-4d44-84f6-e4bb0e045676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70c6937a-4b60-48ba-b630-72ff13123b1a",
            "compositeImage": {
                "id": "7e831774-82a4-4f16-86a0-1ddc5bd9a404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b071eda-89d3-4d44-84f6-e4bb0e045676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3ac1d1-fad2-4a3b-9ece-836537b8c176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b071eda-89d3-4d44-84f6-e4bb0e045676",
                    "LayerId": "e18d0809-90a6-407a-bd6f-0d8f2b0250f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "e18d0809-90a6-407a-bd6f-0d8f2b0250f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70c6937a-4b60-48ba-b630-72ff13123b1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}