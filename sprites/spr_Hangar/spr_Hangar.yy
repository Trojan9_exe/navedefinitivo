{
    "id": "297f0172-7983-4f19-a846-3c61fb5d65ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Hangar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 561,
    "bbox_left": 32,
    "bbox_right": 381,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f723989-150b-447f-bb86-c8f8714b37ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297f0172-7983-4f19-a846-3c61fb5d65ef",
            "compositeImage": {
                "id": "76037b3d-d1bf-4231-8de7-621759360505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f723989-150b-447f-bb86-c8f8714b37ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52742722-8396-4aec-aba5-4062daca9879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f723989-150b-447f-bb86-c8f8714b37ca",
                    "LayerId": "dba9e267-1ce9-476d-8e3e-9ec3cfc45e28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 576,
    "layers": [
        {
            "id": "dba9e267-1ce9-476d-8e3e-9ec3cfc45e28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "297f0172-7983-4f19-a846-3c61fb5d65ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 409,
    "xorig": 204,
    "yorig": 288
}