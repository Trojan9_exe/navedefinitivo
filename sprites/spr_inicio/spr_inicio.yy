{
    "id": "22474e5a-6557-408e-a579-cf2f32e07a76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inicio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55cc1f5a-8b9c-4e41-89f9-2f889d55fcdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22474e5a-6557-408e-a579-cf2f32e07a76",
            "compositeImage": {
                "id": "cd671481-05dc-40d3-a69a-42090459fc51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55cc1f5a-8b9c-4e41-89f9-2f889d55fcdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6b32c1-b0a1-4d9d-99f2-7c38a30a56d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55cc1f5a-8b9c-4e41-89f9-2f889d55fcdc",
                    "LayerId": "9415ece0-0b07-4269-a3f1-c8ca5b610586"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9415ece0-0b07-4269-a3f1-c8ca5b610586",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22474e5a-6557-408e-a579-cf2f32e07a76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}