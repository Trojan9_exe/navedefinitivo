{
    "id": "edb81247-01cf-4455-87af-ec4c8f5acabe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nuvem3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 533,
    "bbox_left": 32,
    "bbox_right": 918,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4c48b85-a10e-4f2f-9292-496dc4eb83a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edb81247-01cf-4455-87af-ec4c8f5acabe",
            "compositeImage": {
                "id": "5644ddae-3828-44c9-a432-08003aa5f714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c48b85-a10e-4f2f-9292-496dc4eb83a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c629b3c8-1403-43eb-a8ad-e51adb9860f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c48b85-a10e-4f2f-9292-496dc4eb83a2",
                    "LayerId": "f38977d9-6de8-421c-b16f-de54f1d85b37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 534,
    "layers": [
        {
            "id": "f38977d9-6de8-421c-b16f-de54f1d85b37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edb81247-01cf-4455-87af-ec4c8f5acabe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 35,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 950,
    "xorig": 475,
    "yorig": 267
}