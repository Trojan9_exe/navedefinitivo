{
    "id": "bfd69fec-7e07-4f44-8d0b-05d66285045a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bomba",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bc4b3e6-5861-45e5-a07b-41b61016c668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd69fec-7e07-4f44-8d0b-05d66285045a",
            "compositeImage": {
                "id": "a12daf49-ad41-4026-b0b2-48f149b5626b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc4b3e6-5861-45e5-a07b-41b61016c668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cdb5979-0c3c-4469-908b-43e117141604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc4b3e6-5861-45e5-a07b-41b61016c668",
                    "LayerId": "d71edf58-aa61-41f8-8ac3-d7b0e75d3d4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d71edf58-aa61-41f8-8ac3-d7b0e75d3d4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd69fec-7e07-4f44-8d0b-05d66285045a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}