{
    "id": "f3af9c09-2566-4717-ac7d-986188955819",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IntroDef",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 720,
    "bbox_left": 123,
    "bbox_right": 1160,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6803f15-90d7-4c33-af93-b46945924ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3af9c09-2566-4717-ac7d-986188955819",
            "compositeImage": {
                "id": "d2c16500-53cb-4972-9253-952729b67899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6803f15-90d7-4c33-af93-b46945924ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3ca574-716d-4226-a81c-586a5cbe207d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6803f15-90d7-4c33-af93-b46945924ad0",
                    "LayerId": "9f5bf0ea-6209-4984-9baa-709cbbadf0a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 723,
    "layers": [
        {
            "id": "9f5bf0ea-6209-4984-9baa-709cbbadf0a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3af9c09-2566-4717-ac7d-986188955819",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1283,
    "xorig": 641,
    "yorig": 361
}