{
    "id": "a769190b-c309-4afa-ba6e-04bf2a5bc252",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ee75d26-74ae-49de-b47e-1b6c6a476aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a769190b-c309-4afa-ba6e-04bf2a5bc252",
            "compositeImage": {
                "id": "ea27cf5e-4053-48ec-b17f-5521d2253d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee75d26-74ae-49de-b47e-1b6c6a476aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6222b930-492a-4595-8c38-dafe405bf1fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee75d26-74ae-49de-b47e-1b6c6a476aba",
                    "LayerId": "5b0fc396-de7f-425b-a943-db36b1c12073"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "5b0fc396-de7f-425b-a943-db36b1c12073",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a769190b-c309-4afa-ba6e-04bf2a5bc252",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}