{
    "id": "2becfd4f-1a69-46da-aeca-b980611036e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_still_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0a0a275-b18b-45c3-b289-7805fd306dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2becfd4f-1a69-46da-aeca-b980611036e5",
            "compositeImage": {
                "id": "d668974e-eeed-415b-b42d-d8195026e8f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a0a275-b18b-45c3-b289-7805fd306dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a047264-b559-4a37-a925-72b8381e32d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a0a275-b18b-45c3-b289-7805fd306dba",
                    "LayerId": "ced92a76-a657-49ba-8f68-2ffdeab12766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ced92a76-a657-49ba-8f68-2ffdeab12766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2becfd4f-1a69-46da-aeca-b980611036e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}