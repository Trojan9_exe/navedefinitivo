{
    "id": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exp_inimigo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 105,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9154e3b0-b223-4d83-98a1-eeeb825f3896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "29d78671-41c0-46ec-824d-772f531a8a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9154e3b0-b223-4d83-98a1-eeeb825f3896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b71e56-7822-43b1-a516-a687cc806b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9154e3b0-b223-4d83-98a1-eeeb825f3896",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "c05b8017-3570-465a-a39d-5118f665cf7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "d1f53283-8c32-436d-b098-0109c452604b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05b8017-3570-465a-a39d-5118f665cf7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d28f3a-e044-4fbd-98c5-c1f28b4cea59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05b8017-3570-465a-a39d-5118f665cf7c",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "b9f870e7-99b1-4ccb-ab11-b26b211a28f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "8553b347-3241-4e9f-8252-758c60e8fed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f870e7-99b1-4ccb-ab11-b26b211a28f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff12121d-b309-47a4-a787-2d26e24d2c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f870e7-99b1-4ccb-ab11-b26b211a28f3",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "bae71d0a-a80d-4b27-89fe-ed13a8dc6504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "09b6f7f1-ef69-4be5-b6a3-fdd3aeb1bf0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae71d0a-a80d-4b27-89fe-ed13a8dc6504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c3d43cb-1173-4b74-913f-b8cb7ca04c00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae71d0a-a80d-4b27-89fe-ed13a8dc6504",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "b75b172e-e998-4062-a213-faa178ca75a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "6658af27-08ce-415f-9db2-005cb7182408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75b172e-e998-4062-a213-faa178ca75a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac7f145-9d28-4891-ab70-03e1d63e24b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75b172e-e998-4062-a213-faa178ca75a8",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "cf9e9b76-0e21-4186-a4b9-ee3caba9fc5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "ad2e5d77-f01c-446e-86ab-d579a8ba476b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9e9b76-0e21-4186-a4b9-ee3caba9fc5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47854c40-52c5-4d08-8418-8c6202bca34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9e9b76-0e21-4186-a4b9-ee3caba9fc5f",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "393dc744-2066-4eb3-9abe-dcb7f2eb394a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "374c00c8-49de-4860-a22c-153d8057b264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393dc744-2066-4eb3-9abe-dcb7f2eb394a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adecae05-30c8-4609-8a0f-2961da82515f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393dc744-2066-4eb3-9abe-dcb7f2eb394a",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        },
        {
            "id": "ef62aff6-0ac6-4fe2-a45d-90f2819d02c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "compositeImage": {
                "id": "ee7ef788-defa-456e-996a-067304d7ca61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef62aff6-0ac6-4fe2-a45d-90f2819d02c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "048738e7-6c8f-4f58-aff8-15bd435c436a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef62aff6-0ac6-4fe2-a45d-90f2819d02c9",
                    "LayerId": "68944b0a-668a-44b8-a3c7-1983379d6c10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "68944b0a-668a-44b8-a3c7-1983379d6c10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6713a29-5934-429a-b30b-99f5f4b7cbf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 48
}