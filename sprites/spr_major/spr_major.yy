{
    "id": "6780b9c9-e5fd-4344-99b5-7103206deff5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_major",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b2ab7c8-846c-4c4b-873b-7536e62b53f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6780b9c9-e5fd-4344-99b5-7103206deff5",
            "compositeImage": {
                "id": "2e692f42-fa5f-4cad-ab39-191c66c1884f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b2ab7c8-846c-4c4b-873b-7536e62b53f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4de141f3-3178-41b2-837c-3534256cf11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b2ab7c8-846c-4c4b-873b-7536e62b53f0",
                    "LayerId": "d07c6670-53e6-41ca-94b0-fe73148a1ffc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "d07c6670-53e6-41ca-94b0-fe73148a1ffc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6780b9c9-e5fd-4344-99b5-7103206deff5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 0,
    "yorig": 0
}