{
    "id": "e5e6132e-2847-444d-a5ba-7c6a5525b0b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_penta",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 470,
    "bbox_left": 16,
    "bbox_right": 498,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "492135a2-18a0-44b6-b05f-a0696c451d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e6132e-2847-444d-a5ba-7c6a5525b0b3",
            "compositeImage": {
                "id": "b9b64b35-80d5-4a90-a7c4-e5aee432be95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492135a2-18a0-44b6-b05f-a0696c451d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c8ee77-20ed-4749-a76e-5dc9e0560756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492135a2-18a0-44b6-b05f-a0696c451d41",
                    "LayerId": "8ce6aabb-9ec9-4bc9-8658-719cdf29dd54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 487,
    "layers": [
        {
            "id": "8ce6aabb-9ec9-4bc9-8658-719cdf29dd54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e6132e-2847-444d-a5ba-7c6a5525b0b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 520,
    "xorig": 260,
    "yorig": 243
}