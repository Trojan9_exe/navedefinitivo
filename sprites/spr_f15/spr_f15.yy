{
    "id": "43530023-1e5e-4aa6-b9ea-161f89448a57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_f15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 9,
    "bbox_right": 74,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfd346a6-c5cb-492f-9eab-3ee16dde9305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "21e65e7d-7eb3-4df1-9756-20c6336b81b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd346a6-c5cb-492f-9eab-3ee16dde9305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01058520-98ec-4e9f-86e4-620b6bc8dd73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd346a6-c5cb-492f-9eab-3ee16dde9305",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "ec20817f-c33a-45a6-963e-94004cd0cbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "18c35bbe-8699-4253-aeae-692a0830783f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec20817f-c33a-45a6-963e-94004cd0cbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a7c4bf-4eb5-43ae-a780-24e8f8af831b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec20817f-c33a-45a6-963e-94004cd0cbef",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "ffb44b5f-1a38-4484-8a72-4188f5fef515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "f9154f13-9337-48f8-8df3-dd6e0872b63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb44b5f-1a38-4484-8a72-4188f5fef515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efef3f31-58e9-446b-96b0-fefad178c46e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb44b5f-1a38-4484-8a72-4188f5fef515",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "366dc530-2179-41ee-9cdf-f69163e57645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "41db2039-18c4-4cd3-b711-a959a11eeaf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "366dc530-2179-41ee-9cdf-f69163e57645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed00bfa8-7389-41bb-a869-66fe35ffc773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "366dc530-2179-41ee-9cdf-f69163e57645",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "15e4b40c-2727-4556-bc99-96cf04ca8877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "52870333-f2b4-48bc-82a1-6b4efaa9b164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15e4b40c-2727-4556-bc99-96cf04ca8877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8b1072-af1b-40d2-b05b-fd5d92ad4867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15e4b40c-2727-4556-bc99-96cf04ca8877",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "5c9c78b3-9fce-4f80-a968-bd545e3e0c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "46ec7b47-a0f8-47cf-92fd-4b4be54d5634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9c78b3-9fce-4f80-a968-bd545e3e0c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a94f6a-8e97-434c-ae8b-670cb60e71f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9c78b3-9fce-4f80-a968-bd545e3e0c4f",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        },
        {
            "id": "1f04ae9a-77d0-4e73-8d33-1f5fe5e6bcfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "compositeImage": {
                "id": "c8c72beb-88c8-4048-a1a9-1d5e84df45b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f04ae9a-77d0-4e73-8d33-1f5fe5e6bcfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0565ba68-2136-468d-a6b5-5598ba4d84ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f04ae9a-77d0-4e73-8d33-1f5fe5e6bcfe",
                    "LayerId": "710a8b58-cd44-40d6-b977-c81ab9ad93a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "710a8b58-cd44-40d6-b977-c81ab9ad93a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43530023-1e5e-4aa6-b9ea-161f89448a57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 42,
    "yorig": 50
}