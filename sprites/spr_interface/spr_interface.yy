{
    "id": "8a548b3c-fed9-4e94-9b2c-dd5858645e8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interface",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 229,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7e037c6-6504-4db5-8139-d9cef30d0bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a548b3c-fed9-4e94-9b2c-dd5858645e8d",
            "compositeImage": {
                "id": "db08ab22-0697-4405-a791-318a08d9cf60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e037c6-6504-4db5-8139-d9cef30d0bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "793e686b-fa99-417b-8c5a-65a3c2f6e3b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e037c6-6504-4db5-8139-d9cef30d0bbb",
                    "LayerId": "6ce619f4-a055-4e59-a969-f505ebfb5734"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "6ce619f4-a055-4e59-a969-f505ebfb5734",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a548b3c-fed9-4e94-9b2c-dd5858645e8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 230,
    "xorig": 115,
    "yorig": 45
}