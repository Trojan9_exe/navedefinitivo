{
    "id": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 138,
    "bbox_right": 243,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e2a3d69-c0dc-4a6d-9f8a-30e9f82f8d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
            "compositeImage": {
                "id": "54a43f29-3a46-4104-a487-ef593a07a110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2a3d69-c0dc-4a6d-9f8a-30e9f82f8d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b912ca3c-b678-45db-b82c-43906db06ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2a3d69-c0dc-4a6d-9f8a-30e9f82f8d38",
                    "LayerId": "7756d4a4-75ca-4329-95ce-df6ebad9dd86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "7756d4a4-75ca-4329-95ce-df6ebad9dd86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef28e913-22ff-463a-bba1-5e7c2ba13bc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 192
}