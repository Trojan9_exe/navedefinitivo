{
    "id": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exp2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 1,
    "bbox_right": 236,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea82035a-9a7c-4da9-893c-05ed1ed856e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "da9180c7-03bb-4e61-b745-371b019a19f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea82035a-9a7c-4da9-893c-05ed1ed856e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c67ec66-e122-4736-9626-e54ad1b0e552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea82035a-9a7c-4da9-893c-05ed1ed856e0",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "bd3d41cf-c2e4-4e73-b4a4-3a232a02ebec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "5eb9cbd5-d909-4a36-b42a-8d98db55b08d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd3d41cf-c2e4-4e73-b4a4-3a232a02ebec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261963a3-401f-451c-81d1-6ff2ccef2248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd3d41cf-c2e4-4e73-b4a4-3a232a02ebec",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "cf2a1aa7-895d-4c14-ab84-75f85d2d5d82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "65074e17-170c-43d3-831c-ae8eb80d2626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf2a1aa7-895d-4c14-ab84-75f85d2d5d82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57081071-b278-465a-88cc-692cdaef6e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf2a1aa7-895d-4c14-ab84-75f85d2d5d82",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "693556c9-4b2a-4e93-baaa-af8fc8e9c16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "32f04300-4387-4a7c-a87f-a5bfab16cb0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693556c9-4b2a-4e93-baaa-af8fc8e9c16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b262f7b5-9635-4175-bcd5-3a6db5a1916d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693556c9-4b2a-4e93-baaa-af8fc8e9c16f",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "d472d15c-2533-44b7-8970-7cb4831cb4d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "4fd0df52-8076-40da-87e5-62900d340af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d472d15c-2533-44b7-8970-7cb4831cb4d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460a2af1-522b-4e1c-b5d9-aa3897cab192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d472d15c-2533-44b7-8970-7cb4831cb4d6",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "a6699896-5672-4ceb-a8d9-62cef514c122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "3b25556e-567d-49fa-adf7-d7d8505818b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6699896-5672-4ceb-a8d9-62cef514c122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0178aa5d-7d5f-413a-b011-6c71151803e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6699896-5672-4ceb-a8d9-62cef514c122",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "5562f111-24de-400c-ae08-2e9d9b92194d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "dd2554fd-eea6-4e6e-8b53-9935bbef3a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5562f111-24de-400c-ae08-2e9d9b92194d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b67ec29-b4ca-49e2-93c8-72780a2b4fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5562f111-24de-400c-ae08-2e9d9b92194d",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        },
        {
            "id": "41a6ae10-5718-4fa6-ae6f-9119e0be4491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "compositeImage": {
                "id": "f3f84cf5-994f-4b59-a378-e7701bc76e01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a6ae10-5718-4fa6-ae6f-9119e0be4491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad77b1d-1008-4957-a331-62948935201e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a6ae10-5718-4fa6-ae6f-9119e0be4491",
                    "LayerId": "2dac97ca-5114-4ccb-9756-5bad5cc77d31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 239,
    "layers": [
        {
            "id": "2dac97ca-5114-4ccb-9756-5bad5cc77d31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23d5bdd2-eca3-4298-b147-a74538ca5f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 237,
    "xorig": 118,
    "yorig": 119
}