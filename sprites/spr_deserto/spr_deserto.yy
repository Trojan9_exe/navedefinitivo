{
    "id": "d55746f6-28d8-4366-b144-7f2bd06f86f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deserto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 720,
    "bbox_left": 0,
    "bbox_right": 1280,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9898587-90de-49a5-a850-e5aff5aa0541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d55746f6-28d8-4366-b144-7f2bd06f86f2",
            "compositeImage": {
                "id": "8122cd32-3370-4ff7-b908-1362304d8809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9898587-90de-49a5-a850-e5aff5aa0541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b13e845-540b-4509-b273-364602e5e9e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9898587-90de-49a5-a850-e5aff5aa0541",
                    "LayerId": "33f59d5a-48b8-410c-a4a1-a7311e8497f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 721,
    "layers": [
        {
            "id": "33f59d5a-48b8-410c-a4a1-a7311e8497f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d55746f6-28d8-4366-b144-7f2bd06f86f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1281,
    "xorig": 640,
    "yorig": 360
}