{
    "id": "1184f470-95f0-49c0-a9fe-5587168635af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Anti",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de125e81-3a53-4e43-9c14-ee0744a7e117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1184f470-95f0-49c0-a9fe-5587168635af",
            "compositeImage": {
                "id": "339365ef-f0b3-48a2-8f75-22462e777089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de125e81-3a53-4e43-9c14-ee0744a7e117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb54522-2bc1-4177-abe2-a50b4d87aeed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de125e81-3a53-4e43-9c14-ee0744a7e117",
                    "LayerId": "d4fe9559-2b16-4ad6-848a-4a6e2b87fb5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 102,
    "layers": [
        {
            "id": "d4fe9559-2b16-4ad6-848a-4a6e2b87fb5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1184f470-95f0-49c0-a9fe-5587168635af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 32,
    "yorig": 87
}