{
    "id": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31d3b9ce-d4ba-4f25-9cda-89a5627fd55c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "21541a82-38a7-46d0-9633-f03dbc641c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d3b9ce-d4ba-4f25-9cda-89a5627fd55c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00cde700-80f1-4d3e-8ee1-4896bf1cb3ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d3b9ce-d4ba-4f25-9cda-89a5627fd55c",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "074e79b0-4b13-4d5c-8237-d23ea6eca0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "ac9ef655-b2d1-4da6-beeb-fb4848a37d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074e79b0-4b13-4d5c-8237-d23ea6eca0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad50108-8496-4daf-b906-f803a45ec546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074e79b0-4b13-4d5c-8237-d23ea6eca0f6",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "2fa2ec15-dcf1-4058-a84c-654d21ee0321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "2a553bf1-5f9f-49c6-b600-87a60102536a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa2ec15-dcf1-4058-a84c-654d21ee0321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f46502-518e-4287-aabd-bfb3ffda68dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa2ec15-dcf1-4058-a84c-654d21ee0321",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "9feb308e-4b2b-4442-8420-9ff7d56299f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "631a2fa8-0eac-4dd0-b4f4-c1429dc5ad06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9feb308e-4b2b-4442-8420-9ff7d56299f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8dd821-731f-417f-b084-1d8905e8a326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9feb308e-4b2b-4442-8420-9ff7d56299f8",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "61f58b42-f7b1-49af-9772-d3c9922322f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "54cb2932-e5b4-402d-bd0b-f9cf724e5509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f58b42-f7b1-49af-9772-d3c9922322f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca2159e-6c44-4808-a46a-deac339a69a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f58b42-f7b1-49af-9772-d3c9922322f7",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "63ed37de-9ba6-4c4a-8131-592c5e54160c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "b5511bde-a39d-455b-bf8a-7f69c6a4672f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ed37de-9ba6-4c4a-8131-592c5e54160c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c1fdbb-a172-4c17-a6ca-f804b8efa20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ed37de-9ba6-4c4a-8131-592c5e54160c",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "98820015-5c25-4982-9127-affa8b936290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "5b25210c-3a0e-4c77-a64d-ea0897b44d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98820015-5c25-4982-9127-affa8b936290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad1d21f-8069-443f-b314-fd25859eb19c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98820015-5c25-4982-9127-affa8b936290",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "6886adb7-bd8c-4627-b9d4-a1c2959614c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "4c82fe46-e377-400b-8ac1-f34af2e31461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6886adb7-bd8c-4627-b9d4-a1c2959614c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c4e49e0-3471-4ea8-b1f9-20adda30d1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6886adb7-bd8c-4627-b9d4-a1c2959614c8",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "afc27906-1b75-4fbf-9a3a-87cea92ef6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "4bc6f110-2fac-4bce-8b48-30ee6392b537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc27906-1b75-4fbf-9a3a-87cea92ef6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e75544e-56da-4463-b418-5005bcb5b639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc27906-1b75-4fbf-9a3a-87cea92ef6f6",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "8dd2e0ba-66e1-4a51-9c5c-6a82eb50689c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "004d59a7-018c-4f8d-877e-89d006938caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd2e0ba-66e1-4a51-9c5c-6a82eb50689c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a212b6f8-b953-4e47-bc7d-b02afab9de38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd2e0ba-66e1-4a51-9c5c-6a82eb50689c",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "097de100-fc01-4437-ba43-ec3cc44972cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "97146e6c-0005-4e65-9771-45c43c845f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097de100-fc01-4437-ba43-ec3cc44972cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e939d9e4-f174-411f-b12d-f924a46c3c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097de100-fc01-4437-ba43-ec3cc44972cc",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "853b6c18-7621-4fab-90cf-125ce541640b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "ecd0b448-1b45-470b-b9cb-055c380fb4d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853b6c18-7621-4fab-90cf-125ce541640b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86cce690-7602-41cf-ba3b-ed160386c7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853b6c18-7621-4fab-90cf-125ce541640b",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "c954dd28-ad1e-4ac9-af0b-4e571a5118c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "ebe162ee-cfe9-4424-8154-8d86205b98c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c954dd28-ad1e-4ac9-af0b-4e571a5118c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c24842-5fa2-4ce0-9c3c-e3adbaa11499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c954dd28-ad1e-4ac9-af0b-4e571a5118c0",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "ae381850-2cb6-4571-b57a-132c264919f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "d5afa4d0-2f92-49e5-aa9e-854f8d2098e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae381850-2cb6-4571-b57a-132c264919f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34629c6f-154f-41a4-8890-3f558e2d8e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae381850-2cb6-4571-b57a-132c264919f8",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "aea15900-f819-4cf8-9119-761a5e87d556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "fcb3805c-4593-4327-94ea-53784c781f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea15900-f819-4cf8-9119-761a5e87d556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3cf374-fda2-4d81-938f-7dc558d62cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea15900-f819-4cf8-9119-761a5e87d556",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "1f77bd2e-79e0-4ce0-9216-d8e6fa9da469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "e4f6281b-7d27-4a79-99dc-33ba64dc4df3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f77bd2e-79e0-4ce0-9216-d8e6fa9da469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36c4ff8-b69d-4af8-a836-fe600a698c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f77bd2e-79e0-4ce0-9216-d8e6fa9da469",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "b38ae67d-094b-40c5-84e0-32a004e35721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "aa584c03-83ae-48f1-99c3-7a262bbc547e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38ae67d-094b-40c5-84e0-32a004e35721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc3f40f6-56ac-4306-ba84-337ba2966f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38ae67d-094b-40c5-84e0-32a004e35721",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "ce6dd2f2-a8b4-4138-aff0-eb1bf0ec0a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "8e8d84de-81c1-4040-ab06-cca352a2df42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6dd2f2-a8b4-4138-aff0-eb1bf0ec0a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca5aefa-4401-40f9-a88d-97f4df3d4c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6dd2f2-a8b4-4138-aff0-eb1bf0ec0a0c",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "d46a57ce-ee02-45eb-b68d-7d138ea9ad8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "87b230fc-75f0-4520-86de-95bc614340c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46a57ce-ee02-45eb-b68d-7d138ea9ad8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a719f03d-5700-4242-9542-7ffad1434c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46a57ce-ee02-45eb-b68d-7d138ea9ad8d",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "570f3f1b-c41f-461c-af0c-22124f236076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "de5f3d61-da74-488b-93b4-7b1000bc8eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570f3f1b-c41f-461c-af0c-22124f236076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a13e078-2dff-4c22-bba3-70cc17cceda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570f3f1b-c41f-461c-af0c-22124f236076",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "feb43807-0da9-4efe-9d8f-b7e793e9a753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "3c9b9800-ab23-4999-b298-d220faa2fdda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb43807-0da9-4efe-9d8f-b7e793e9a753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25a4ecb-bf34-4f39-955e-5e333a0d64bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb43807-0da9-4efe-9d8f-b7e793e9a753",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "58fd8aa9-e733-4af2-b552-c98440952a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "566d137d-2da1-4c38-8795-84848ea7177d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58fd8aa9-e733-4af2-b552-c98440952a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69525563-994c-453d-921c-2115e38e4f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58fd8aa9-e733-4af2-b552-c98440952a00",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "e6fa3e78-329b-4583-a2cc-eee42f627eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "5692b7cd-ad87-4443-9f2c-cd1fe0137c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6fa3e78-329b-4583-a2cc-eee42f627eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc77a12-d8bf-41c3-bd74-52c966d06d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6fa3e78-329b-4583-a2cc-eee42f627eba",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "feb97a38-45b1-4ada-a5ca-47c26fabbf5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "f513b485-2e3c-4301-868a-0a709f9dd204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb97a38-45b1-4ada-a5ca-47c26fabbf5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad8d182e-626c-4e9c-9bcc-51ebe47d1e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb97a38-45b1-4ada-a5ca-47c26fabbf5b",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "e49badb4-f419-4a20-8532-14dbccead7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "d9d1a50a-2bf6-4894-8990-0ae5624edebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49badb4-f419-4a20-8532-14dbccead7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8fd828-0532-47dd-93f6-ad5f47187934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49badb4-f419-4a20-8532-14dbccead7fd",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "ca55f274-7bb8-4625-9a52-66d599ad53c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "9f998b45-b3d8-4815-9d16-418956540c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca55f274-7bb8-4625-9a52-66d599ad53c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c9b8a2-996f-4b92-8f79-fddeed2e8829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca55f274-7bb8-4625-9a52-66d599ad53c6",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "73cac713-a959-46da-97ce-80a691e7b667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "437ee339-751b-4f87-8eeb-4146481f4a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cac713-a959-46da-97ce-80a691e7b667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395cac4f-8d2b-4317-ab0a-ea0a72aa2f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cac713-a959-46da-97ce-80a691e7b667",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "b4b9fc32-e5f4-450f-9ef7-e87068350681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "8b6ca8a4-fe6c-451a-a65d-22844d19cd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b9fc32-e5f4-450f-9ef7-e87068350681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdecf2e7-1ab9-458c-9749-fc4350d2c699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b9fc32-e5f4-450f-9ef7-e87068350681",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "85b06970-b228-4518-92d3-429d9190efd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "9629e8f8-77d9-40d2-9299-e2e68d09da8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b06970-b228-4518-92d3-429d9190efd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3d3036-ea4d-4c5f-b1ba-e6492eea540e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b06970-b228-4518-92d3-429d9190efd4",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "25a13216-4f58-4275-a643-0d919b57fb82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "2a5dd804-d676-4e3f-a4ea-62478ed961fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a13216-4f58-4275-a643-0d919b57fb82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14232a51-1df2-4675-bf9d-bca320a21d94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a13216-4f58-4275-a643-0d919b57fb82",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "be020027-7488-4550-b781-fc9e9e3f11d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "afcae6e0-8f53-483e-8e91-2d4877fc762a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be020027-7488-4550-b781-fc9e9e3f11d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1ac7ca-716b-4cce-b9ab-cba7806b2cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be020027-7488-4550-b781-fc9e9e3f11d9",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        },
        {
            "id": "8026501e-fd48-4581-b275-55cf52f5306e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "compositeImage": {
                "id": "958247bd-025b-4591-9e7c-95a94ef0ef14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8026501e-fd48-4581-b275-55cf52f5306e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7112fba-434a-40a3-8ce9-b5378639074b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8026501e-fd48-4581-b275-55cf52f5306e",
                    "LayerId": "9885b5a6-92b4-4669-9a22-2df20362dbc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9885b5a6-92b4-4669-9a22-2df20362dbc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4056afd2-193d-4f11-88ba-0fc1e2b5b8bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}