{
    "id": "03856174-7a81-4b18-b956-656d5a86420a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vida",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 17,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dacead31-708e-48a6-8fd7-15e4c856e182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03856174-7a81-4b18-b956-656d5a86420a",
            "compositeImage": {
                "id": "af4ac85d-e919-4485-939f-127e63236870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dacead31-708e-48a6-8fd7-15e4c856e182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6115a1f0-769e-45ea-888a-2560f7127c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dacead31-708e-48a6-8fd7-15e4c856e182",
                    "LayerId": "4f1cfac3-926c-4a5c-9cda-f82beaaf3535"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "4f1cfac3-926c-4a5c-9cda-f82beaaf3535",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03856174-7a81-4b18-b956-656d5a86420a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}