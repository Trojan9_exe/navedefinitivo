{
    "id": "b9fdf3c0-9f65-46e0-bf56-0cdd6a32a55d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_terrain_desert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "737f0fca-287b-433e-af06-e99dd4deafc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9fdf3c0-9f65-46e0-bf56-0cdd6a32a55d",
            "compositeImage": {
                "id": "8cb3c22b-5799-4bda-b773-34b454d83fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "737f0fca-287b-433e-af06-e99dd4deafc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "795b80de-fb47-4be4-b09f-73cf7d6e8f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "737f0fca-287b-433e-af06-e99dd4deafc4",
                    "LayerId": "3539e6f5-dc7e-4bdd-878f-d601a8cf720c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "3539e6f5-dc7e-4bdd-878f-d601a8cf720c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9fdf3c0-9f65-46e0-bf56-0cdd6a32a55d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}