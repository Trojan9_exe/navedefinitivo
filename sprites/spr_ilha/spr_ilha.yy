{
    "id": "85faf633-3672-4467-a636-4f6cbf173dc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ilha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 15,
    "bbox_right": 285,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c91cba3f-3392-4294-a9e4-73dbcf4778bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85faf633-3672-4467-a636-4f6cbf173dc7",
            "compositeImage": {
                "id": "5c234e42-b1f5-4df8-b745-ccfb0c56a0c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91cba3f-3392-4294-a9e4-73dbcf4778bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618006a5-e003-40af-9d97-a66cc5bcc392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91cba3f-3392-4294-a9e4-73dbcf4778bc",
                    "LayerId": "2ab98abf-5e30-4830-b580-63f59f0a2e51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 410,
    "layers": [
        {
            "id": "2ab98abf-5e30-4830-b580-63f59f0a2e51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85faf633-3672-4467-a636-4f6cbf173dc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}