{
    "id": "d6a42be2-5ed5-4023-8cab-e14adfc16220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BaseaAmericana",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a761dd8-3db8-4efe-9c62-168658a84db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6a42be2-5ed5-4023-8cab-e14adfc16220",
            "compositeImage": {
                "id": "fa54ef7c-e93a-4472-bdaa-cba89e51c8dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a761dd8-3db8-4efe-9c62-168658a84db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc93b45-0f3c-4c94-8a28-d815fd9db72d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a761dd8-3db8-4efe-9c62-168658a84db2",
                    "LayerId": "3f0e97c7-f37f-4824-b4d3-5b4474c4fafb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "3f0e97c7-f37f-4824-b4d3-5b4474c4fafb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6a42be2-5ed5-4023-8cab-e14adfc16220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}