{
    "id": "1ba96737-f2b3-4ae9-b72c-19ab8d84392b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tanque",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c31f3770-44b6-49b0-b0bd-8f3a942d5f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba96737-f2b3-4ae9-b72c-19ab8d84392b",
            "compositeImage": {
                "id": "ebc23d5b-5fb3-4e30-9a70-a4d32e320313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c31f3770-44b6-49b0-b0bd-8f3a942d5f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6df12be-e926-4b77-a30f-3c02eefa9f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c31f3770-44b6-49b0-b0bd-8f3a942d5f51",
                    "LayerId": "3942f3a7-7846-4291-8c1a-9a6c64298cf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "3942f3a7-7846-4291-8c1a-9a6c64298cf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ba96737-f2b3-4ae9-b72c-19ab8d84392b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 50
}