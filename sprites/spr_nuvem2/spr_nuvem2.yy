{
    "id": "da20e99a-ee46-4026-bf7f-636709d5160b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nuvem2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 526,
    "bbox_left": 28,
    "bbox_right": 896,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53c8dac1-dd95-4ea4-b62d-0b9d4ab415b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da20e99a-ee46-4026-bf7f-636709d5160b",
            "compositeImage": {
                "id": "7e810709-49fd-430d-816f-52b2f5cb271e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53c8dac1-dd95-4ea4-b62d-0b9d4ab415b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0245217e-54af-499d-84a1-fc6c6b992061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c8dac1-dd95-4ea4-b62d-0b9d4ab415b4",
                    "LayerId": "32376cf9-de9c-4f6a-86eb-2ed334835ccc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 534,
    "layers": [
        {
            "id": "32376cf9-de9c-4f6a-86eb-2ed334835ccc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da20e99a-ee46-4026-bf7f-636709d5160b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 35,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 950,
    "xorig": 475,
    "yorig": 267
}