{
    "id": "6de2ec2d-e180-4b60-94ba-c34735e6daad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_intro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea61f7bf-baaa-4021-bfa6-e786fcbea60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6de2ec2d-e180-4b60-94ba-c34735e6daad",
            "compositeImage": {
                "id": "c9b775d9-053e-4315-9ebd-e8012f711a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea61f7bf-baaa-4021-bfa6-e786fcbea60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "259ac1e7-5643-4762-81fa-8ba782fdbca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea61f7bf-baaa-4021-bfa6-e786fcbea60d",
                    "LayerId": "4ee4c382-78e7-40ea-8217-f6ffe6944801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "4ee4c382-78e7-40ea-8217-f6ffe6944801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6de2ec2d-e180-4b60-94ba-c34735e6daad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}