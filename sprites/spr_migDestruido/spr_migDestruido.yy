{
    "id": "8078e41f-4437-48f1-a2e9-5d19ac6b963c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_migDestruido",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 11,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08ca9138-02bb-46eb-b456-502960cd4dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8078e41f-4437-48f1-a2e9-5d19ac6b963c",
            "compositeImage": {
                "id": "87ab60bc-dc76-47e0-86d8-986318a94c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ca9138-02bb-46eb-b456-502960cd4dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b855dc-e92c-47ab-95ba-50fe7d933cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ca9138-02bb-46eb-b456-502960cd4dfe",
                    "LayerId": "ca08504f-5fc7-40f0-bbd3-3422f7040c03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "ca08504f-5fc7-40f0-bbd3-3422f7040c03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8078e41f-4437-48f1-a2e9-5d19ac6b963c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 50
}