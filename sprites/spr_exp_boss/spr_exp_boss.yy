{
    "id": "6a6fa47c-0bce-4f92-9021-200153897e6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exp_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 1,
    "bbox_right": 236,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ff180c1-5653-4171-a988-6918cb0919f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "e41b205e-6389-4e7a-ba8a-b2cad4b05b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff180c1-5653-4171-a988-6918cb0919f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be31e2b3-495c-4dab-9f3d-2e771be7419f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff180c1-5653-4171-a988-6918cb0919f4",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "70b99647-b707-46e2-a027-7116ce978513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "54cd087e-da94-4416-ba77-0459870c3271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b99647-b707-46e2-a027-7116ce978513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9505f45-feaa-4f9a-8bfe-1c9602cd66dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b99647-b707-46e2-a027-7116ce978513",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "8836272d-cf2c-4c49-8a85-7f3152535f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "a418e143-7d3c-4ab9-80ea-a351ffc5ea7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8836272d-cf2c-4c49-8a85-7f3152535f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63472628-5a0d-41ff-acbc-781b4d06f8a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8836272d-cf2c-4c49-8a85-7f3152535f79",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "aec6ce49-8b10-498a-be55-f25f9f62b291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "f49df6ca-ce53-4683-8737-bbc7f2c99509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec6ce49-8b10-498a-be55-f25f9f62b291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d97a2ff-2733-41b7-80f9-427abb167028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec6ce49-8b10-498a-be55-f25f9f62b291",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "cb0c7bb3-dde1-4f06-a23b-53244a5e6e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "c68dead8-78d9-4b6b-92ab-2f89d59aa524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0c7bb3-dde1-4f06-a23b-53244a5e6e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c007b71c-0971-4f66-8407-35ac3dd0b014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0c7bb3-dde1-4f06-a23b-53244a5e6e14",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "8a16c343-c937-4e50-aa72-ec53ddaa868d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "eb90b630-3fa5-46cf-9e14-8ed5bf756219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a16c343-c937-4e50-aa72-ec53ddaa868d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169aa440-b1ac-4726-b00a-4f942ef31796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a16c343-c937-4e50-aa72-ec53ddaa868d",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "ac29eebf-c9f8-427f-9bc2-3dea72502da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "4c0a6df8-8dd3-4b24-84e8-4fd088917aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac29eebf-c9f8-427f-9bc2-3dea72502da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe1c21c-9e07-486a-9875-8d61c0bc7dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac29eebf-c9f8-427f-9bc2-3dea72502da2",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        },
        {
            "id": "83ac33c8-57c4-4037-baee-983c48ebb3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "compositeImage": {
                "id": "9b8b2256-2abc-4357-b3c9-40fed8e1c2a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ac33c8-57c4-4037-baee-983c48ebb3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822aff3e-72f3-447e-a2a4-fb58c96fd504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ac33c8-57c4-4037-baee-983c48ebb3bf",
                    "LayerId": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 239,
    "layers": [
        {
            "id": "6f665ad1-5773-40ea-9046-c8b2a6df3ae9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6fa47c-0bce-4f92-9021-200153897e6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 237,
    "xorig": 380,
    "yorig": 87
}