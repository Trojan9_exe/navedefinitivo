{
    "id": "481b3994-20d0-40ed-8fcd-dedd7e3f1cc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiros",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97757189-9fed-4efd-9da1-3601814bc9cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "481b3994-20d0-40ed-8fcd-dedd7e3f1cc9",
            "compositeImage": {
                "id": "9f403d48-a868-4843-aa5e-d540fb3cf2da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97757189-9fed-4efd-9da1-3601814bc9cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125c8bcd-a38b-4204-9945-ebf5757d39ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97757189-9fed-4efd-9da1-3601814bc9cf",
                    "LayerId": "0b4456c8-7f61-472c-a20d-d5b6608e4ecc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b4456c8-7f61-472c-a20d-d5b6608e4ecc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "481b3994-20d0-40ed-8fcd-dedd7e3f1cc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}