{
    "id": "d87539eb-78b0-4424-9e2a-e26a15140a54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TankDestruido",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "98ee6e74-67f5-4ed4-a6a5-014776dc2d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d87539eb-78b0-4424-9e2a-e26a15140a54",
            "compositeImage": {
                "id": "d466e922-5f7c-48a0-9aa9-8732e3c00e3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ee6e74-67f5-4ed4-a6a5-014776dc2d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c7bd62-a72a-418d-9fee-156855e46f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ee6e74-67f5-4ed4-a6a5-014776dc2d03",
                    "LayerId": "ff821092-ce27-4d56-b730-fb6b80e86537"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ff821092-ce27-4d56-b730-fb6b80e86537",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d87539eb-78b0-4424-9e2a-e26a15140a54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 50
}