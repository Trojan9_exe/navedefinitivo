{
    "id": "018e5659-d710-44c8-9a9b-28001125ca06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 105,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30a65e4b-2765-48d3-8fd2-e3d858af5e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "710b3f88-156d-41fb-83ca-1318e0525933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a65e4b-2765-48d3-8fd2-e3d858af5e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cff1076-9ca1-404d-96fb-92a982be1279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a65e4b-2765-48d3-8fd2-e3d858af5e91",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "5524ae97-261a-4ff2-adba-67c7207ad10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "6b59ee49-9c7a-4b32-9b8f-fe38db49a048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5524ae97-261a-4ff2-adba-67c7207ad10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307a3305-fb58-4c0c-b2b7-3e274e23f6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5524ae97-261a-4ff2-adba-67c7207ad10c",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "7af371fe-1efb-46e2-bf93-07a6bcb01512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "450b003b-9a4b-4f64-907a-338b4260b7ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7af371fe-1efb-46e2-bf93-07a6bcb01512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde3ce73-3709-4e47-9e0e-99174774e03f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7af371fe-1efb-46e2-bf93-07a6bcb01512",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "130b25ed-1bb8-4549-ad3e-2d90721393be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "4f360e62-3ffc-480d-871e-d111b48e6420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130b25ed-1bb8-4549-ad3e-2d90721393be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162f4602-a4d6-4f95-9c83-9a35b26451a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130b25ed-1bb8-4549-ad3e-2d90721393be",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "a06c9094-885e-4491-82f8-1dd0ff585362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "f14145a4-6696-431b-81ad-69c434cf2261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06c9094-885e-4491-82f8-1dd0ff585362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f824621e-f287-47f0-a393-a9baa4784583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06c9094-885e-4491-82f8-1dd0ff585362",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "f5790c80-e0fa-417a-a8a5-5e098ec8ded2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "a98bdf20-5cfe-49ac-b477-0dd071e0853d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5790c80-e0fa-417a-a8a5-5e098ec8ded2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93886fb8-c239-4e6f-ae78-144b053ee566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5790c80-e0fa-417a-a8a5-5e098ec8ded2",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "85cab6b2-cc4b-4566-af2b-72d98210d2af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "973552d1-2e0a-4484-9cc2-67124f731421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85cab6b2-cc4b-4566-af2b-72d98210d2af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041581c8-e90d-475f-8566-e07238bfafa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85cab6b2-cc4b-4566-af2b-72d98210d2af",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        },
        {
            "id": "34d70180-a6aa-410d-8bda-b1715b0e378e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "compositeImage": {
                "id": "bc06e012-fca9-4409-8f9c-e02bd9bcfc52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34d70180-a6aa-410d-8bda-b1715b0e378e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ec14b4a-f2c2-4daf-bf03-c4858ecc63e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34d70180-a6aa-410d-8bda-b1715b0e378e",
                    "LayerId": "ded99689-3724-407a-aaf7-551d30c69f55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ded99689-3724-407a-aaf7-551d30c69f55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "018e5659-d710-44c8-9a9b-28001125ca06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 14,
    "yorig": 15
}