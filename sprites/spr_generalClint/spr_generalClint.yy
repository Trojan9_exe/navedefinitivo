{
    "id": "43682300-1404-43c9-9cbe-a500b53a6435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_generalClint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38d6a2b4-16d8-4653-8229-06731ee1dcfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43682300-1404-43c9-9cbe-a500b53a6435",
            "compositeImage": {
                "id": "be735d69-cd96-4121-9c10-86f8407a9963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d6a2b4-16d8-4653-8229-06731ee1dcfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aeaf9a3-fb54-4bc5-8837-4c42f1086044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d6a2b4-16d8-4653-8229-06731ee1dcfc",
                    "LayerId": "88802442-1323-4c07-9207-51a86114b264"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "88802442-1323-4c07-9207-51a86114b264",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43682300-1404-43c9-9cbe-a500b53a6435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 0,
    "yorig": 0
}