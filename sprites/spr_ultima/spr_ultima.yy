{
    "id": "38d86c5d-9f86-4dcc-9ade-f0f5f68fa567",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ultima",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "595551fb-6a9a-4722-a3fa-872ac7845970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d86c5d-9f86-4dcc-9ade-f0f5f68fa567",
            "compositeImage": {
                "id": "a5c35d6c-c5b6-4ef2-8484-e6efab04db78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595551fb-6a9a-4722-a3fa-872ac7845970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af05370-42f0-4fcc-b502-2df3d96bb456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595551fb-6a9a-4722-a3fa-872ac7845970",
                    "LayerId": "1e60eb73-7f4d-452f-b234-31fb9b719f9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "1e60eb73-7f4d-452f-b234-31fb9b719f9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d86c5d-9f86-4dcc-9ade-f0f5f68fa567",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}