{
    "id": "c97bd99f-a130-4add-81f5-3d0b7cf2511f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Aux",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fa57fcc-bda9-4185-a1ad-9290435a99f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c97bd99f-a130-4add-81f5-3d0b7cf2511f",
            "compositeImage": {
                "id": "3ff5492b-b720-459d-b33d-33ac683592d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa57fcc-bda9-4185-a1ad-9290435a99f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d85c356-127b-43a4-8c11-0c520f915076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa57fcc-bda9-4185-a1ad-9290435a99f2",
                    "LayerId": "620fec80-e9fa-42cd-8180-c4c5bbdcd6ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "620fec80-e9fa-42cd-8180-c4c5bbdcd6ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c97bd99f-a130-4add-81f5-3d0b7cf2511f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}