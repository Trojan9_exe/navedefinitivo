{
    "id": "6fe5aed6-1cb3-4ee2-9fec-7acc79e3ed28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barras",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4e34a2e-6854-41ae-8d4d-62f6260e82ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fe5aed6-1cb3-4ee2-9fec-7acc79e3ed28",
            "compositeImage": {
                "id": "9e9b4e6f-ade8-4b97-bb16-13744e1adc72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e34a2e-6854-41ae-8d4d-62f6260e82ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b8a7fbf-94f3-4039-bca1-ecd2beb2ed8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e34a2e-6854-41ae-8d4d-62f6260e82ab",
                    "LayerId": "1e5ee284-ebc4-4664-9053-6a34551cf474"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "1e5ee284-ebc4-4664-9053-6a34551cf474",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fe5aed6-1cb3-4ee2-9fec-7acc79e3ed28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}