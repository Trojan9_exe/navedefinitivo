{
    "id": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "T_TUT_1_1",
    "momentList": [
        {
            "id": "be1fa976-ad53-40fa-8236-ac654f3f6d77",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "2886aae2-8bd3-4797-acaa-93a08f98c08e",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 0
        },
        {
            "id": "189efbcf-43e8-42d0-b547-4fff03e034be",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "38dd7a3d-59e3-4166-8034-b69ac30a3e71",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 10,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 10
        },
        {
            "id": "9d35dc44-0fc6-491f-89d0-e3a41811d513",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "5137fe58-ceae-4d8a-b81e-72a48a1251a2",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 20,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 20
        },
        {
            "id": "c8d99df1-cf57-46f8-a16c-00831e014370",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "2fbb62de-9634-47a9-bf12-d6ea2a66e40d",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 30
        },
        {
            "id": "c962b1be-3a70-4894-94cb-8716f05c032c",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "e4ae8c3a-2fe8-4980-9c83-74691cb5c04b",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 40,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 40
        },
        {
            "id": "73d347ba-ae86-4aac-86eb-a2fa678e0298",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "5009c70d-4826-404d-a0d8-3151efa6dcb2",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 60,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 60
        },
        {
            "id": "bdb2c7c1-4e48-44be-9bd9-26eef2c32338",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "179d92da-e61b-4c48-9f81-e4c7d0e5e32c",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 90,
                "eventtype": 0,
                "m_owner": "bfd82cc9-71bc-4cbf-bbb1-fb6704b0a28c"
            },
            "moment": 90
        }
    ]
}