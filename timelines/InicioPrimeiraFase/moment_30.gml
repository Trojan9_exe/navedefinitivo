///@description finaliza diálogo e começa a missão
if(obj_cutscene.next == 28) {
	var obj_cut = instance_find(obj_cutscene, 0);
	obj_cutscene.SPPiloto = false;
	obj_cutscene.SPGeneral = false;
	obj_cutscene.podeDigitar = false;
	instance_destroy(obj_cut);
	timeline_goto_next();
}
else timeline_position -= 1;