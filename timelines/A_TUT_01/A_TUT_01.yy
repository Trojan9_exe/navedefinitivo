{
    "id": "55d674a7-976e-4927-8efa-072d922a12dc",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "A_TUT_01",
    "momentList": [
        {
            "id": "7c7d54be-043e-4486-a2e8-a3a275e04bc7",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "19c9e1bc-5392-409e-8672-b23abda0f02e",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "55d674a7-976e-4927-8efa-072d922a12dc"
            },
            "moment": 0
        },
        {
            "id": "d230280e-f483-458e-a9c0-7445dfc4bc75",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "9c8d3b2c-d4d5-4e88-b5bd-cf535687a132",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "55d674a7-976e-4927-8efa-072d922a12dc"
            },
            "moment": 30
        },
        {
            "id": "7db3320d-b2fa-4d2e-a410-8721b8ff5c4a",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "df5c561e-24d7-4da2-8a9b-b71899074d3b",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 45,
                "eventtype": 0,
                "m_owner": "55d674a7-976e-4927-8efa-072d922a12dc"
            },
            "moment": 45
        },
        {
            "id": "6bf82c20-87ef-4821-8a43-f2d3a83374ce",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "2a7dd1f1-b7b6-4a92-9770-a90e42666941",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 100,
                "eventtype": 0,
                "m_owner": "55d674a7-976e-4927-8efa-072d922a12dc"
            },
            "moment": 100
        },
        {
            "id": "4a7c6a75-603f-4cc9-be4c-98b8b889944b",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "a1e05aac-b2f0-4e52-b242-33a38d3a0d19",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 120,
                "eventtype": 0,
                "m_owner": "55d674a7-976e-4927-8efa-072d922a12dc"
            },
            "moment": 120
        }
    ]
}