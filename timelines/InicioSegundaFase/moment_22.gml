///@description finaliza diálogo e começa a missão
if(obj_cutscene.next == 42) {
	var obj_cut = instance_find(obj_cutscene, 0);
	obj_cut.SPPiloto = false;
	obj_cut.SPGeneral = false;
	obj_cut.podeDigitar = false;
	obj_cut.US_Base = false;
	instance_destroy(obj_cut);
	timeline_goto_next();
}
else timeline_position -= 1;