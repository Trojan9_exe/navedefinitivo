/// @description iniciar no centro da tela?

if(global.showPlayerInterface && instance_exists(obj_F_15)){
	var player = instance_find(obj_F_15,0);
	player.interacao = false;
	
	switch(global.direcao){
		case 0:
		var playerx = room_width/2;
		var playery = room_height-150;
		break;
		case 1:
		var playerx = 150;
		var playery = room_height/2;
		break;
		case 2:
		var playerx = room_width/2;
		var playery = 150;
		break;
		case 3:
		var playerx = room_width-150;
		var playery = room_height/2;
		break;
	}
	
	if(player.x!=playerx || player.y!=playery)
	{
		player.x = Approach(player.x, playerx, player.walkingSpeed/2);
		player.y = Approach(player.y, playery, player.walkingSpeed/2);
	}
	else{
		player.interacao = true;
		instance_destroy();
	}
}