/// @description CONFIGURAR LEVEL - ETAPA
// PLAYER CONFIG
global.direcao = 1; //0:baixo 1:esquerda 2:cima 3:direita 
global.AuxHP = 100;
global.AuxHP2 = 100;
global.podeCriar = true;
global.texto = true;
global.HaveBombsAux = 3;
global.HaveBombsAux2 = 3;
global.vidas = 3;
global.pontos = 0;
global.haveBombsLeft = 3;
global.energia_MAX = global.game_dificulty * 300;
global.player_tiro_damage = global.game_dificulty * 10;
global.player_self_kill_damage = global.game_dificulty * 100;
global.player_bomb_damage = global.game_dificulty * 300;
global.showPlayerInterface = true;
global.start_countdown = 0;
global.level_countdown = timeline_max_moment(timeline_index);
global.game_debug = false;
global.endgame = false;
global.inimigo_speed = 8;
startRoomTimeline(LOOP_30_STEPS_NUVEM, 0, 1, 1);

// ENEMIES CONFIG
global.enemy_tiro_damage = global.game_dificulty * 10;
global.enemy_self_kill_damage = global.game_dificulty * 10;
global.enemyHP = global.game_dificulty*20;

// BOSS CONFIG
global.boss_tiro_damage =  global.game_dificulty * 30;
global.boss_self_kill_damage = global.game_dificulty * 1000;
global.BossHP = global.game_dificulty * 300;
global.floor_distance = 30;

//BACKGROUND
var lay_id = layer_get_id("Background");
var back_id = layer_background_get_id(lay_id);
layer_background_sprite(back_id, spr_deserto);
layer_background_htiled(back_id, true);
layer_background_vtiled(back_id, true);
layer_background_xscale(back_id, 1);
layer_background_yscale(back_id, 1);
var lay_speed = 8;
layer_vspeed(lay_id, ((global.direcao==0)-(global.direcao==2))*lay_speed);
layer_hspeed(lay_id, ((global.direcao==3)-(global.direcao==1))*lay_speed);
if(global.showPlayerInterface && !instance_exists(obj_F_15)) // HORA DE CRIAR O JOGADOR ---------------
{
	//iniciar no centro da tela
	var playerx = 0;
	if (global.direcao==3) playerx = room_width;
	else if (global.direcao!=1) playerx = room_width/2; // por random?
	var playery = 0;
	if(!global.direcao) playery = room_height;
	else if(global.direcao!=2) playery = room_height/2;
	
	instance_create_layer(playerx, playery ,"player", obj_F_15);
	instance_create_layer(-10, 580, "playerAux", obj_f15_Aux1);
	instance_create_layer(-10, 220, "playerAux", obj_F15_Azul);
	startRoomTimeline(NavezinhaIn, 0, 1, 1);
}