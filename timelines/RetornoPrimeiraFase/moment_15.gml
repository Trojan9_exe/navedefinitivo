///@description finaliza diálogo e começa outra cutscene
if(obj_cutscene.next == 33) {
	var obj_cut = instance_find(obj_cutscene, 0);
	obj_cut.SPPiloto = false;
	//obj_cutscene.SPGeneral = false;
	obj_cut.SPGeneral2 = false;
	obj_cut.podeDigitar = false;
	obj_cut.lincoln = false;
	instance_destroy(obj_cut);
	timeline_goto_next();
}
else timeline_position -= 1;