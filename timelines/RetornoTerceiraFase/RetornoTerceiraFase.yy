{
    "id": "9bb87d4a-61be-4b37-a6ce-473feba07d79",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "RetornoTerceiraFase",
    "momentList": [
        {
            "id": "c09656d6-d409-409b-984e-35f4a4964279",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "4fb12a36-fb52-4c75-8bd5-07e425f9df87",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "9bb87d4a-61be-4b37-a6ce-473feba07d79"
            },
            "moment": 0
        },
        {
            "id": "037679f9-73ea-494d-8ed8-d340ed2a693e",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "7b967b65-ad69-4676-8ecb-6e793b399bf0",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 1,
                "eventtype": 0,
                "m_owner": "9bb87d4a-61be-4b37-a6ce-473feba07d79"
            },
            "moment": 1
        },
        {
            "id": "90c4a95c-e338-4fcc-a4b2-86a9e344c7cc",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "5411b0d8-2fb7-4a0a-97f1-c4d577b92fe2",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 60,
                "eventtype": 0,
                "m_owner": "9bb87d4a-61be-4b37-a6ce-473feba07d79"
            },
            "moment": 60
        },
        {
            "id": "45bdbd24-6614-4d14-8c2c-f3facedc5790",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "9b27ee57-3691-4d22-98bc-ee500ad580a7",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 120,
                "eventtype": 0,
                "m_owner": "9bb87d4a-61be-4b37-a6ce-473feba07d79"
            },
            "moment": 120
        },
        {
            "id": "7849831f-da8e-4bf0-aeba-b1e47028a815",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "c807d42e-58d3-4cbb-bef3-d1a545e4298b",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 205,
                "eventtype": 0,
                "m_owner": "9bb87d4a-61be-4b37-a6ce-473feba07d79"
            },
            "moment": 205
        }
    ]
}