if(!instance_exists(obj_intro)) {
	if(instance_exists(obj_cutscene)) instance_destroy(obj_cutscene);
	global.direcao = random_range(0, 3);
	//BACKGROUND
	var lay_id = layer_get_id("Background");
	var back_id = layer_background_get_id(lay_id);
	layer_background_sprite(back_id, spr_water);
	layer_background_htiled(back_id, true);
	layer_background_vtiled(back_id, true);
	layer_background_xscale(back_id, 0.2);
	layer_background_yscale(back_id, 0.2);
	var lay_speed = 3;
	layer_vspeed(lay_id, ((global.direcao==0)-(global.direcao==2))*lay_speed);
	layer_hspeed(lay_id, ((global.direcao==3)-(global.direcao==1))*lay_speed);

	instance_create_layer(640, 360, "intro", obj_intro);
}
else timeline_position -= 1;