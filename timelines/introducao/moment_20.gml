/// @description verifica desacelerar
var lay_id = layer_get_id("Background");

if(obj_cutscene.next >= 8) {
	var lay_speed = Approach(layer_get_hspeed(lay_id), -3, 0.2);
	layer_hspeed(lay_id, lay_speed); 
	var player = instance_find(obj_F_15,0);
	player.x = Approach(player.x, 300, 5);
}
if(obj_cutscene.next == 9) {
	if(layer_get_hspeed(lay_id)<-3) obj_cutscene.podeDigitar = false;
	else //se desacelerou
		obj_cutscene.podeDigitar = true;
}
else timeline_position -=1;