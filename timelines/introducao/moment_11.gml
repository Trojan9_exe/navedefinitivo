/// @description voando
if(instance_exists(obj_F_15)){
	var player = instance_find(obj_F_15,0);
	var pos = 800;
	if(player.x<pos){
		var x_spd = 2;
		if(player.x < (pos*0.5)) x_spd = 4;
		else if(player.x < (pos*0.7)) x_spd = 6;
		else if(player.x < (pos*0.9)) x_spd = 8;
		else x_spd = 10;
		
		var lay_speed = -x_spd*2;
		var lay_id = layer_get_id("Background");
		layer_hspeed(lay_id, lay_speed);
		
		player.x = Approach(player.x, pos, x_spd);
		
		if(instance_exists(obj_enterprise)){
			var plat = instance_find(obj_enterprise,0);
			plat.x = Approach(plat.x, -plat.sprite_width, x_spd*2);
			if(plat.x<=-plat.sprite_width) instance_destroy(plat);
		}
		//if(!instance_exists(obj_intro)) instance_create_layer(0, 0, "intro", obj_intro);
		
		timeline_position -=1;
	}
	else{
		obj_cutscene.podeDigitar = true;
		var obj_cut = instance_find(obj_cutscene,0);
		//obj_cut.next = 6;
	}
}