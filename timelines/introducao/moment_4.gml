if(obj_cutscene.next > 72) {
	// PLAYER CONFIG
	global.tutorial = true;
	global.podeCriar = false;
	global.direcao = 1; //0:cima 1:direita 2:baixo 3:esquerda 
	global.vidas = 1;
	global.tipo_controle = 0;
	global.haveBombsLeft = 0;
	global.energia_MAX =  global.game_dificulty * 300;
	global.player_tiro_damage =  global.game_dificulty * 10;
	global.player_self_kill_damage = global.game_dificulty * 100;
	global.player_bomb_damage = global.game_dificulty * 300;
	global.showPlayerInterface = false;
	global.endgame = false;
	// ENEMIES CONFIG
	global.enemy_tiro_damage = global.game_dificulty * 10;
	global.enemy_self_kill_damage = global.game_dificulty * 10;
	global.enemyHP = 10;
	global.inimigo_speed = 8;

	// BOSS CONFIG
	global.boss_tiro_damage = global.game_dificulty * 30;
	global.boss_self_kill_damage = global.game_dificulty * 1000;
	global.BossHP = global.game_dificulty*300;
	global.floor_distance = 10;

	//BACKGROUND
	var lay_id = layer_get_id("Background");
	var back_id = layer_background_get_id(lay_id);
	layer_background_sprite(back_id, spr_water);
	layer_background_htiled(back_id, true);
	layer_background_vtiled(back_id, true);
	layer_background_xscale(back_id, 0.5);
	layer_background_yscale(back_id, 0.5);

	var lay_speed = 3;
	layer_hspeed(lay_id, lay_speed);
	
	obj_cutscene.next = 0;
	if(!instance_exists(obj_enterprise)) 
		instance_create_layer(-180, 0, "elementos", obj_enterprise);
	if(!instance_exists(obj_F_15)) {
		var player = instance_create_layer(300, room_height/2 + 10, "player", obj_F_15);
		player.interacao = false;
	}
	obj_cutscene.SPGeneral = true;
	obj_cutscene.SPPiloto = true;
}
else timeline_position -= 1;
