{
    "id": "2280f972-5cc9-462c-b803-315cbcddffab",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "InicioTerceiraFase",
    "momentList": [
        {
            "id": "8e5c9921-ae3a-4bf2-8035-de541d5d05fc",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "0b8fd2d7-a580-46f6-a81e-3c4c25ca153d",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "2280f972-5cc9-462c-b803-315cbcddffab"
            },
            "moment": 0
        },
        {
            "id": "3417e5cf-8c77-4f50-be39-795affbdaa08",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "66b15f90-3e1d-4ba4-866c-373ee618cd22",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 1,
                "eventtype": 0,
                "m_owner": "2280f972-5cc9-462c-b803-315cbcddffab"
            },
            "moment": 1
        },
        {
            "id": "4cc2cec6-0905-4847-8075-afb094781f95",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "d6613912-6c9e-4ada-9441-35c388bd5233",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 8,
                "eventtype": 0,
                "m_owner": "2280f972-5cc9-462c-b803-315cbcddffab"
            },
            "moment": 8
        },
        {
            "id": "81d2190d-5c37-4621-900c-361d1571fa13",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "99eac73e-faf7-46ce-8b22-a275797cd2b6",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "2280f972-5cc9-462c-b803-315cbcddffab"
            },
            "moment": 30
        }
    ]
}