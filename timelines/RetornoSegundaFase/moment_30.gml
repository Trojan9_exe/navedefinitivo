///@description finaliza diálogo e começa outra cutscene
if(obj_cutscene.next == 49) {
	var obj_cut = instance_find(obj_cutscene, 0);
	obj_cut.SPPiloto = false;
	obj_cut.Powell = false;
	obj_cut.podeDigitar = false;
	instance_destroy(obj_cut);
	timeline_goto_next();
}
else timeline_position -= 1;