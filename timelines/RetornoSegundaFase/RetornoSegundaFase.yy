{
    "id": "cb5da356-226a-4395-a617-ed1814027d96",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "RetornoSegundaFase",
    "momentList": [
        {
            "id": "69d04a4d-9443-4b1f-bca9-4e1be9a119af",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "b5c2ba5e-7193-45b7-bf70-e6b044208700",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "cb5da356-226a-4395-a617-ed1814027d96"
            },
            "moment": 0
        },
        {
            "id": "7c367b9a-dc4f-4f13-ab3d-f56cf11eff10",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "09611ac2-35bb-40c1-8299-d34e610d169f",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 1,
                "eventtype": 0,
                "m_owner": "cb5da356-226a-4395-a617-ed1814027d96"
            },
            "moment": 1
        },
        {
            "id": "fa716cd3-2ba3-4cb5-85ee-49a020421f3c",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "9c13fd0a-8fbb-453f-84e6-585a0da4efee",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 8,
                "eventtype": 0,
                "m_owner": "cb5da356-226a-4395-a617-ed1814027d96"
            },
            "moment": 8
        },
        {
            "id": "43b7cd66-1cf4-45ff-ade0-73e410f9966e",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "d8670028-14c7-44a3-b118-d6c5a5ad8b90",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "cb5da356-226a-4395-a617-ed1814027d96"
            },
            "moment": 30
        }
    ]
}