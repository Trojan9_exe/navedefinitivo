/// @description CONFIGURAR LEVEL - ETAPA
// PLAYER CONFIG
global.direcao = 2; //0:cima 1:direita 2:baixo 3:esquerda 
global.vidas = 3;
global.haveBombsLeft = 0;
global.energia_MAX =  global.game_dificulty * 300;
global.player_tiro_damage =  global.game_dificulty * 10;
global.player_self_kill_damage = global.game_dificulty * 100;
global.player_bomb_damage = global.game_dificulty * 300;
global.showPlayerInterface = true;
global.start_countdown = 0;
global.level_countdown = timeline_max_moment(timeline_index);
global.endgame = false;
//global.tipo_controle = 0;
// ENEMIES CONFIG
global.enemy_tiro_damage = global.game_dificulty * 10;
global.enemy_self_kill_damage = global.game_dificulty * 10;
global.enemyHP = global.game_dificulty*20;

// BOSS CONFIG
global.boss_tiro_damage = global.game_dificulty * 30;
global.boss_self_kill_damage = global.game_dificulty * 1000;
global.BossHP = global.game_dificulty*300;

global.floor_distance = 30;

//BACKGROUND
var lay_id = layer_get_id("Background");
var back_id = layer_background_get_id(lay_id);
layer_background_sprite(back_id, spr_grass);
layer_background_htiled(back_id, true);
layer_background_vtiled(back_id, true);
layer_background_xscale(back_id, 0.6);
layer_background_yscale(back_id, 0.6);
var lay_speed = 4;
layer_vspeed(lay_id, ((global.direcao==0)-(global.direcao==2))*lay_speed);
layer_hspeed(lay_id, ((global.direcao==3)-(global.direcao==1))*lay_speed);