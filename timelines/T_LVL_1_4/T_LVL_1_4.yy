{
    "id": "d23fd377-532e-47c8-8e4d-58a99210722f",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "T_LVL_1_4",
    "momentList": [
        {
            "id": "c4266731-db9e-448f-a2e8-98a8536ae847",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "2bffc3cf-509b-4d7b-90fa-204dc90df43d",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "d23fd377-532e-47c8-8e4d-58a99210722f"
            },
            "moment": 0
        },
        {
            "id": "c236641d-4212-4ee5-958e-a51f047607c2",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "f2494439-64b6-44bc-921c-966ccb982689",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 300,
                "eventtype": 0,
                "m_owner": "d23fd377-532e-47c8-8e4d-58a99210722f"
            },
            "moment": 300
        },
        {
            "id": "934193f1-58f9-406a-9785-5b701e286f6d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "e6056ff1-8171-4ecd-8aaa-a1fc59ab13d2",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 500,
                "eventtype": 0,
                "m_owner": "d23fd377-532e-47c8-8e4d-58a99210722f"
            },
            "moment": 500
        },
        {
            "id": "3e34b4aa-0052-4846-943c-8d7666176f53",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "f73c7167-3e21-4f1e-a9c1-c69fd316f8af",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 900,
                "eventtype": 0,
                "m_owner": "d23fd377-532e-47c8-8e4d-58a99210722f"
            },
            "moment": 900
        }
    ]
}