/// @description criarInimigoQtdWavePos(layer, object, qtd, waveMode)
/// @param layer timeline to create and play
/// @param object position to play
/// @param qtd position to play
/// @param waveMode position to play

for(var i=argument2; i>0; i--){
	var rangeInicio = (!global.direcao || global.direcao==2) ? 250 : 50;
	var rangeFim = (!global.direcao || global.direcao==2) ? room_width-250 : room_height-50;
	
	switch(argument3){
		case 1: // ------------------------------------- formação em V
		default: // ------------------------------------- linha reta
			// basicamente dividir o espaço pela qtd de objetos
			var relativePos = rangeInicio+(((rangeFim-rangeInicio)/(argument2+1))*i);
			
			if(argument3){ // ------------------------------------- formação em V
				var relativeDist = 200*(((argument2/2)%1)+1);
				var relativeVPos = (i<=(argument2/2)) ? i-1 : argument2-i;
				var relativeVDist = 100*relativeVPos;
			}else{ // ------------------------------------- linha reta
				var relativeDist = 200;
				var relativeVPos = 0;
				var relativeVDist = 0;
			}
			
			switch(global.direcao){
				case 0: //cima 
					var posY = -relativeDist+relativeVDist;
					var posX = relativePos;
				break;
				case 2: //baixo
					var posY = room_height+relativeDist-relativeVDist;
					var posX = relativePos;
				break;
				case 1: //direita 
					var posY = relativePos;
					var posX = room_width+relativeDist-relativeVDist;
				break;
				case 3: //esquerda 
					var posY = relativePos;
					var posX = -relativeDist+relativeVDist;
				break;
				default: break;
			}
			
		break; // ------------------------------------- linha reta
	}

	instance_create_layer(posX, posY, argument0, argument1);
}