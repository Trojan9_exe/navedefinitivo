/// @description Executado no momento de criação da room somente na primeira abertura do jogo
// Velocidade do jogo
game_set_speed(60, gamespeed_fps);
//definição do roteiro de timelines
var iTL = 0;
global.timelineIdx = 0; // esse cara que sabe em qual timeline está. Utilize a função 'timeline_goto_next' ou 'timeline_goto_previous'
var isTesting = false;
if(isTesting){

	global.timelines[iTL++] = T_TST_1_1;
	global.timelines[iTL++] = T_TST_1_2;
	global.timelines[iTL++] = T_TST_1_3;
	global.timelines[iTL++] = T_TST_1_4;
	global.timelines[iTL++] = T_TST_2_1;
	global.timelines[iTL++] = T_TST_2_2;
	global.timelines[iTL++] = T_TST_2_3;
	global.timelines[iTL++] = T_TST_2_4;

}
else {
	//no futuro, cada timeline(T_LVL) pode ser já uma missão inteira;
	
	//TUTORIAL:
	global.timelines[iTL++] = introducao;
	
	//PRIMEIRA FASE:
	global.timelines[iTL++] = InicioPrimeiraFase; //cutscene pré primeira missão;
	global.timelines[iTL++] = T_LVL_1_1;
	global.timelines[iTL++] = RetornoPrimeiraFase; 
	
	//SEGUNDA FASE:
	global.timelines[iTL++] = InicioSegundaFase;
	global.timelines[iTL++] = T_LVL_1_2;
	global.timelines[iTL++] = RetornoSegundaFase;
	
	//TERCEIRA FASE:
	global.timelines[iTL++] = InicioTerceiraFase;
	global.timelines[iTL++] = T_LVL_1_3;
	global.timelines[iTL++] = RetornoTerceiraFase;
	
	//está timeline pode ser a fase bônus;
	//podemos colocar um OVNI passando e abduzindo o f15 kkkk, ó as ideia;
	//global.timelines[iTL++] = T_LVL_1_4;
}

global.timelinesMAX = iTL; // esse cara que sabe em qual parar. No caso de atingir a última timeline executa 'timeline_goto_end'

// FIRST TIME - INICIALIZA TODAS AS VARIAVEIS
global.pause = false;
global.endgame = false; 
global.showPlayerInterface = false;

global.game_debug = false;

global.direcao = 0; //0:cima 1:direita 2:baixo 3:esquerda 
global.game_dificulty = 2;
global.tipo_controle = 0;
global.game_last_level = -1;
global.game_level = -1;

global.haveBombsLeft = 0;
global.energia_MAX = 0;
global.pontos = 0;
global.vidas = 3; // precisa iniciar em 1, se for zero ja se fudeu
global.start_countdown = 0;
global.level_countdown = 0;

global.enemyPoints = 10;
global.BossPoints = 1000;
global.player_tiro_speed = 20;
global.player_tiro_damage = 0;
global.player_bomb_damage = 0;
//global.inimigo_speed = 8;
global.boss_speed = 4;

global.BossHP = 0;
global.enemyHP = 0;
global.floor_distance = 16;

// for tutorial purposes
global.tut_needed_enemies = 0;
global.tut_needed_bosses = 0;

// verificação de requisitos
global.GP = gamepad_is_supported();
