/// @description checkGamepadKeyIsBeingPressed(gamepad_slot, variable, key)
if(!argument1) return gamepad_button_check_pressed(argument0, argument2);
else if gamepad_button_check_released(argument0, argument2) return false;
else return true;