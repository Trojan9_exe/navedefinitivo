/// @description obj_controle_evt_BeginStep()

if(!global.pause){
	if(global.game_last_level != global.game_level){ // TROCA DE NIVEL ------------------------------------
		DestruirTudo();
		startRoomTimeline(global.game_level, 0, 0, 1); 
		global.game_last_level = global.game_level;
	}
	
	//if(global.showPlayerInterface && !instance_exists(obj_F_15)) // HORA DE CRIAR O JOGADOR ---------------
	//{
	//	//iniciar no centro da tela
	//	var playerx = 0;
	//	if (global.direcao==3) playerx = room_width;
	//	else if (global.direcao!=1) playerx = room_width/2; // por random?
	//	var playery = 0;
	//	if(!global.direcao) playery = room_height;
	//	else if(global.direcao!=2) playery = room_height/2;
		
	//	instance_create_layer(playerx, playery ,"player", obj_F_15);
	//	startRoomTimeline(NavezinhaIn, 0, 1, 1);
	//}
	
	//if(!global.showPlayerInterface && instance_exists(obj_F_15)) // TIRA O JOGADOR DAI --------------------
	//{
	//	if(instance_find(obj_F_15,0).interacao){
	//		//instance_find(obj_F_15,0).interacao = false;
	//		//startRoomTimeline(NavezinhaOut, 0, 1, 1); // sem loop
	//		NaveSaindo(global.direcao, obj_F_15);
	//	}
	//}

	if(keyboard_check_pressed(vk_escape)) {
		global.pause = true;	
		instance_deactivate_all(true);
	}
}

input_up = keyboard_check_pressed(global.key_up);
input_down = keyboard_check_pressed(global.key_down);
input_enter = keyboard_check_pressed(global.key_enter);

var ds_grid = menu_pages[pag], ds_height = ds_grid_height(ds_grid);

if(inputting) {
	switch(ds_grid[# 1, menu_option[pag]]) {
		case menu_tipoElemento.shift:
		var hinput = keyboard_check_pressed(global.key_right) - keyboard_check_pressed(global.key_left);
			if(hinput != 0) {
				ds_grid[# 3, menu_option[pag]] += hinput;
				ds_grid[# 3, menu_option[pag]] = clamp(ds_grid[# 3, menu_option[pag]], 0, array_length_1d(ds_grid[# 4, menu_option[pag]]) - 1);
				script_execute(ds_grid[# 2, menu_option[pag]], ds_grid[# 3, menu_option[pag]]);
			}
		break;
		
		case menu_tipoElemento.slider:
			var hinput = keyboard_check(global.key_right) - keyboard_check(global.key_left);
				if(hinput != 0) {
					ds_grid[# 3, menu_option[pag]] += hinput * 0.01;
					ds_grid[# 3, menu_option[pag]] = clamp(ds_grid[# 3, menu_option[pag]], 0, 1);
					script_execute(ds_grid[# 2, menu_option[pag]], ds_grid[# 3, menu_option[pag]]);
				}
		break;
	}

} 
else {
	var change = input_down - input_up;
	if(change != 0) 
	{
		menu_option[pag] += change;
	
		if(menu_option[pag] > ds_height - 1) 
			menu_option[pag] = 0;
	
		if(menu_option[pag] < 0)
			menu_option[pag] = ds_height - 1;
	}
}

if(input_enter) 
{
	switch(ds_grid[# 1, menu_option[pag]]) 
	{
		case menu_tipoElemento.page_transfer:
			 pag = ds_grid[# 2, menu_option[pag]];
		break;
		
		case menu_tipoElemento.script_rn:
			script_execute(ds_grid[# 2, menu_option[pag]]);
		break;
		/*
		if(inputting) 
			script_execute(ds_grid[# 2, menu_option[pag]], ds_grid[# 3, menu_option[pag]]);
		break;
		*/
		//case menu_tipoElemento.toggle:
		case menu_tipoElemento.shift:
		case menu_tipoElemento.slider:
			inputting = !inputting;
		break;
	}
}


	