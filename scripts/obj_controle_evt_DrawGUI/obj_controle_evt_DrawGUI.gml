if(!global.pause) 
	exit;

var b = c_black;
var gwidth = global.view_width, gheight = global.view_heigth;
var ds_grid = menu_pages[pag], ds_height = ds_grid_height(ds_grid);
var y_buffer = 64, x_buffer = 32;
var start_y = (gheight / 2) - ((((ds_height - 1) / 2) * y_buffer)), start_x = gwidth / 2;

draw_set_font(font_interface);

//fundo do menu;
draw_rectangle_color(0,0, gwidth, gheight, b,b,b,b, false);

//elementos na esquerda
draw_set_valign(fa_middle);
draw_set_halign(fa_right);

var itx = start_x - x_buffer, ity, xo;
var yy = 0; repeat(ds_height) {
	ity = start_y + (yy * y_buffer);
	b = c_white;
	xo = 0;
	
	if(yy == menu_option[pag]) {
		b = c_lime;
		xo = - (x_buffer / 2);
	}
//explicação para o uso da hashtag nos vetores:

//So these are called "accessors" - they're different for every data structure type. The grids use "#", 
//while ds_lists use "|", and ds_maps use "?". It's quite arbitrary. But basically, they're just a "quick way" to get access to a grid. 
//There are actually functions that will do the same thing:
//ds_grid_get(index, x cell position, y cell position);    //gets the value a cell holds
//ds_grid_set(index, x cell position, y cell position, value);   //sets a cell equal to a value
//But instead of using those, you can just write
//value = ds_grid[# cell_x, cell_y];
//ds_grid[# cell_x, cell_y] = value;
//So think of it like a shortcut!﻿
	
	draw_text_color(itx + xo, ity, ds_grid[# 0, yy], b,b,b,b, 1);
	yy++;
}

//linha do meio
draw_line(start_x, start_y - y_buffer, start_x, ity * y_buffer);

//elementos na direita
draw_set_halign(fa_left);
var rtx = start_x + x_buffer, rty;
yy = 0; repeat(ds_height) {
	rty = start_y + (yy * y_buffer);
	switch(ds_grid[# 1, yy]) {
		case menu_tipoElemento.shift:
			var current_val = ds_grid[# 3, yy];
			var current_array = ds_grid[# 4, yy];
			var left_shift = "<< ";
			var right_shift = " >>";
			
			if(current_val == 0)
				left_shift = "";
			if(current_val == array_length_1d(ds_grid[# 4, yy]) - 1)
				right_shift = "";
				
			b = c_white;
			//trocando de cor conforme o que foi selecionado;
			if(inputting and yy == menu_option[pag]) b = c_yellow;
			draw_text_color(rtx, rty, left_shift + current_array[current_val] + right_shift, b,b,b,b, 1);
		break;
		
		case menu_tipoElemento.slider:
			var len = 80;
			var current_val = ds_grid[# 3, yy];
			var current_array = ds_grid[# 4, yy];
			var circle_pos = ((current_val - current_array[0]) / (current_array[1] - current_array[0]));
			
			//exemplo
			//current_val = 3;
			//current_array = [2, 5];
			//ou seja... => ((3 - 2) / (5 - 2));
			//=> 1 / 3, a porcentagem será de:
			//1/3 * 100 = 33%
			
			b = c_white;
			if(inputting and yy == menu_option[pag]) b = c_yellow;
			draw_line_width(rtx, rty, rtx + len, rty, 4);
			draw_circle_color(rtx + (circle_pos * len), rty, 4, b, b, false);
			draw_text_color(rtx + (len * 1.2), rty, string(floor(circle_pos * 100))+"%", b,b,b,b, 1);
		break;
		
		case menu_tipoElemento.toggle:
			var current_val = ds_grid[# 3, yy];
			var c1, c2;
			b = c_white;
			if(inputting and yy == menu_option[pag]) b = c_yellow;
			if(current_val == 0) {
				c1 = b;
				c2 = c_dkgray;
			}
			else {
				c1 = c_dkgray;
				c2 = b;
			}
			
			draw_text_color(rtx, rty, "SETAS", c1,c1,c1,c1, 1);
			draw_text_color(rtx + 110, rty, "JOYSTICK", c2,c2,c2,c2, 1);
			draw_text_color(rtx + 260, rty, "WSAD", c2,c2,c2,c2, 1);
		break;
			
	}
	
	yy++;
}
draw_set_valign(fa_top);