/// @description timeline_goto_next()
if(global.timelinesMAX && global.timelineIdx > 1){
	global.timelineIdx--;
	if(global.game_level == global.timelines[global.timelineIdx]){ //em caso de ser a mesma timeline em execução
		timeline_goto_previous(); //recursão baby!
	}else global.game_level = global.timelines[global.timelineIdx]; // vai para a prox timeline
}else //antes da primeira timeline, faz o que?
	timeline_goto_end();