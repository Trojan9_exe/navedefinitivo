/// @description timeline_goto_next()
if(global.timelinesMAX && (global.timelineIdx+1) < global.timelinesMAX){
	global.timelineIdx++;
	if(global.game_level == global.timelines[global.timelineIdx]){ //em caso de ser a mesma timeline
		timeline_goto_next(); //recursão baby!
	}else global.game_level = global.timelines[global.timelineIdx]; // vai para a prox timeline
}else //última timeline
	timeline_goto_end();