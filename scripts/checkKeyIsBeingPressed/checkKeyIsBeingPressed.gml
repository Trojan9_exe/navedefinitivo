/// @description checkKeyIsBeingPressed(isMovingRight, vk_right)
if(!argument0) return keyboard_check_pressed(argument1);
else if keyboard_check_released(argument1) return false;
else return true;