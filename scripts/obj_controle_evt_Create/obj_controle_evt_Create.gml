/// @description obj_controle_evt_Create()

global.pause = false;
global.view_width = camera_get_view_width(view_camera[0]);
global.view_heigth = camera_get_view_height(view_camera[0]);

global.key_up = vk_up;
global.key_down = vk_down;
global.key_enter = vk_enter;
global.key_left = vk_left;
global.key_right = vk_right;

display_set_gui_size(global.view_width, global.view_heigth);

enum menu_pag {
	principal, configs, /*creditos,*/ audio, controles, desenvolvimento, height 
}

enum menu_tipoElemento {
	script_rn, page_transfer, slider, shift, toggle	
}

//criando páginas;
ds_menu_main = CriarMenu(
	["JOGAR", menu_tipoElemento.script_rn, ContinuarJogo],
	["CONFIGURAÇÕES", menu_tipoElemento.page_transfer, menu_pag.configs],
	//["CRÉDITOS", menu_tipoElemento.page_transfer, menu_pag.creditos],
	["SELEÇÃO DE FASES", menu_tipoElemento.shift, TrocaFase, 0, ["INTRO", "01_IN", "01", "01_END", "02_IN", "02", "02_END", "03_IN", "03", "03_END"]],
	["SAIR", menu_tipoElemento.script_rn, SairJogo]);

ds_menu_configuracao = CriarMenu(
	["VOLUME", menu_tipoElemento.page_transfer, menu_pag.audio],
	["CONTROLES", menu_tipoElemento.page_transfer, menu_pag.controles],
	["DESENVOLVIMENTO", menu_tipoElemento.page_transfer, menu_pag.desenvolvimento],
	["TELA CHEIA", menu_tipoElemento.shift, TrocarTela, 0, ["SIM", "NÃO"]],
	["DIFICULDADE", menu_tipoElemento.shift, TrocaDificuldade, 1, ["FÁCIL", "MÉDIO", "DIFÍCIL"]],
	["VOLTAR", menu_tipoElemento.page_transfer, menu_pag.principal]);
	
ds_menu_desenvolvimento = CriarMenu(
	//[""] será usado para os itens de debug...
	["DEBUG", menu_tipoElemento.shift, TrocarDebug, 1, ["LIGADO", "DESLIGADO"]],
	["VOLTAR",menu_tipoElemento.page_transfer, menu_pag.configs]);
	
ds_menu_creditos = CriarMenu(
	//[""] será usado para os créditos, com nomes ou imagens...
	["VOLTAR",menu_tipoElemento.page_transfer, menu_pag.principal]);
	
ds_menu_controles = CriarMenu(
	["PLAYER 1", menu_tipoElemento.shift, TrocarControles, 0, ["WSAD", "SETAS", "JOYSTICK"]],
	//["PLAYER 2", menu_tipoElemento.shift, TrocarControles, 0, ["WSAD", "SETAS", "JOYSTICK"]],
	["VOLTAR", menu_tipoElemento.page_transfer, menu_pag.configs]);

ds_menu_audio = CriarMenu(
	["MASTER", menu_tipoElemento.slider, ControleVolume, 1, [0,1]],
	["EFEITOS", menu_tipoElemento.slider, ControleVolume, 1, [0,1]],
	["MÚSICA", menu_tipoElemento.slider, ControleVolume, 1, [0,1]],
	["VOLTAR", menu_tipoElemento.page_transfer, menu_pag.configs]);
	
pag = 0;
menu_pages = [ds_menu_main, ds_menu_configuracao, /*ds_menu_creditos,*/ ds_menu_audio, ds_menu_controles, ds_menu_desenvolvimento];
var i = 0, array_len = array_length_1d(menu_pages);
repeat(array_len) {
	menu_option[i] = 0;
	i++
}

inputting = false;