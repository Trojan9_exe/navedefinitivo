//verificar com o switch qual a direção da fase;
//0:cima 1:direita 2:baixo 3:esquerda 
switch(argument0) {
	case 0: 
		obj_F_15.image_index = 1; //assim a nave para de tremer qnd vai sair da tela;
		obj_F_15.image_speed = 0;
		obj_F_15.vspeed -= 8; 
		obj_F_15.hspeed = 0;
		
		var obj_aux1 = instance_find(obj_f15_Aux1, 0);
		var obj_aux2 = instance_find(obj_F15_Azul, 0);
		
		if(instance_exists(obj_aux1)) { 
			obj_aux1.hspeed += 8;
			obj_aux1.vspeed = 0;
		}
		
		if(instance_exists(obj_aux2)) {
			obj_aux2.vspeed -= 8;
			obj_aux2.hspeed = 0;
		}
		
		break;
	case 1: 
		obj_F_15.image_index = 1; //assim a nave para de tremer qnd vai sair da tela;
		obj_F_15.image_speed = 0;
		obj_F_15.hspeed += 8; 
		obj_F_15.vspeed = 0;
		var obj_aux1 = instance_find(obj_f15_Aux1, 0);
		var obj_aux2 = instance_find(obj_F15_Azul, 0);
		if(instance_exists(obj_aux1)) { 
			obj_aux1.hspeed += 8;
			obj_aux1.vspeed = 0;
		}
		if(instance_exists(obj_aux2)) {
			obj_aux2.hspeed += 8;
			obj_aux2.vspeed = 0;
		}
		break;
	case 2: 
		obj_F_15.image_index = 1; //assim a nave para de tremer qnd vai sair da tela;
		obj_F_15.image_speed = 0;
		obj_F_15.vspeed += 8; 
		obj_F_15.hspeed = 0;
		var obj_aux1 = instance_find(obj_f15_Aux1, 0);
		var obj_aux2 = instance_find(obj_F15_Azul, 0);
		
		if(instance_exists(obj_aux1)) {
			obj_aux1.vspeed += 8;
			obj_aux1.hspeed = 0;
		}
		
		if(instance_exists(obj_aux2)) {
			obj_aux2.vspeed += 8;
			obj_aux2.hspeed = 0;
		}
		break;
	case 3: 
		obj_F_15.image_index = 1; //assim a nave para de tremer qnd vai sair da tela;
		obj_F_15.image_speed = 0;
		obj_F_15.hspeed -= 8;
		obj_F_15.vspeed = 0;
		var obj_aux1 = instance_find(obj_f15_Aux1, 0);
		var obj_aux2 = instance_find(obj_F15_Azul, 0);
		if(instance_exists(obj_aux1)) {
			obj_aux1.hspeed -= 8;
			obj_aux1.vspeed = 0;
		}
		if(instance_exists(obj_aux2)) {
			obj_aux2.hspeed -= 8;
			obj_aux2.vspeed = 0;
		}
		break;
}