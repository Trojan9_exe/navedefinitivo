/// @description obj_controle_evt_Draw()

if(global.showPlayerInterface){
//	// -------------------------------------------------------- INTERFACE
	draw_sprite(spr_interface, 1, 1140, 130);
	draw_set_font(font_aux);
	draw_set_color(c_white);
	draw_text(1220, 70, string(global.pontos) + "  Pts");
	draw_text(1100, 148, "x" + string(global.haveBombsLeft));
	draw_text(1205, 148, "x" + string(global.vidas));
	if(instance_exists(obj_F_15)) {
		var player = instance_find(obj_F_15,0);
		var restOfLife = (player.hp/global.energia_MAX)*100;
		if(!variable_instance_exists(player, "lastHP") /*|| player.lastHP < restOfLife*/) {
			player.lastHP = restOfLife;
		}
		player.lastHP = Approach(player.lastHP, restOfLife, 1);
		draw_healthbar(1032, 115, 1245, 95, player.lastHP, c_white, c_red, c_lime, 0, true, true);
	}
	else {
		draw_healthbar(1032, 115, 1245, 95, 100, c_white, c_red, c_lime, 0, true, true);
	}
	// -------------------------------------------------------- INTERFACE
	
	
	if(global.game_debug){
	
		if(global.level_countdown>=0){
			draw_set_font(font_countdown);
			draw_set_color(c_red);
			draw_text(5, 5, "TEMPO RESTANTE DA FASE: "+string(global.level_countdown/room_speed)+" seg");
			global.level_countdown--;
		}
		
		if(global.start_countdown>=0){
			draw_set_font(font_countdown);
			draw_set_color(c_red);
			draw_text(5, 25, "TEMPO DE VIDA DO BOSS: "+string(global.start_countdown/room_speed)+" seg");
			global.start_countdown--;
		}
	
		draw_set_font(font_debug);
		draw_set_color(c_yellow);
		draw_text(5, 60, "DEBUG:");
		draw_text(5, 70, "direcao: "+string(global.direcao));
		if(instance_exists(obj_F_15)){
			var player = instance_find(obj_F_15,0);
			draw_text(5, 80, "movement:");
			if(player.isMovingUp) draw_text(00, 90, "up");
			if(player.isMovingRight) draw_text(10, 90, " -->");
			if(player.isMovingLeft) draw_text(10, 90, "<-- ");
			if(player.isMovingDown) draw_text(40, 90, "down");
			var curFrame = player.image_index;
			var curFrameSpeed = player.image_speed;
			draw_text(5, 155, "frame index:"+string(curFrame));
			draw_text(5, 170, "frame speed:"+string(curFrameSpeed));
			draw_text(5, 180, "timeline:"+string(global.game_level));
		}
		switch(global.tipo_controle){
			case 0: draw_text(5, 110, "controle: WASD"); break;
			case 1: draw_text(5, 110, "controle: SETAS"); break;
			case 2: draw_text(5, 110, "controle: GAMEPAD"); break;
			default: break;
		}
		switch(global.game_dificulty){
			case 1: draw_text(5, 125, "dificuldade: FACIL"); break;
			case 2: draw_text(5, 125, "dificuldade: MEDIO"); break;
			case 3: draw_text(5, 125, "dificuldade: DIFICIL"); break;
			default: break;
		}
		if(global.tut_needed_enemies>0){
			draw_set_color(c_red);
			draw_text(5, 140, "INIMIGOS RESTANTES:"+string(global.tut_needed_enemies));
		}
		if(global.tut_needed_bosses>0){
			draw_set_color(c_red);
			draw_text(5, 140, "BOSS RESTANTES:"+string(global.tut_needed_bosses));
		}
	}
}