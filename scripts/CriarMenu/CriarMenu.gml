var arg, i = 0;
repeat(argument_count)
{
	//para x argumentos que temos 
	//o loop vai repeti-los, x vezes;
	arg[i] = argument[i];
	i++;
}
var ds_grid = ds_grid_create(5, argument_count);
i = 0; repeat(argument_count)
{
	var array = arg[i];
	var array_len = array_length_1d(array);
	
	var xx = 0; repeat(array_len) { 
		ds_grid[# xx, i] = array[xx];
		xx++;
	}	
	
	i++;
}
return ds_grid;