/// @description criarInimigoDirecao(layer, object)
/// @param layer timeline to create and play
/// @param object position to play

var spawnRangeInit = (global.direcao == 1 || global.direcao == 3) ? 100 : 300;
var spawnRangeEnd = (global.direcao == 1 || global.direcao == 3) ? room_height-spawnRangeInit : room_width-spawnRangeInit ;

// centralizar o boss
if(argument1 == obj_boss || argument1 == obj_boss_2 || argument1 == obj_boss_3 ||argument1 == obj_boss_4 ){
	spawnRangeInit += 100; // deixa o range menor ao centro
	spawnRangeEnd -= 100; // deixa o range menor ao centro
	if(global.direcao == 0 || global.direcao == 2) { // area maior, diminui mais
		spawnRangeInit += 200;
		spawnRangeEnd -= 200;
	}
}

switch(global.direcao){
	case 0: //cima 
		instance_create_layer(irandom_range(spawnRangeInit, spawnRangeEnd), -sprite_height, argument0, argument1);
	break;
	case 1: //direita 
		instance_create_layer(room_width+sprite_width, irandom_range(spawnRangeInit, spawnRangeEnd), argument0, argument1);
	break;
	case 2: //baixo
		instance_create_layer(irandom_range(spawnRangeInit, spawnRangeEnd), room_height+sprite_height, argument0, argument1);
	break;
	case 3: //esquerda 
		instance_create_layer(-sprite_width, irandom_range(spawnRangeInit, spawnRangeEnd), argument0, argument1);
	break;
	default: break;
}