if(hp <= 0) {
	global.vidas -= 1;
	
	var aux1 = instance_find(obj_f15_Aux1, 0);
	var aux2 = instance_find(obj_F15_Azul, 0);
	if(instance_exists(aux1)) instance_destroy(aux1);
	if(instance_exists(aux2)) instance_destroy(aux2);
	
	instance_destroy();
	//iniciar no centro da tela
	var playerx = 0;
	if (global.direcao==3) playerx = room_width;
	else if (global.direcao!=1) playerx = room_width/2; // por random?
	var playery = 0;
	if(!global.direcao) playery = room_height;
	else if(global.direcao!=2) playery = room_height/2;
	instance_create_layer(playerx, playery ,"player", obj_F_15);
	startRoomTimeline(NavezinhaIn, 0, 1, 1);
}
else if(interacao){
	//player movement
	//abstracao do tipo de controle
	switch(global.tipo_controle){
		case 6 : // Mouse Input - Bem óbvio.
		break;
		case 5 : // Device Input - Gambiarra para pegar duplo click
		break;
		case 4 : // Gesture Input - General Touchscreen or Touchpad
		break;
		case 3 : // GamePad Input
			/* RETIRADO DO MANUAL
			gp_face1		Top button 1 (this maps to the "A" on an Xbox 360 controller and the cross on a PS controller)
			gp_face2		Top button 2 (this maps to the "B" on an Xbox 360 controller and the circle on a PS controller)
			gp_face3		Top button 3 (this maps to the "X" on an Xbox 360 controller and the square on a PS controller)
			gp_face4		Top button 4 (this maps to the "Y" on an Xbox 360 controller and the triangle on a PS controller)
			gp_shoulderl	Left shoulder button
			gp_shoulderlb	Left shoulder trigger
			gp_shoulderr	Right shoulder button
			gp_shoulderrb	Right shoulder trigger
			gp_select		The select button (this is the PS button on a DS4 controller)
			gp_start		The start button (this is the "options" button on a PS4 controller)
			gp_stickl		The left stick pressed (as a button)
			gp_stickr		The right stick pressed (as a button)
			gp_padu			D-pad up
			gp_padd			D-pad down
			gp_padl			D-pad left
			gp_padr			D-pad right
			gp_axislh		Left stick horizontal axis (analogue)
			gp_axislv		Left stick vertical axis (analogue)
			gp_axisrh		Right stick horizontal axis (analogue)
			gp_axisrv		Right stick vertical axis (analogue)
			*/
			if(global.GP){
				var maxpads = gamepad_get_device_count();
				for (var i = 0; i < maxpads; i++)
			    {
			    if (gamepad_is_connected(i))
			        {
						// do stuff with pad "i"
						isMovingUp = checkGamepadKeyIsBeingPressed(i, isMovingUp, gp_padu);
						isMovingLeft = checkGamepadKeyIsBeingPressed(i, isMovingLeft, gp_padl);
						isMovingDown = checkGamepadKeyIsBeingPressed(i, isMovingDown, gp_padd);
						isMovingRight = checkGamepadKeyIsBeingPressed(i, isMovingRight, gp_padr);
						isKeyBombing = checkGamepadKeyIsBeingPressed(i, isKeyBombing, gp_face1);
						isKeyShooting = checkGamepadKeyIsBeingPressed(i, isKeyShooting, gp_face2);
						/* FOR LEFT STICK
						var haxis = gamepad_axis_value(0, gp_axislh);
						var vaxis = gamepad_axis_value(0, gp_axislv);
						direction = point_direction(0, 0, haxis, vaxis);
						speed = point_distance(0 ,0, haxis, vaxis) * 5;
						*/
			        }
			    }
			}
		break;
		case 2 : // Virtual Keys - FOR MOBILE - IF CREATED
		break;
		case 1: // Keyboard Input 1
			isMovingUp = checkKeyIsBeingPressed(isMovingUp, vk_up);
			isMovingLeft = checkKeyIsBeingPressed(isMovingLeft, vk_left);
			isMovingDown = checkKeyIsBeingPressed(isMovingDown, vk_down);
			isMovingRight = checkKeyIsBeingPressed(isMovingRight, vk_right);
			isKeyBombing = checkKeyIsBeingPressed(isKeyBombing, ord("B"));
			isKeyShooting = checkKeyIsBeingPressed(isKeyShooting, vk_space); //ord(" ") NAO USE ESPAÇO EM CONJUNTO DO DIRECIONAL, LEIA NO DUNDOC
		break;
		case 0: // Keyboard Input 2
			isMovingUp = checkKeyIsBeingPressed(isMovingUp, ord("W"));
			isMovingLeft = checkKeyIsBeingPressed(isMovingLeft, ord("A"));
			isMovingDown = checkKeyIsBeingPressed(isMovingDown, ord("S"));
			isMovingRight = checkKeyIsBeingPressed(isMovingRight, ord("D"));
			isKeyBombing = checkKeyIsBeingPressed(isKeyBombing, ord("B"));
			isKeyShooting = checkKeyIsBeingPressed(isKeyShooting, ord(" "));
		break;
	}

	//player movement
	if( (isMovingLeft && x > limitMovingLeft) || (isMovingRight && x < limitMovingRight) )
		hspeed = walkingSpeed * ( isMovingRight - isMovingLeft );
	else
		hspeed = 0; //fim da area permitida
		
	if( (isMovingUp && y > limitMovingUp) || (isMovingDown && y < limitMovingDown) )
		vspeed = walkingSpeed * ( isMovingDown - isMovingUp );
	else
		vspeed = 0; //fim da tela
	
	
	//IMAGE INDEX
	var imgChangeIdx = 0;
	switch(global.direcao) {
		case 0: imgChangeIdx = ( isMovingRight - isMovingLeft ); break;
		case 1: imgChangeIdx = ( isMovingDown - isMovingUp ); break;
		case 2: imgChangeIdx = ( isMovingLeft - isMovingRight ); break;
		case 3: imgChangeIdx = ( isMovingUp - isMovingDown ); break;
		default: break;
	}
	
	if(imgChangeIdx!=0){
		image_speed=0;
		if(image_index+imgChangeIdx<=6 && image_index+imgChangeIdx>=0)
			image_index += imgChangeIdx;
	}
	else{
		if(image_index!=3)
			image_speed=(image_index<3)-(image_index>3);
		else image_speed=0;
	}
	
		
	if(isKeyShooting && isTimeToShoot>=shootingDelay) {
		switch(global.direcao){
			case 0: //cima 
			case 2: //baixo
				instance_create_layer(x-6+hspeed, y,"player_tiros", obj_tiro);
				instance_create_layer(x+6+hspeed, y,"player_tiros", obj_tiro);
			break;
			case 1: //direita 
			case 3: //esquerda 
				instance_create_layer(x, y-6+vspeed,"player_tiros", obj_tiro);
				instance_create_layer(x, y+6+vspeed,"player_tiros", obj_tiro);
			break;
			default: break;
		}
		
		audio_play_sound(sound_tiro_03, 1, 0);
		
		if(isTimeToShootCycle>=isTimeToShootCycleDelay) {
			isTimeToShoot = -5;
			isTimeToShootCycle = 0;
		}
		else{
			isTimeToShootCycle++;
			isTimeToShoot = 0;
		}
	}

	if(global.haveBombsLeft >0 && isKeyBombing && isTimeToBOMBShoot>=shootingBOMBDelay){
		instance_create_layer(x, y, "player_tiros", obj_bomba);
		audio_play_sound(sound_missile_01, 1, 0);
		global.haveBombsLeft --;
		isTimeToBOMBShoot = 0;
	}
	//if(keyboard_check(ord("M"))) instance_destroy(obj_f15_Aux1);
	
	isTimeToBOMBShoot++;
	isTimeToShoot++;
}