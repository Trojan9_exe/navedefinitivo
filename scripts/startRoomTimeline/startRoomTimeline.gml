/// @description startRoomTimeline(timeline, position, loop, running)
/// @param timeline timeline to create and play
/// @param position position to play
/// @param loop position to play
/// @param running position to play
with (instance_create_layer(0, 0, "interface", obj_timeline)) {
	timeline_index = argument0;
	timeline_position = argument1;
	timeline_loop = argument2;
	timeline_running = argument3;
}