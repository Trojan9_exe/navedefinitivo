/// @description Executado no momento de criação da room
//if(!audio_is_playing(sound_Playlist)) audio_play_sound(sound_Playlist, 1, 1);	

// Identifica primeira abertura do jogo
if(!variable_global_exists("game_dificulty")){
	jogoPrimeiraAberturaDoJogo();
	global.timelineIdx = -1; // Na primeira abertura começa ensinando o jogador
}else{
	global.timelineIdx = 0; // depois disso pode começar do primeiro nível
}

timeline_goto_next();

if(!instance_exists(obj_controle)){
	// CREATE THE WHOLE CONTROL OBJECT
	instance_create_layer(0, 0, "interface", obj_controle);
	background_color = c_black;
}